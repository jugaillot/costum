function autoCompleteSearch(indexMin, indexMax, callBack, notUrl){
	mylog.log('START -------- autoCompleteSearch! agenda. ', typeof callBack, callBack, searchObject.types);
	var data=searchInterface.constructObjectAndUrl(notUrl);    
  mylog.log('autoCompleteSearch! agenda. searchObject.types', searchObject.types);
  
	loadingData = true;

	var str = '<i class=\'fa fa-circle-o-notch fa-spin\'></i>';
	$('.btn-start-search').html(str);
	$('.btn-start-search').addClass('bg-azure');
	$('.btn-start-search').removeClass('bg-dark');

	if((indexMin==0 || (typeof pageEvent != 'undefined' && pageEvent)) && directory.appKeyParam != '#agenda'  ){
		coInterface.simpleScroll(0, 400);  
		coInterface.showLoader('#dropdown_search',trad.currentlyresearching);
		mapCO.showLoader();

	}else if(indexMin>0)
		coInterface.showLoader('#btnShowMoreResult',trad.currentlyresearching);

	if(isMapEnd)
		coInterface.showLoader('#map-loading-data', trad.currentlyloading);

	if(typeof data.startDate != 'undefined' && data.startDate != null){
		data.startDateUTC = moment(data.startDate*1000).local().format();
	}


	$.ajax({
		type: 'POST',
		url: baseUrl+'/' + moduleId + '/search/globalautocomplete',
		data: data,
		dataType: 'json',
		error: function (data){
			mylog.log('>>> error autocomplete search'); 
			mylog.dir(data);   
			$('#dropdown_search').html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
		},
		success: function(data){
			mylog.log('>>> success autocomplete search agenda. !!!! ', data);
			if(!data){ 
				toastr.error(data.content); 
			} else {
				//Get results object
				var results=data.results;
				//Get count object
				if(typeof data.count != 'undefined')
					searchAllEngine.searchCount=data.count;


				if((searchObject.indexMin==0 && !pageEvent) || (typeof searchObject.count != "undefined" && searchObject.count)){
					//Prepare footer and header of directory 
					mylog.log('autoCompleteSearch headerSearchContainer');

					$('.headerSearchContainer').html( directory.headerHtml(searchObject.indexMin) );
					if (data.count.organizations > (searchObject.indexStep || indexStep)){
						$('.footerSearchContainer').html( directory.footerHtml() );
					}	
				}
				str = '';

				if( typeof agenda != 'undefined' && 
					searchObject.types.length == 1 && 
					$.inArray( 'events', searchObject.types) >= 0 &&
					(   results.length > 0 ||
					( results.length == 0 && agenda.noResult == false) ) ){
					mylog.log('agenda. autoCompleteSearch moment', moment().get('date'), agenda.dayCount);
					var startMoment = agenda.getStartMoment(agenda.dayCount);
					mylog.log('agenda. autoCompleteSearch startMoment', startMoment);
					str = agenda.getDateHtml(startMoment);
				}

				if(typeof searchObject.ranges != 'undefined' && (typeof searchObject.indexStep == 'undefined' || searchObject.indexStep > 0))
					results=searchAllEngine.prepareAllSearch(results);
				//Add correct number to type filters
				if(searchObject.count)
					refreshCountBadge();

				//parcours la liste des résultats de la recherche
				str += directory.showResultsDirectoryHtml(results);
				if(	( notNull(directory.appKeyParam) && 
						typeof themeParams.pages[directory.appKeyParam] != 'undefined' && 
						typeof themeParams.pages[directory.appKeyParam].showMap != 'undefined') || 
					location.hash.indexOf('onMap=true')>=1 )
					showMap(true);


				if(str == '') {
					if( typeof agenda != 'undefined' &&
						typeof agenda.noResult != 'undefined' && 
						agenda.noResult === true &&
						searchObject.types.length == 1 && 
						$.inArray( 'events', searchObject.types) >= 0 ){
						// loadingData =false;
						// agenda.addDay();
					} else {
						$.unblockUI();
						$('.btn-start-search').html('<i class=\'fa fa-refresh\'></i>'); 
						str=directory.endOfResult(true);
						if(indexMin == 0){
							$('#dropdown_search').html(str);
						}else{
							$('.search-loader').remove();
							$('#dropdown_search').append(str);
						}
					}


					
				} else {       
					//CHARGEMENT DE TYPE SCROLL => ALL ELEMENTS OR MORE THAN ONE TYPE 
					//si on n'est pas sur une première recherche (chargement de la suite des résultat)
					var typeElement;
					if(indexMin > 0 && (typeof pageEvent == 'undefined' || !pageEvent)){
						//on affiche le résultat à l'écran
						$('#dropdown_search').append(str);

						if($('.search-loader').length) $('.search-loader').remove();
						if($('.pageTable').html()=='' && (searchObject.initType!= 'all' || searchObject.types.length==1)){
							typeElement=(searchObject.types=='persons') ? 'citoyens' : searchObject.types;
							initPageTable(searchAllEngine.searchCount[typeElement]);
						}
					}else{
						//on affiche le résultat à l'écran
						$('#dropdown_search').html(str);

						if(searchObject.initType == "all" && Object.keys(results).length < searchObject.indexStep){
							str=directory.endOfResult();   
							$('#dropdown_search').append(str);
						}

						if(directory.appKeyParam == '#agenda'){
							if(searchObject.text != '')
								$('#search-content .calendar').hide(700);
							else if(!$('#search-content .calendar').is(':visible'))
								$('#search-content .calendar').show(700);
						}

						if(searchObject.initType!= 'all' || searchObject.types.length==1){
							typeElement=(searchObject.types=='persons') ? 'citoyens' : searchObject.types;
							initPageTable(searchAllEngine.searchCount[typeElement]);
						}

						pageEvent=false;
					}
				}
				//remet l'icon "loupe" du bouton search
				$('.btn-start-search').html('<i class=\'fa fa-refresh\'></i>');
				//active les link lbh
				coInterface.bindLBHLinks();
				searchObject.count=false;
				$.unblockUI();
				$('#map-loading-data').html('');

				//initialise les boutons pour garder une entité dans Mon répertoire (boutons links)
				directory.bindBtnElement();
				coInterface.bindButtonOpenForm();
				//signal que le chargement est terminé
				loadingData = false;

				//quand la recherche est terminé, on remet la couleur normal du bouton search
				$('.btn-start-search').removeClass('bg-azure');
				$('#btn-my-co').removeClass('hidden');
			}


			if(mapElements.length==0 || indexMin == 0) 
				mapElements = results;
			else 
				$.extend(mapElements, results);
			if(location.hash == '#search' && searchObject.types.length > 1)
				directory.switcherViewer(mapElements);
			else
				directory.switcherViewer(results);
			//affiche les éléments sur la carte


			mapCO.clearMap();
			mapCO.addElts(mapElements);
			mapCO.map.invalidateSize();
			$('.btn-show-map').off().click(function(){
				if(typeof mapCO != 'undefined')
					showMap();
			});
			spinSearchAddon();
			searchAllEngine.searchInProgress = false;
			if(typeof callBack == 'function')
				callBack(results);
		}
	});


}