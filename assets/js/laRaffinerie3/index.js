costum.laRaffinerie3 = {

	directoryEvent : function(keyE, valE){
		mylog.log("costum.laRaffinerie3.directoryEvent", keyE, valE);
		var now = moment().format("YYYY-MM-DD");
		var isSameOrAfter = moment( moment(valE.startDate).format("YYYY-MM-DD") ).isSameOrAfter(now);
		mylog.log("costum.laRaffinerie3.directoryEvent isSameOrAfter", isSameOrAfter);
		var str = "";
		if ( isSameOrAfter === true ||  ( typeof valE.openingHours != "undefined" && valE.openingHours != null ) ) {
			var style = "background-color: #d6d6d6;";
			$.each(costum.paramsData.poles, function(kT, vT){
				//mylog.log("directoryEvent poles", kT, vT);
				if(typeof  valE.tags != "undefined" && 
					 valE.tags != null &&
					  valE.tags.length > 0 &&
					  $.inArray(kT, valE.tags) > -1)
					style = "background-color: "+vT.color+";";
			});
			str = "<div class='col-xs-12 col-sm-6 margin-bottom-10 text-center eventDirRaf' style='' >"+
					"<a href='#page.type.events.id."+keyE+"' class='lbh col-xs-12 no-padding ' style='"+style+"'' >"+
						"<div class='col-xs-6 no-padding divImg'  style='' >";

							if(typeof valE.imgMediumProfil != "undefined" && valE.imgMediumProfil != null)
								str+= valE.imgMediumProfil;
							else 
								str+= "<img src='"+baseUrl+valE.profilMediumImageUrl+"' style=''/>";
						str+="</div>"+
						"<div class='col-xs-6 eventDirText' style='' >"+
							"<div style=''>"+
								"<span style='text-transform: capitalize; font-size: 22px; ' class=' titleFont1 col-xs-12'>";
									var secStart = "";									
									if(typeof valE.startDateSec != "undefined" && valE.startDateSec != null)
										secStart = valE.startDateSec;
									else if(typeof valE.startDate != "undefined" && valE.startDate != null &&
											typeof valE.startDate.sec != "undefined" && valE.startDate.sec != null)
										secStart = valE.startDate.sec;

									if(secStart != ""){
										str+=	moment(secStart*1000).locale("fr").format("ddd DD.MM.YY")+"<br/>"+
		    								moment(secStart*1000).locale("fr").format("HH:mm");
									}
	    						str+=	"</span>"+
	    						"<span style='font-size: 22px;  color : black' class='titleFont2 col-xs-12 no-padding'>"+
	    							valE.name+
	    						"</span>"+
	    					"</div>"+
						"</div>"+
					"</a>"+
				"</div>";
		}
		mylog.log("costum.laRaffinerie3.directoryEvent str", str);
		return str;
	}
} ;

costum.initHtmlPosition = function(){
	mylog.log("costum.initHtmlPosition");
	$(window).bind("scroll",function(){ 
		//mylog.log("initHtmlPosition scroll iWindowsSize", $(this).scrollTop());
		var iWindowsSize = $(window).width();
		//mylog.log("scroll hva", $("#headerBand").length, iWindowsSize);
		
		if (iWindowsSize  > 860 ){
			if ($(this).scrollTop()  > 500 ){
				$(".footerRaffinerie").addClass("affix");
			}else
				$(".footerRaffinerie").removeClass("affix");
		}
	});
} ;

costum.calendar = {
	buildCalObj : function(eventObj, taskCal){
		var backgroundColor = "#d6d6d6 !important";
		$.each(costum.paramsData.poles, function(kT, vT){
			//mylog.log("directoryEvent poles", kT, vT);
			if(typeof  eventObj.tags != "undefined" && 
				 eventObj.tags != null &&
				  eventObj.tags.length > 0 &&
				  $.inArray(kT, eventObj.tags) > -1)
				backgroundColor = vT.color+ " !important" ;
		});

		taskCal.backgroundColor = backgroundColor;

		return taskCal;
	}
}