// pageProfil.views.newspaper = function(){
// 	mylog.log("costum pageProfil.views.newspaper");
// 	//coInterface.scrollTo("#profil_imgPreview");
// 	setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
// 		ajaxPost('#journal', baseUrl+"/news/co/index/type/"+typeItem+"/id/"+contextData.id, 
// 			{nbCol:1},
// 			function(){},"html");
// 	}, 700);
// };

pageProfil.views.directory = function(callBack){
	mylog.log("pageProfil.views.directory");
	var dataIcon = (!notEmpty(pageProfil.params.dir)) ? "users" : $(".smma[data-type-dir="+pageProfil.params.dir+"]").data("icon");
	pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[contextData.type] : pageProfil.params.dir;
	var sub=(!notEmpty(pageProfil.params.sub)) ? "" : "/sub/"+pageProfil.params.sub;

	var sort = ( ( pageProfil.params.dir == "events" ) ? "/sort/1" : "" );
	getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
		'/id/'+contextData.id+'/dataName/'+pageProfil.params.dir+sub+sort+'?tpl=json',
				function(data){ 
					var type = ($.inArray(pageProfil.params.dir, ["poi","ressources","vote","actions","discuss"]) >=0) ? pageProfil.params.dir : null;
					mylog.log("pageProfil.views.directory canEdit" , canEdit);
					if(typeof canEdit != "undefined" && canEdit)
						canEdit=pageProfil.params.dir;
					mylog.log("pageProfil.views.directory edit" , canEdit);
					displayInTheContainer(data, pageProfil.params.dir, dataIcon, type, canEdit);
					if(typeof mapCO != "undefined"){
						mapCO.clearMap();
			            mapCO.addElts(data);
			            mapCO.map.invalidateSize();
					}
					coInterface.bindButtonOpenForm();

					if(typeof callBack != "undefined" && callBack != null)
						callBack();
				}
	,"html");
}


pageProfil.initCallB = function(){
	mylog.log("pageProfil.initCallB Raffinerie");
	if(typeof pageProfil.params.view != "undefined" &&
	pageProfil.params.view != null && 
	pageProfil.params.view == "directory" && 
	typeof pageProfil.params.dir != "undefined" &&
	pageProfil.params.dir != null && 
	pageProfil.params.dir == "events"){
		$("#showHideCalendar").trigger("click");
	}
}