costum.notragora={
	// init : function(){
	// 	mylog.log("notragora here init");
	// 	addItemsToSly();
	// },
	poi : {
		afterSave : function(data){
			//if(dyFObj.editMode){
				mylog.log("iciciiiiii dans result good", data);
				dyFObj.closeForm();
				getPoiNA();
		}
	}
};

costum.loadByHash = function(oldHash){
	mylog.log("costum.loadByHash start", oldHash);
	var hash = "";
	if(typeof oldHash == "undefined"){
		hash = "";
	} else if(oldHash.indexOf("#element.detail.type") >= 0){
		mylog.log("costum.loadByHash if 1");
		var splitHash = oldHash.split(".");
		hash = "#page.type."+splitHash[3]+".id."+splitHash[5];
	}
	else if(oldHash.indexOf("#default.directoryjs?type=organizations") >= 0){
		mylog.log("costum.loadByHash if 2");
		hash = "#search";
	} else if(oldHash.indexOf("#default.directoryjs?type=poi") >= 0){
		mylog.log("costum.loadByHash if 2");
		hash = "#production";
	}  else if(oldHash.indexOf("#default.apropos") >= 0){
		mylog.log("costum.loadByHash if 2");
		hash = "#apropos";
	} else {;
		mylog.log("costum.loadByHash else");
		hash = oldHash;
	}
	//alert("loadByHash "+hash);
	mylog.log("costum.loadByHash end", hash);
	return hash;
};
directory.elementPanelHtml = function(params){
	//if(directory.dirLog) mylog.log("----------- elementPanelHtml",params.type,params.name,params.elTagsList);
	mylog.log("----------- Notragora elementPanelHtml",params.type,params.name,params.elTagsList, params);
	str = "";

	var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? "grayscale" : "" ) ;
	var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad["Wait for confirmation"] : "" ) ;
	var classType=params.type;
	if(params.type=="events") classType="";
	//str += "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12 searchEntityContainer "+grayscale+" "+classType+" "+params.elTagsList+" "+params.elRolesList+" contain_"+params.type+"_"+params.id+"'>";
	str += "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer "+grayscale+" "+classType+" "+params.elTagsList+" "+params.elRolesList+" contain_"+params.type+"_"+params.id+"'>";
	str +=    '<div class="searchEntity" id="entity'+params.id+'">';


	//var addFollowBtn = ( $.inArray(params.type, ["poi","ressources"])>=0 )  ? false : true;
	if(typeof params.edit  != "undefined" && notNull(params.edit))
		str += this.getAdminToolBar(params);

	if( params.tobeactivated == true ){
		str += "<div class='dateUpdated'><i class='fa fa-flash'></i> <span class='hidden-xs'>"+trad["Wait for confirmation"]+" </span></div>";
	}else{
		timeAction= /*(params.type=="events") ? trad.created :*/ trad.actif;
		if(params.updated != null )
			str += "<div class='dateUpdated'><i class='fa fa-flash'></i> <span class='hidden-xs'>"+timeAction+" </span>" + params.updated + "</div>";
	}

	var linkAction = " lbh";
	var parentStr = "" ;
	if(typeof params.imgType !="undefined" && params.imgType=="banner"){
		str += "<a href='"+params.hash+"' class='container-img-banner add2fav "+linkAction+">" + params.imgBanner + "</a>";
		str += "<div class='padding-10 informations tooltips'  data-toggle='tooltip' data-placement='top' data-original-title='"+tipIsInviting+"'>";

		str += "<div class='entityRight banner no-padding'>";



		if(typeof params.size == "undefined" || params.size == undefined || params.size == "max"){
			str += "<div class='entityCenter no-padding'>";
				str += "<a href='"+params.hash+"' class='container-thumbnail-profil add2fav "+linkAction+"'>" + params.imgProfil + "</a>";
				str += "<a href='"+params.hash+"' class='add2fav pull-right margin-top-15 "+linkAction+"'>" + params.htmlIco + "</a>";
			str += "</div>";
		}
	} else {
		var imgProfil = params.imgMediumProfil ;
		if(typeObj[itemType] && typeObj[itemType].col == "poi" 
		&& typeof params.medias != "undefined" && typeof params.medias[0] != "undefined" 
		&& typeof params.medias[0].content != "undefined"  && typeof params.medias[0].content.image != "undefined")
		imgProfil= "<img class='img-responsive' src='"+params.medias[0].content.image+"'/>";
		str += "<a href='"+params.hash+"' class='container-img-profil add2fav "+linkAction+"'>" + imgProfil + "</a>";
		str += "<div class='padding-10 informations tooltips'  data-toggle='tooltip' data-placement='top' data-original-title='"+tipIsInviting+"'>";

		str += "<div class='entityRight profil no-padding'>";

		
		if(typeof params.parent != "undefined" && typeObj[itemType] && typeObj[itemType].col == "poi"){
			$.each(params.parent, function(keyP, valP){
				parentStr +='<a href="#page.type.'+valP.type+'.id.'+keyP+'" style="font-size: 13px!important" class="entityName text-green lbh add2fav text-light-weight margin-bottom-5 "><i class="fa fa-users"></i> '+valP.name+'</a>';
				// str += "<a  href='#page.type."+valP.type+".id."+keyP+"' class=' entityName bold text-green add2fav "+linkAction+"'>"+
				//         "<i class='fa fa-users'></i> "+valP.name+
				//       "</a>";  
			});
		}

		if(typeof params.size == "undefined" || params.size == undefined || params.size == "max"){
			str += "<div class='entityCenter no-padding'>";
			str +=    "<a href='"+params.hash+"' class='add2fav pull-right "+linkAction+"'>" + params.htmlIco + "</a>";
			str += "</div>";
		}
	}
    
	if(notEmpty(params.typePoi)){
	//	str += "<span class='typePoiDir'><i class='fa fa-chevron-right'></i> " + tradCategory[params.typePoi] + "<hr></span>";  
	}

	var iconFaReply ="";// notEmpty(params.parent) ? "<i class='fa fa-reply fa-rotate-180'></i> " : "";
	str += parentStr+"<a  href='"+params.hash+"' class='"+params.size+" entityName bold text-dark add2fav "+linkAction+"'>"+
				iconFaReply + params.name +
			"</a>";  
	
	if(typeof(params.statusLink)!="undefined"){
		if( typeof(params.statusLink.isAdmin)!="undefined" && 
			typeof(params.statusLink.isAdminPending)=="undefined" && 
			typeof(params.statusLink.isAdminInviting)=="undefined" && 
			typeof(params.statusLink.toBeValidated)=="undefined")
			str+="<span class='text-red'>"+trad.administrator+"</span>";

		if(typeof(params.statusLink.isAdminInviting)!="undefined")
			str+="<span class='text-red'>"+trad.invitingToAdmin+"</span>";
		
		if(typeof(params.statusLink.toBeValidated)!="undefined" || typeof(params.statusLink.isAdminPending)!="undefined")
			str+="<span class='text-red'>"+trad.waitingValidation+"</span>";
	}

	if(params.rolesLbl != "")
		str += "<div class='rolesContainer'>"+params.rolesLbl+"</div>";


	var thisLocality = "";
	if(params.fullLocality != "" && params.fullLocality != " ")
		thisLocality = "<a href='"+params.hash+"' data-id='" + params.dataId + "'  class='entityLocality add2fav"+linkAction+"'>"+
						"<i class='fa fa-home'></i> " + params.fullLocality + "</a>";
	else thisLocality = "";

	str += thisLocality;

	var devise = (typeof params.devise != "undefined") ? params.devise : "";
	if(typeof params.price != "undefined" && params.price != "")
		str += "<div class='entityPrice text-azure'><i class='fa fa-money'></i> " + params.price + " " + devise + "</div>";

	if($.inArray(params.type, ["classifieds","ressources"])>=0 && typeof params.category != "undefined"){
		str += "<div class='entityType col-xs-12 no-padding'><span class='uppercase bold pull-left'>" + tradCategory[params.section] + " </span><span class='pull-left'>";
		if(typeof params.category != "undefined" && params.type != "poi") 
			str += " > " + tradCategory[params.category];
		if(typeof params.subtype != "undefined") str += " > " + tradCategory[params.subtype];
			str += "</span></div>";
	}

	if(notEmpty(params.typeEvent))
		str += "<div class='entityType'><span class='uppercase bold'>" + tradCategory[params.typeEvent] + "</span></div>";  

	if(params.type=="events"){
		var dateFormated = directory.getDateFormated(params, true);
		var countSubEvents = ( params.links && params.links.subEvents ) ? "<br/><i class='fa fa-calendar'></i> "+Object.keys(params.links.subEvents).length+" "+trad["subevent-s"]  : "" ;
		str += dateFormated+countSubEvents;
	}
	str += "<div class='entityDescription'>" + ( (params.shortDescription == null ) ? "" : params.shortDescription ) + "</div>";

	str += "<div class='tagsContainer text-red'>"+params.tagsLbl+"</div>";
	if(typeof params.counts != "undefined"){
		str+="<div class='col-xs-12 no-padding communityCounts'>";
		$.each(params.counts, function (key, count){
			iconLink=(key=="followers") ? "link" : "group";
			str +=  "<small class='pull-left lbh letter-light bg-transparent url elipsis bold countMembers margin-right-10'>"+
			"<i class='fa fa-"+iconLink+"'></i> "+ count + " " + trad[key] +
			"</small>";
		});
		str+="</div>";
	}

	str += "</div>";
	str += "</div>";
	str += "</div>";
	str += "</div>";
	str += "</div>";
	return str;
};

directory.headerHtml = function(){
    mylog.log("-----------headerHtml ");
    headerStr = '';
    if(!directory.isCostum("header") || directory.getCostumValue("header")){
        mylog.log("-----------headerHtml :",searchObject.count);
        if((typeof searchObject.count != "undefined" && searchObject.count) || searchObject.indexMin==0 ){
            countHeader=0;
            mylog.log("-----------headerHtml countHeader:",countHeader);
            if(searchObject.countType.length > 1 && typeof searchObject.ranges != "undefined"){
                $.each(searchAllEngine.searchCount, function(e, v){
                    countHeader+=v;
                });
                mylog.log("-----------headerHtml countHeader2:",countHeader);
                //posClass="right"
            } else {
                typeCount = (searchObject.types[0]=="persons") ? "citoyens" : searchObject.types[0];
                if(typeof searchAllEngine.searchCount[typeCount] != "undefined")
                    countHeader=searchAllEngine.searchCount[typeCount];
                mylog.log("-----------headerHtml countHeader3:",countHeader);
                // posClass=(typeCount == "classified") ? "left" : "right";
            }


            mylog.log("-----------headerHtml :"+countHeader, searchObject.initType);
            var resultsStr = "Productions des groupes de travail";
            var titleSize= "col-md-8 col-sm-8";
            var toolsSize= "col-md-4 col-sm-4";
            var colorH = "text-brown";

            if(searchObject.types[0]=="organizations"){
                resultsStr = "Groupes de travail";
                colorH = "text-green";
            }

            headerFilters = "";
            // if( typeof costum != "undefined" && costum != null && 
            //     typeof costum.filters != 'undefined' && 
            //     typeof costum.filters.searchFilters != 'undefined'){
            //     $.each(costum.filters.searchFilters,function(k,p) { 
            //         colorClass = (typeof p.colorClass != "undefined") ? p.colorClass : "";
            //         headerFilters += "<a href='javascript:;' data-filter='"+k+"' class='btn-coopfilter btn btn-xs "+colorClass+"'> "+p.label+"</a> ";
            //     });
            // }

            headerStr +='<div class="col-xs-12 margin-bottom-10">'+
                    '<h4 class="elipsis '+titleSize+' '+colorH+' col-xs-10 no-padding">'+
                        "<i class='fa fa-angle-down'></i> " + countHeader + " "+resultsStr+" ";
            // if( !notNull(directory.costum) 
            // || typeof directory.costum.header == "undefined"
            // || typeof directory.costum.header.searchTypeHtml == "undefined"
            // || directory.costum.header.searchTypeHtml === false){
            //     headerStr += '<small>'+directory.searchTypeHtml()+'</small>';
            // }
            headerStr += headerFilters+'</h4>'+ 
                        '<div class="'+toolsSize+' col-xs-2 pull-right no-padding text-right headerSearchTools" style="padding-top:3px !important;">';
            // TODO CLEM :: ADD SURVEY  
            // if(searchObject.types.length == 1 && searchObject.types[0]=="vote"){
            //  headerStr +='<a href="javascript:;" data-form-type="survey" class="addBtnFoot btn-open-form btn btn-default addBtnAll letter-turq margin-bottom-10">'+ 
            //                  '<i class="fa fa-plus"></i>'+
            //                  '<span>'+tradDynForm.createsurvey+'</span>'+
            //              '</a>';
            // }
            mylog.log("headerStr ", directory.appKeyParam);
            if( notNull(costum) 
	              && typeof costum.app != "undefined"
	              && typeof directory.appKeyParam != "undefined"
	              && typeof costum.app[directory.appKeyParam] != "undefined"
	              && typeof costum.app[directory.appKeyParam].map != "undefined"
	              && typeof costum.app[directory.appKeyParam].map.hash != "undefined"){
	        	mylog.log("headerStr if");
	            headerStr+= '<button class="lbh-menu-app btn-map-na hidden-xs" data-hash="'+costum.app[directory.appKeyParam].map.hash+'" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
	                          '<i class="fa fa-map-marker"></i> '+trad.map
	                        '</button>';
	        } else if( !notNull(directory.costum) 
						|| typeof directory.costum.header == "undefined"
						|| typeof directory.costum.header.map== "undefined"
						|| directory.costum.header.map) {
	        	mylog.log("headerStr else");
	            headerStr+=       '<button class="btn-show-map hidden-xs" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
	              '<i class="fa fa-map-marker"></i> '+trad.map
	            '</button>';
	        }

            if(userId!="" && searchObject.initType=="classifieds"){
                headerStr+='<button class="btn btn-default letter-blue addToAlert margin-right-5 tooltips" data-toggle="tooltip" data-placement="bottom" title="'+trad.bealertofnewitems+'" data-value="list" onclick="directory.addToAlert();"><i class="fa fa-bell"></i> <span class="hidden-xs">'+trad.alert+'</span></button>';
            }

            if(/*userId != "" 
            && */notNull(directory.costum) 
            && typeof directory.costum.header != "undefined" 
            && typeof directory.costum.header.add != "undefined"
            // && typeof directory.costum.header.add[searchObject.initType] != "undefined"
            && typeof directory.costum.header.add[searchObject.types] != "undefined"
            ){
                headerStr+="<div class='pull-right'>";
                headerStr+=directory.createBtnHtml();
                headerStr+="</div>";
            }
            if(!notNull(directory.costum) 
            || typeof directory.costum.header == "undefined"
            || typeof directory.costum.header.viewMode == "undefined"
            || directory.costum.header.viewMode)  {
                headerStr+='<button class="btn switchDirectoryView ';
                if(directory.viewMode=="list") headerStr+='active ';
                headerStr+=     'margin-right-5" data-value="list"><i class="fa fa-bars"></i></button>'+
                            '<button class="btn switchDirectoryView ';
                if(directory.viewMode=="block") headerStr+='active ';
                headerStr+=     '" data-value="block"><i class="fa fa-th-large"></i></button>';
            }


            headerStr+= '</div>';      
        }
    }

    mylog.log("-----------headerHtml headerStr", headerStr);
    return headerStr;
}