adminPanel.views.organizations = function(){
	var data={
		title : "Structures",
		types : [ "organizations" ],
		table : {
            name: {
                name : "Nom"
            },
            validated : { 
                name : "Valider",
                class : "col-xs-2 text-center"
            },
            tags : { 
                name : "Tags",
                class : "col-xs-2 text-center"
            },
            actions : {
                class : "col-xs-3 text-center"
            }
        },
        actions : {
            update : true,
            validated : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};