

/*[] Enregistre citoyens Ou tu regardes si il existe déjà mettre à jour ces infos 

[] Recherche si groupe existe par adress 
	[] si oui retour avec lemail framalist
	[] si non création du groupe
		[] nom automatique / code automatique / email framaliste auto
		[] envoie mail au pacte nouveau citoyen gérard - email framaliste
[] Retour data
	bool exist (pour le groupe) 
	object elt (frama) 
[] Retour subscribe auto mailing sendInblue
[] Mot si cest le premier à créer 
[] Mot avec email framaliste suscribe auto à la mailing
,Properties to finish in dynform (ex: mesures niveau d'ambition)
/*"level" : {
    "label" : "Crée un niveau",
    "inputType" : "lists",
    "entries":{
        "name":{
            "label":"Nom du niveau",
            "type":"text",
            "class":"col-xs-3"
        },
        "description":{
            "label":"Description du niveau",
            "type":"textarea",
            "class":"col-xs-8"
        }
    }
}*/

var pacte={
	initScopeObj : function(){
		$(".content-input-scope-pacte").html(scopeObj.getHtml("Code postal"));
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["FR", "RE", "GP", "GF", "MQ", "YT", "NC", "PM"],
					cp : true
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.markers.getMarker = function (data) {
			//mylog.log("mapObj.getMarker", data.type);
			//alert()
			var imgM = modules.map.assets + '/images/markers/ngo-marker-default.png';
			return imgM;
		};
		mapCustom.popup.default = function(data){
			//mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = modules.map.assets + "/images/thumb/default_organizations.png"; ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='javascript:;' onclick='pacte.joinGroupMap(\""+id+"\");' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup += 	"Rejoindre le collectif"
					popup += '</div></a>';
				popup += '</div>';
			popup += '</div>';
			return popup;
		};
	},
	joinGroupMap : function(id){
		groupSelected=resultMapGroup[id];
		scopeObj.selected=groupSelected.scope;
		$.each(groupSelected.scope, function(e,v){
			zoneName= (typeof v.cityName != "undefined") ? v.cityName : v.name ;
		});
		pacte.launchRegister({exist : true}, zoneName);
	},
	launchRegister : function(result, city){
		var msgHeader="<span class='text-justify'>Bienvenue ! <br/><br/></span>";/*<span class='text-justify'>Bienvenue ! <br/>Pour encourager les candidat.es à adopter des mesures du Pacte dans leurs programmes,<br/> des habitant.es se réunissent pour former des collectifs locaux.<br/><br/>Les collectifs locaux s’engagent à respecter une charte éthique.</span>"+
				"<br/><br/><a href='https://nextcloud.transition-citoyenne.org/index.php/s/7ywrCwNPF8TckPN' target='_blank' style='width:60%;color:#5b2649 !important;padding:5px 10px;' class='bg-white text-purple'>Lire la charte</a><br/><br/>"; */
		var modalCandidate='<div class="modal fade portfolio-modal" role="dialog" id="modalIsCandidate" style="background-color:rgba(0,0,0,0.4);">'+
				'<div class="modal-dialog" style="margin-top:150px;">'+
			        '<div class="modal-content" style="border-radius:20px;">'+
				        '<div class="close-modal" data-dismiss="modal" style="width: 35px;height: 35px;top: 7px;right: 25px;">'+
				         	'<div class="lr" style="height: 30px;">'+
				                '<div class="rl" style="height: 30px;">'+
				               	'</div>'+
				            '</div>'+
				        '</div>'+
			            '<div class="modal-body center margin-top-20">'+
			                '<h4 class="no-margin text-center">Êtes-vous candidat⋅e aux élections municipales de mars 2020 ?</h4>'+
			                '<div style="margin-top:20px;width:100%;text-align:center;">'+
			                	'<button type="button" class="btn btn-default bg-purple text-white btnCandidateQuestion margin-right-10" data-value="true" style="padding: 5px 25px;font-size: 18px;font-weight: 700;">Oui</button>'+
			                	'<button type="button" class="btn btn-default bg-purple text-white btnCandidateQuestion margin-left-10" data-value="false" style="padding: 5px 25px;font-size: 18px;font-weight: 700;">Non</button>'+
			                '</div>'+
			            '</div>'
			        '</div>'+
			    '</div>'+
			'</div>';
		if($("#modalIsCandidate").length <=0)
			$(".main-container").append(modalCandidate);
		$("#modalIsCandidate").modal("show");
		$(".form-register .footerNoneCandidate").remove();
		$(".btnCandidateQuestion").off().on("click",function(){
			$("#modalIsCandidate").modal("hide");
			if($(this).data("value")===true){
				$(".form-register .msgGroup").remove();
			    $('#modalRegister').modal("show");
			    var params={};
			    if($(".form-register input[name='isCandidate']").length <=0)
					$(".form-register").append("<input type='hidden' name='isCandidate' id='isCandidate' value='1'>");
				else
					$(".form-register input[name='isCandidate']").val(1);
				$('.form-register .agreeContent').hide();
				$('.form-register #newsletter').prop('checked', false);
				$('.form-register .receiveNewsletter').hide();
				btnSignerExplain="<br/><br/><a href='https://www.pacte-transition.org/#communiquer?preview=poi.5de7cb0d69086484548b4731' target='_blank' style='padding: 5px 35px;color: #5b2649 !important;border-radius: 5px;' class='bg-white text-purple'>Comment signer ?</a><br/><br/>";
			    if(result.exist){
			    	params.elt=result.elt;
			    	$(".form-register .emailRegister").after("<div class='msgGroup'>"+
						"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe</label>"+
						"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
		    		"</div>");
		    		params.msgHeader=msgHeader+'<span class="text-justify">Des habitant⋅es de <b>'+city+'</b> se mobilisent déjà.<br/><br/>'+
		  				'En tant que candidat·e, vous pouvez signer un Pacte pour la transition en vous engageant auprès d’eux.'+
		  				btnSignerExplain+
		  				'Pour suivre leurs avancées ou les rencontrer, écrivez-leur ici :</span>';

		    		/*'<span class="text-justify">Pour préserver l’indépendance de ces collectifs, les candidat⋅es ne peuvent pas en faire partie.'+
		  				' Cependant, nous vous invitons à vous inspirer des 32 mesures du Pacte pour constituer votre programme.<br/><br/>'+
		  				'Pour être référencée comme signataire du Pacte pour la Transition sur notre site, votre liste doit toutefois collaborer avec un collectif local créé dans votre commune.<br/><br/>'+
		  				'Plusieurs habitant.es de <b>'+city+'</b> se sont déjà regroupé.es pour définir leurs priorités pour la commune.<br/><br/>'+
		  				'Remplissez ce formulaire pour entrer en contact avec eux</span>';*/
		  				$('.form-register').find(".createBtn").html("Je souhaite suivre l'avancée du Pacte");
		  		}else{
		  			$('.form-register').find(".createBtn").html("<i class='fa fa-sign-in'></i> S'inscrire");
		  			params.elt=result.elt;
		  			params.msgHeader=msgHeader+'<span class="text-justify">Aucun·e habitant·e ne s’est encore signalé·e à <b>'+city+'</b>.<br/><br/>'+
		  				'Nous vous invitons à vous inspirer des 32 mesures du Pacte pour constituer votre programme.'+
		  				btnSignerExplain+
		  				'Remplissez ce formulaire pour rester informé·e :</span>';

		  			/*'<span class="text-justify">Pour préserver l’indépendance de ces collectifs, les candidat⋅es ne peuvent pas en faire partie.'+
		  				' Cependant, nous vous invitons à vous inspirer des 32 mesures du Pacte pour constituer votre programme.<br/><br/>'+
		  				'Pour être référencée comme signataire du Pacte pour la Transition sur notre site, votre liste doit toutefois collaborer avec un collectif local créé dans votre commune.<br/><br/>'+
		  				'Aucun collectif ne semble encore constitué à <b>'+city+'</b>.<br/><br/>'+
		  				'Remplissez ce formulaire pour rester informé⋅e de la suite !</span>';*/
					
				}
		  		if($(".form-register .info-register-form").length > 0)
		  			$('.form-register .info-register-form').html(params.msgHeader);
		  		else
					$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");

				Login.runRegisterValidator();
			}else{
				$(".form-register .msgGroup").remove();
				    $('#modalRegister').modal("show");
				    var params={};
				    $('.form-register .agreeContent').show();
				    $('.form-register').find(".createBtn").html("<i class='fa fa-sign-in'></i> S'inscrire");
				    if($(".form-register input[name='isCandidate']").length <=0)
						$(".form-register").append("<input type='hidden' name='isCandidate' id='isCandidate' value='0'>");
					else
						$(".form-register input[name='isCandidate']").val(0);
					
					footerStr="<div class='footerNoneCandidate text-center' style='width:100%;margin-top:110px;'>"+
						"<a href='https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1' target='_blank' class='bg-purple' style='width: 100%;border-radius: 10px;font-size: 22px;padding: 10px 25px;color: white !important;text-decoration: none;'>"+
							"Rester seulement informé·e des avancées du Pacte"+
						"</a>";
				    if(result.exist){
				    	params.elt=result.elt;
				    	footerStr+="<br/><br/><span class='text-justify margin-top-10 text-purple' style='font-size:16px;'>Pour écrire directement au collectif, envoyer un mail à<br/>"+
							"<a href='mailto:"+result.elt.email+"' class='text-orange bold'>"+result.elt.email+"</a>.</span>";
				
			  			params.msgHeader=msgHeader+'<span class="text-justify">Plusieurs habitant·es de <b>'+city+'</b> se mobilisent déjà !<br/><br/>'
				  				+'Pour suivre leurs avancées, ou les rejoindre, inscrivez-vous à la liste de diffusion locale en leur écrivant ici.</span>';

			  			/*'<span class="text-justify">Plusieurs habitant·es de <b>'+city+'</b> se sont déjà regroupé.es pour définir leurs priorités pour la commune.'
				  				+'<br/>Remplissez ce formulaire pour les rejoindre.</span>';*/
				  		$(".form-register .emailRegister").after("<div class='msgGroup'>"+
							"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe *</label>"+
							"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
			    		"</div>");
			  		}else{
			  			params.elt=result.elt;
			  			params.msgHeader=msgHeader+'<span class="text-justify">Vous êtes le·la premièr·e habitant·e de <b>'+city+'</b> à vous signaler !<br/><br/>'+
			  				'Si vous souhaitez créer le collectif local de '+city+', remplissez ce formulaire. Nous vous mettrons en contact avec les prochaines personnes de votre commune qui s’inscriront.<br/><br/>'+
							'</span>';
			  		}
			  		footerStr+="</div>";
			  		$(".form-register .form-register-inputs").append(footerStr);
			  		if($(".form-register .info-register-form").length > 0)
			  			$('.form-register .info-register-form').html(params.msgHeader);
			  		else
						$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");
					Login.runRegisterValidator();
			}
		});
		/*bootbox.dialog({
            message: 'Êtes-vous candidat⋅e aux élections municipales de mars 2020 ?',
	            buttons: {
	                success: {
	                    label: "Oui",
	                    className: "btn-secondary",
	                    callback: function () {
	                    	$(".form-register .msgGroup").remove();
						    $('#modalRegister').modal("show");
						    var params={};
						    if($(".form-register input[name='isCandidate']").length <=0)
								$(".form-register").append("<input type='hidden' name='isCandidate' id='isCandidate' value='1'>");
							else
								$(".form-register input[name='isCandidate']").val(1);
							$('.form-register .agreeContent').hide();
						    if(result.exist){
						    	params.elt=result.elt;
						    	$(".form-register .emailRegister").after("<div class='msgGroup'>"+
									"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe</label>"+
									"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
					    		"</div>");
					    		params.msgHeader=msgHeader+'<span class="text-justify">Pour préserver l’indépendance de ces collectifs, les candidat⋅es ne peuvent pas en faire partie.'+
					  				' Cependant, nous vous invitons à vous inspirer des 32 mesures du Pacte pour constituer votre programme.<br/><br/>'+
					  				'Pour être référencée comme signataire du Pacte pour la Transition sur notre site, votre liste doit toutefois collaborer avec un collectif local créé dans votre commune.<br/><br/>'+
					  				'Plusieurs habitant.es de <b>'+city+'</b> se sont déjà regroupé.es pour définir leurs priorités pour la commune.<br/><br/>'+
					  				'Remplissez ce formulaire pour entrer en contact avec eux</span>';
					  				$('.form-register').find(".createBtn").html("Envoyer mon message au collectif");
					  		}else{
					  			$('.form-register').find(".createBtn").html("<i class='fa fa-sign-in'></i> S'inscrire");
					  			params.elt=result.elt;
					  			params.msgHeader=msgHeader+'<span class="text-justify">Pour préserver l’indépendance de ces collectifs, les candidat⋅es ne peuvent pas en faire partie.'+
					  				' Cependant, nous vous invitons à vous inspirer des 32 mesures du Pacte pour constituer votre programme.<br/><br/>'+
					  				'Pour être référencée comme signataire du Pacte pour la Transition sur notre site, votre liste doit toutefois collaborer avec un collectif local créé dans votre commune.<br/><br/>'+
					  				'Aucun collectif ne semble encore constitué à <b>'+city+'</b>.<br/><br/>'+
					  				'Remplissez ce formulaire pour rester informé⋅e de la suite !</span>';
								
							}
					  		if($(".form-register .info-register-form").length > 0)
					  			$('.form-register .info-register-form').html(params.msgHeader);
					  		else
								$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");

							Login.runRegisterValidator();
	                    }
	                },
	                cancel: {
	                  label: "Non",
	                  className: "bg-purple text-white",
	                  callback: function() {
	                  	$(".form-register .msgGroup").remove();
					    $('#modalRegister').modal("show");
					    var params={};
					    $('.form-register .agreeContent').show();
					    $('.form-register').find(".createBtn").html("<i class='fa fa-sign-in'></i> S'inscrire");
					    if($(".form-register input[name='isCandidate']").length <=0)
							$(".form-register").append("<input type='hidden' name='isCandidate' id='isCandidate' value='0'>");
						else
							$(".form-register input[name='isCandidate']").val(0);
					    if(result.exist){
					    	params.elt=result.elt;
				  			params.msgHeader=msgHeader+'<span class="text-justify">Plusieurs habitant·es de <b>'+city+'</b> se sont déjà regroupé.es pour définir leurs priorités pour la commune.'
					  				+'<br/>Remplissez ce formulaire pour les rejoindre.</span>';
					  		$(".form-register .emailRegister").after("<div class='msgGroup'>"+
								"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe</label>"+
								"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
				    		"</div>");
				  		}else{
				  			params.elt=result.elt;
				  			params.msgHeader=msgHeader+'<span class="text-justify">Vous êtes le·la premièr·e habitant·e de <b>'+city+'</b> à vous signaler !<br/><br/>'+
				  				'Si vous souhaitez créer le collectif local de '+city+', remplissez ce formulaire. Nous vous mettrons en contact avec les prochaines personnes de votre commune qui s’inscriront.<br/><br/>'+
								'</span>';
				  		}
				  		if($(".form-register .info-register-form").length > 0)
				  			$('.form-register .info-register-form').html(params.msgHeader);
				  		else
							$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");
						Login.runRegisterValidator();
	                  }
	                }
	              }
	      	});*/
	},
	contractsHtml : function(data){
		html="";
		$.each(data, function(e,v){
			if(typeof v.links != "undefined" && typeof v.links.measures != "undefined")
				countMeasures=Object.keys(v.links.measures).length;
			descriptionMes='';
			if(typeof v.shortDescription != "undefined")
				descriptionMes=v.shortDescription;
			else if(typeof v.description != "undefined")
				descriptionMes=v.description;
			html+="<div class='searchEntity poi poi"+e+" contracts shadow2 col-xs-12' data-hash='#page.type.poi.id."+e+"'>"+
					"<h3 class='entityName col-xs-12'><span class='text-purple'>"+v.name+"</span>";
					if(descriptionMes != "" || typeof v.ownerList != "undefined"){
						html+="<span class='subTiltle'> <b>-</b> ";
						if(descriptionMes != ""){
							html+="<b class='text-orange'>"+descriptionMes+"</b>";
						}
						if(typeof v.ownerList != "undefined"){
							if(descriptionMes!="") html+=" (";
							html+="Tête de liste : <b>"+v.ownerList+"</b>";
							if(descriptionMes!="") html+=" )";
						}
						html+="</span>";
					}
					html+="</h3>";
					html+="<span class='col-xs-12'>Cette liste s’est engagée le <b class='text-purple'>"+v.signedOn+"</b> ";
					if(typeof countMeasures != "undefined")
						html+="sur <b class='text-orange'>"+countMeasures+" mesures.</b><br/></span>";
					html+="<br/></span>";
					if(typeof countMeasures != "undefined"){
						html+="<div class='col-xs-12 contentListMesure"+e+" no-padding' style='display:none;'>";
							$.each(v.links.measures,function(k, mes){
									html+="<div class='col-xs-12'>"+
										"<a href='#page.type.poi.id."+k+"' class='col-xs-12 no-padding lbh-preview-element' style='font-size:18px;'>"+mes.name+"</a>"+
										"<span>Niveau d'ambition "+mes.level+"</span>"+
									"</div>";
							});
							if(typeof v.localMeasures != "undefined" && Object.keys(v.localMeasures).length >0){
								$.each(v.localMeasures,function(k, mes){
									html+="<div class='col-xs-12'>"+
											"<span class='col-xs-12 no-padding bold' style='color: #2C3E50;font-size:18px;'>"+mes.name+"</span>"+
											"<span>"+mes.description+"</span>"+
										"</div>";
								});
							}
							
						html+="</div>";
					}
					html+="<div class='col-xs-12 margin-top-10'>";
							if(typeof countMeasures != "undefined")
								html+="<a href='javascript:;' data-id='"+e+"' data-show='true' class='showMeasures'><i class='fa fa-arrow-down'></i> Voir les mesures</a>";
							if(typeof v.urlContract != "undefined" && notEmpty(v.urlContract))
								html+="<a href='"+v.urlContract+"' target='_blank' class='"+((typeof countMeasures != "undefined") ? 'margin-left-10' : '')+"'><i class='fa fa-file-o'></i> Voir le contrat</a>";
					html+="</div>";
				html+="</div>";
		});
		return html;
	}
};
scopeObj.onclickScope = function () {
	//if($(".content-input-scope-pacte .item-globalscope-checker[data-scope-value='54c09656f6b95c1418008e00cities']").length > 0)
	$(".item-globalscope-checker[data-scope-value='54c09656f6b95c1418008e00cities'], .item-globalscope-checker[data-scope-value='54c09653f6b95c141800849ecities'], .item-globalscope-checker[data-scope-value='54c09653f6b95c141800849ecities'], .item-globalscope-checker[data-scope-value='54c09638f6b95c141800266ccities']").remove();
	if($(".content-input-scope-pacte .item-globalscope-checker").length==0){
		$(".content-input-scope-pacte #footerDropdownGS").html('<label class="text-dark margin-top-5"><i class="fa fa-times"></i> Aucun résultat</label>');
	}
	$(".content-input-scope-pacte .item-globalscope-checker").off().on('click', function(){
		var key = $(this).data("scope-value");
		if( typeof myScopes != "undefined" &&
				typeof myScopes.search != "undefined" &&
				typeof myScopes.search[key]  != "undefined" ){
					var scopeDF = myScopes.search[key];
					var nameZone = (typeof scopeDF.cityName != "undefined") ? scopeDF.cityName : scopeDF.name ;
		}
		scopeObj.selected={};
		scopeObj.selected[key] = myScopes.search[key];
		if(location.hash.indexOf("signatures") >= 0){
			coInterface.showLoader("#results-candidatures");
			$.ajax({
	    	  	type: "POST",
	    	  	url: baseUrl+"/costum/pacte/searchsignatures",
	    	  	data: {
	    	  		scope:scopeObj.selected,
	    	  	},
	    	 	success: function(data){
	    	 		
	    	 		countContracts=Object.keys(data.contracts).length;
	    	 		urlStr="";
	    	 		if(countContracts > 0){
	    	 			plur=(countContracts>1) ? "s" : "";
	    	 			if(typeof data.elt != "undefined" && typeof data.elt.url != "undefined" && notEmpty(data.elt.url)){
	    	 				var urlR = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
							var url= (data.elt.url.match(urlR)) ? data.elt.url : "http://"+data.elt.url;
							urlStr="<br/>Pour suivre ce collectif, rendez-vous sur <a href='"+url+"' class='text-orange' target='_blank'>"+data.elt.url+"</a>";
	    	 			}
	    	 			str="<span class='col-xs-12 resultTitle'>Le collectif local de <b class='text-purple'>"+nameZone+"</b> a signé un Pacte pour la Transition<br/>"+
		    	 				" avec <b class='text-orange'>"+countContracts+" liste"+plur+"</b> candidate"+plur+" !"+
    	 					urlStr+
    	 				"</span>";
	 					str+=pacte.contractsHtml(data.contracts);
	    	 		}else{
	    	 			if(data.exist){
	    	 				if(typeof data.elt.url != "undefined" && notEmpty(data.elt.url)){
		    	 				var urlR = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
								var url= (data.elt.url.match(urlR)) ? data.elt.url : "http://"+data.elt.url;
								urlStr="Pour suivre ce collectif, rendez-vous sur <a href='"+url+"' class='text-orange' target='_blank'>"+data.elt.url+"</a><br/>";
	    	 				}
	    	 				str="<span class='text-justify col-xs-12 resultTitle'>Le <b class='text-purple'>collectif local de</b> <b class='text-orange'>"+nameZone+"</b> n’a pas encore obtenu de signatures.<br/>";
	    	 				str+=	urlStr;
	    	 				str+=	"Pour soutenir <b class='text-purple'>leur action</b> et les aider à obtenir des <b class='text-purple'>signatures</b>,<br/> Écrivez leur via l'adresse <br/> <a href='mailto:"+data.elt.email+"'><b class='text-orange' style='font-size:25px;'>"+data.elt.email+"</b></a> !";
	    	 				str+="</span>";
	    	 				//'<button class="joinGroup btn btn-link bg-purple">Rejoindre le collectif</button>';
		    	 		}else{
		    	 			str='<span class="text-justify col-xs-12 resultTitle">'+
		    	 					'Aucune liste n\'a signé le Pacte dans votre commune ! Par ailleurs, il n’existe pas encore de collectif du Pacte pour la Transition dans votre commune.<br/><br/> <b class="text-orange" style="font-size: 22px;">Vous souhaitez vous engager ?</b><br/>'+
		    	 					'<button class="joinGroup btn btn-link bg-purple text-white margin-top-10" style="border-radius:3px;text-decoration:none;font-size:20px;">C\'est par ici</button>'+
		    	 				'</span>';
		    	 		}
	    	 		}
	    	 		
	    	 		$("#results-candidatures").html(str);
	    	 		$(".showMeasures").off().on("click", function(){
	    	 			idContact=$(this).data("id");
	    	 			if($(".contentListMesure"+idContact).is(":visible")){
	    	 				$(".contentListMesure"+idContact).hide(300);
	    	 				$(this).html("<i class='fa fa-arrow-down'></i> Voir les mesures");
	    	 			}else{
	    	 				$(".contentListMesure"+idContact).show(300);
	    	 				$(this).html("<i class='fa fa-arrow-up'></i> Cacher les mesures");

	    	 			}
	    	 		});
	    	 		coInterface.bindLBHLinks();
	    	 		$(".joinGroup").off().on("click", function(){
	    	 			pacte.launchRegister(data, nameZone);
					});
	    		}
			});
		}else{
			$.ajax({
	    	  	type: "POST",
	    	  	url: baseUrl+"/costum/pacte/checkexist",
	    	  	data: {
	    	  		scope:scopeObj.selected,
	    	  	},
	    	 	success: function(data){
    	 			pacte.launchRegister(data, nameZone);
	    		}
			});
		}
				
			
	});
};

Login.runRegisterValidator = function(params) { 
	
	//alert("runRegisterValidator 3");
	var form4 = $('.form-register');
	var errorHandler3 = $('.errorHandler', form4);
	var createBtn = null;
	$(".form-register .container").removeClass("col-lg-offset-3 col-sm-offset-2 col-lg-6 col-sm-8 col-xs-12");
	/*if($(".form-register .linkFAQ").length <= 0){
		$(".form-register .form-register-inputs").append("<div class='linkFAQ text-center col-xs-12' style=''><span class='text-justify' style='font-size:18px;'>Encore une question ? <br/><br/></span><a href='#faq' target='_blank' class='bg-purple' style='padding:5px 10px; color:white;'>Lire la FAQ</a>");
	}*/
	if($(".form-register .surnameRegister").length <= 0){
	$('.form-register').find(".nameRegister").before('<div class="surnameRegister">'+
                    '<label class="letter-black"><i class="fa fa-address-book-o"></i> Prénom</label>'+
                    '<input class="form-control" id="registerSurname" name="surnname" type="text" placeholder="Prénom"><br/>'+
               '</div>');
	}
	/*if($(".form-register .receiveNewsletter").length <= 0){
		$('.form-register').find(".agreeContent").before('<div class="form-group pull-left margin-bottom-10 receiveNewsletter" style="width:100%;">'+
                    '<div class="checkbox-content pull-left no-padding">'+
                        '<label for="newsletter" class="">'+
                           '<input type="checkbox" class="newsletter" id="newsletter" name="newsletter">'+
                            '<span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>'+
                        	'<span class="newsletterMsg checkbox-msg text-purple pull-left">S\'inscrire à la newsletter du Pacte pour la Transition</span>'+
                       '</label>'+
                    '</div>');
                   

                    /*<!--<div>
                        <label for="agree" class="checkbox-inline letter-red">
                            <input type="checkbox" class="grey agree" id="agree" name="agree">
                            <?php echo Yii::t("login","I agree to the Terms of") ?> 
                            <a href="https://www.communecter.org/doc/Conditions Générales d'Utilisation.pdf" target="_blank" class="bootbox-spp text-dark">
                                <?php echo Yii::t("login","Service and Privacy Policy") ?>
                            </a>
                        </label>
                    </div>-->
                </div><div class="receiveNewsletter">'+
                    '<label class="letter-black"><i class="fa fa-address-book-o"></i> Prénom</label>'+
                    '<input class="form-control" id="registerSurname" name="surnname" type="text" placeholder="Prénom"><br/>'+
               '</div>');
	}*/
	$('.form-register').find(".nameRegister label").html('<i class="fa fa-address-book-o"></i> Nom');
	$('.form-register').find(".nameRegister input").attr("placeholder","Nom");
	$('.form-register').find(".usernameRegister").remove();
	$('.form-register').find(".passwordRegister").remove();
	$('.form-register').find(".passwordAgainRegister").remove();
	$('.form-register').find(".agreeMsg").removeClass("text-red").addClass("text-purple").html("Je m’engage à respecter la <a href='https://nextcloud.transition-citoyenne.org/index.php/s/7ywrCwNPF8TckPN' target='_blank' class='text-orange'>charte des collectifs locaux</a>");
	/*if($(".form-register .newsletterContent").length <= 0){
		checkNewsletter='<div class="form-group pull-left newsletterContent margin-top-10" style="width:100%;">'+
                    	'<div class="checkbox-content pull-left no-padding">'+
                        	'<label for="newsletter" class="">'+
                            	'<input type="checkbox" class="newsletter checkbox-info" id="newsletter" name="newsletter">'+
                            	'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
                        	'</label>'+
                        '</div>'+
                        '<span class="checkbox-msg">Je souhaite recevoir la newsletter du Pacte</span>'+
                    '</div>';
		$('.form-register').find(".agreeContent").after(checkNewsletter);
	}*/
	
	// if(form4.valid())
	// 	form4.destroy();
	form4.validate({
		rules : {
			name : {
				required : true,
				minlength : 2
			},
			email3 : {
				required : { 
				 	depends:function(){
				 		$(this).val($.trim($(this).val()));
				 		return true;
				 	}
				},
				email : true
			}//,
			//agree: {
			//	agreeValidation : true
				/*required:true,
				minlength: 1,
				depends: function() {
					alert();
					return false;
				}*/
				//required : true,
				//checkbox:["element", "elelme"]
				/*minlength : 1,*/
				/*required : function(el){
					//depends:function(){
								alert();
					 			if($(this).is(":checked")){
					 				alert();
					 				return true;
					 				
					 			}else{
					 				alert("false");
					 			}
					 	//	}
					 	}*/
			//}
		},

		//messages: {
		//	agree: trad["mustacceptCGU"],
		//},
		submitHandler : function(form) { 
			//alert("runRegisterValidator 4");
			
			if(!form4.find("#agree").is(":checked") && ($(".form-register #isCandidate").val()!=1 || $(".form-register #isCandidate").val()!="1")){
				var validator = form4.validate();
					validator.showErrors({
  						"agree": "Vous devez vous engager à respecter la charte des collectifs locaux avant de vous inscrire !"
				});
				return false;
			}
			if(($(".form-register #isCandidate").val()!=1 || $(".form-register #isCandidate").val()!="1") && 
				$('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() == ""){
				var validator = form4.validate();
					validator.showErrors({
  						"textMsgGroup": "Pour s'inscrire, Vous devez écrire un message au groupe !"
				});
				return false;
			}
			errorHandler3.hide();
			$(".createBtn").prop('disabled', true);
    		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
			var params = { 
			   "name" : $('.form-register #registerSurname').val()+" "+$('.form-register #registerName').val(),
			   "email" : $(".form-register #email3").val(),
			   "pendingUserId" : pendingUserId
            };
            if($(".form-register #isCandidate").val()==1 || $(".form-register #isCandidate").val()=="1")
            	params.isCandidate=true;
            if($('.form-register #isInvitation').val())
            	params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
		  		params.scope = scopeObj.selected;
		  	}
		  	if($(".form-register #newsletter").is(":checked"))
		  		params.newsletter=true;
		  	if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
		  		params.msgGroup=$(".form-register #textMsgGroup").val();
		  	$.ajax({
	    	  type: "POST",
	    	  url: baseUrl+"/costum/pacte/register",
	    	  data: params,
	    	  success: function(data){
	    		  if(data.result) {
	    		  	//createBtn.stop();
					$(".createBtn").prop('disabled', false);
    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
					$("#registerName").val("");
					$("#username").val("");
					$("#email3").val("");
					$("#password3").val("");
					$("#passwordAgain").val("");
					$("#passwordAgain").val("");
					$("#registerSurname").val("");
					$('#agree').prop('checked', false);
					$('#newsletter').prop('checked', false);
				    if(typeof params.isCandidate != "undefined" && params.isCandidate){
				    	if(typeof params.msgGroup != "undefined" && notEmpty(params.msgGroup))
				    		toastr.success("Votre message a bien été envoyé au collectif local");
    		  			else
    		  				toastr.success("Vous êtes bien inscrit.e à la newsletter du Pacte");
				    }
				    else
				    	toastr.success("Merci, votre inscription a bien été prise en compte. Vous allez être mis en contact par mail avec votre collectif local");
    		  		$('.modal').modal('hide');
    		  		scopeObj.selected={};
	    		  }
	    		  else {
	    		  	toastr.error(data.msg);
	    		  	$('.modal').modal('hide');	    		  	
    		  		scopeObj.selected={};
	    		  }
	    	  },
	    	  error: function(data) {
	    	  	toastr.error(trad["somethingwentwrong"]);
	    	  	$(".createBtn").prop('disabled', false);
    			$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
	    	  	//createBtn.stop();
	    	  },
	    	  dataType: "json"
	    	});
		    return false;
		},
		invalidHandler : function(event, validator) {//display error alert on form submit
			errorHandler3.show();
			$(".createBtn").prop('disabled', false);
    		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
			//createBtn.stop();
		}
	});
};
//jQuery(document).ready(function() {
Login.runRegisterValidator();
//});
function startSearch(indexMin, indexMax, callBack){
	mylog.log("startSearch directory.js", typeof callBack, callBack, loadingData);
	if(loadingData) return;

	loadingData = true;
	//scrollH= 0;
	//MAPREMOVE
	//showIsLoading(true);

	mylog.log("startSearch", searchObject.indexMin, indexMax, searchObject.indexStep, searchObject.types);
	searchObject.indexMin = (typeof indexMin == "undefined") ? 0 : indexMin;
	autoCompleteSearch(indexMin, indexMax, function(){
		inc=0;
		$(".bodySearchContainer.mesures .searchEntityContainer").each(function(){
			title=$(this).find(".entityName").text();
			if(title.indexOf("# ") >= 0)
				$(this).addClass("principes");
		});
		countPR=$(".bodySearchContainer.mesures .searchEntityContainer.principes").length;
		inc=1;
		$(".bodySearchContainer.mesures .searchEntityContainer.principes").each(function(){
			if(countPR==inc)
				$(this).addClass("margin-bottom-20");
			inc++;
		});
		if($(".app-mesures").length > 0 && $(".app-mesures #main-search-bar").length <= 0){
			inpSearch='<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center margin-top-20"><input type="text" class="form-control pull-left text-center main-search-bar" id="main-search-bar" placeholder="Rechercher parmi les mesures">'+
	    		'<span class="text-white input-group-addon pull-left main-search-bar-addon" id="main-search-bar-addon">'+
	        		'<i class="fa fa-arrow-circle-right"></i></div>'+
	    		'</span>';
			$(".app-mesures").prepend(inpSearch);
			searchInterface.setSearchbar();
		}
		if($(".bodySearchContainer.mesures .searchEntityContainer").length <= 0){
			$("#dropdown_search").append("<span class='text-purple col-xs-12 text-center margin-top-50' style='text-transform:uppercase;font-size:20px;'>Aucun résultat</span>");
        }  	
    });
    
}
