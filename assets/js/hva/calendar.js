agenda.heightScroll = 900;
agenda.noResult = true;
agenda.nbLaunchSesearch = 2;
agenda.getDateHtml = function(startMoment){
  // mylog.log("agenda.getDateHtml");
  return "<div class='col-xs-12 margin-top-5' style='background-color : #3b9ca263; '><center>"+
          "<h5>"+moment(startMoment).locale("fr").format('dddd DD MMMM')+"</h5></center></div>";
}

calendar.init=function(domCal, initDate, initMode){
    	//mylog.log(" calendar2 init ", domCal);
      if(notNull(domCal))
        calendar.domTarget=domCal;
      //INIT DATE IF NOT CURRENT 
      if(typeof initDate != "undefined" && notNull(initDate) ){
        splitInit=initDate.split("-");
        dateToShow = new Date(splitInit[0], splitInit[1]-1, splitInit[2]);
      }
      else{
        initDate=null;
        dateToShow = new Date();
      }
      // INIT MODE CALENDAR (MONTH , WEEK, DAY)
      if(notNull(initMode))
        calendar.viewMode=initMode;
      $(calendar.domTarget).fullCalendar('destroy');
      $(calendar.domTarget).fullCalendar({
            header : {
                left : 'prev,next',
                center : 'title',
                right : 'today, month'
            },
            fixedWeekCount:false,
            height: 'auto',
            lang : mainLanguage,
            year : dateToShow.getFullYear(),
            month : dateToShow.getMonth(),
            date : dateToShow.getDate(),
            //gotoDate:moment(initDate),
            editable : false,
            eventBackgroundColor: '#FFA200',
            textColor: '#fff',
            defaultView: calendar.viewMode,
            events : [],
            eventLimit: 3,
            timezone : 'local',
            eventClick: function (calEvent, jsEvent, view) {
              //mylog.log(" calendar2 eventClick", calEvent, jsEvent, view);
              onchangeClick=false;
              link = "#page.type.events.id."+calEvent.id;
             // previewHash=link.split(".");
             // hashT=location.hash.split("?");
              getStatus=searchInterface.getUrlSearchParams();       
              urlHistoric="?preview=events."+calEvent.id;
           //   if($("#entity"+previewHash[4]).length > 0) setTimeout(function(){$("#entity"+previewHash[4]).addClass("active");},200); 
              if(getStatus != "") urlHistoric+="&"+getStatus; 
              history.replaceState({}, null, urlHistoric);
              urlCtrl.openPreview(link);
              //closePopovers();
              // calendar.showPopup=true;
              //popoverElement = $(jsEvent.currentTarget);
            },
            eventRender:function(event, element, view) {
                //mylog.log(" calendar2 eventRender event",event,"element",element, "element", view);
                if(calendar.options.popupRender){
                    popupHtml=calendar.popupHtml(event);
                    element.popover({
                        html:true,
                        animation: true,
                        container:'body',
                        title: event.name,
                        template:calendar.popupTemplate(),
                        placement: 'top',
                        trigger: 'focus',
                        content: popupHtml,
                    });
                    element.attr('tabindex', -1);
                }
            }
      });
};