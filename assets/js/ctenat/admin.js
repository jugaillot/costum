adminPanel.views.importctenat = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/importctenat/', {}, function(){},"html");
};

adminPanel.views.indicators=function(){
	var data={
		title : "Liste des indicateurs",
		types : [ "poi" ],
		forced : {
			"type" : "indicator"
		},
		table : {
            name: {
                name : "Nom"
            },
            actions: {
				name : "Domaine d'actions"
			},
			cibleDD : {
				name : "Cible DD"
			}
        }
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.csv = {
			poi : {
				subtype : "indicator",
				key : "ctenat"
			}
		};
		data.actions={
			update : {
				subType : "indicator"
			},
			delete : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

var listFilterStatusAction = costum.listStatutAction; 
adminPanel.views.actionsCTE=function(){
	var data={
		title : "Gestion des actions",
		id : costum.contextId,
		collection : costum.contextType,
		slug : costum.slug,
		admin : true,
		url : baseUrl+'/costum/ctenat/answersadmin',
		table : {
			project: {
				name : "Projet"
			},
			organization: {
				name : "Porteur"
			},
			actions: {
				name : "Domaine d'actions"
			},
			cibleDD: {
				name : "Cible DD"
			},
			privateanswer :{
				name : "Privé",
				class : "col-xs-1 text-center"
			},
			statusAnswer : {
				name : "Statuts",
				class : "col-xs-1 text-center"
			},
			comment : {
				name : "Commentaires",
				class : "col-xs-1 text-center"
			}
		},
		paramsFilter : {
			container : "#filterContainer",
			filters : {
				status : {
		 			view : "selectList",
		 			type : "filters",
		 			field : "priorisation",
		 			name : "Statuts",
		 			action : "filters",
		 			list : costum.listFilterStatusAction
		 			// list : [ 
						// "Action validée",
						// "Action en maturation",
						// "Action lauréate",
						// "Action refusée",
						// "Action Candidate"
	     //            ]
		 		}
			}
		}
	};
	
	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
		|| (typeof canEdit != "undefined" && canEdit) ){
		data.csv = {
			urls : [
				{
					url : baseUrl+'/costum/ctenat/answerscsv/slug/ctenat/admin/true'
				} 
			]
		};
		// data.pdf = {
		// 	urls : [
		// 		{
		// 			url : baseUrl+'/costum/ctenat/allanswers/slug/'+contextData.slug+"/admin/true"
		// 		} 
		// 	]
		// };
		data.actions={
			statusAnswer : {
				list : costum.listStatutAction
     //            list : [ 
					// "Action validée",
					// "Action en maturation",
					// "Action lauréate",
					// "Action refusée",
					// "Action Candidate"
     //            ]
            },
			privateanswer : true,
			statusAnswer : true,
			pdf : true,
			deleteAnswer : true
		};
	}
			
	ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};
adminPanel.views.territory = function(){
	var data={
		title : "Gestion des Territoires",
		types : [ "projects" ],
		forced : {
			"category" : "cteR"
		},
		table : {
            name: {
                name : "Nom"
            },
            private : { 
            	"name" : "Privé",
        		class : "col-xs-1 text-center"
        	},
            //pdf : { "name" : "PDF"},
            status : { 
            	"name" : "Statuts",
            	class : "col-xs-2 text-center"
        	}
        },
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				fields : {
		 			category : "cteR"
		 		}
			},
			filters : {
				status : {
		 			view : "selectList",
		 			type : "filters",
		 			field : "source.status.ctenat",
		 			name : "Statuts",
		 			action : "filters",
		 			list : [ 
						"Territoire Candidat",
						"Territoire Candidat refusé", 
						"Territoire lauréat", 
						"Dossier Territoire Complet",
						"CTE Signé"
	                ]
		 		}
			}
		}
	};

	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.csv = {
			project : {
				key : "ctenat",
				category : "cteR"
			}
		};
		data["actions"]={
			status : {
                list : [ 
					"Territoire Candidat",
					"Territoire Candidat refusé", 
					"Territoire lauréat", 
					"Dossier Territoire Complet",
					"CTE Signé"
                ]
            },
            //pdf : true,
            private : true,
            aap : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.users = function(){
	var data={
		title : "Gestion des utilisateurs-trices",
		types : [ "citoyens" ],
		table : {
            name: {
                name : "Nom"
            },
            email : { name : "E-mail"}
        }
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			switchToUser : true,
			banUser : true,
			deleteUser : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.organizations = function(){
	var data={
		title : "Les organisations sur le CTE NAT",
		types : [ "organizations" ],
		table : {
            name: {
                name : "Nom"
            },
            tags : { name : "Mots clés"}
        },
        actions : {
        	//banUser : true,
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.community = function(){
    //alert("HZE");
    var data={
        title : "Membres du CTENAT",
        id : costum.contextId,
        collection : costum.contextType,
        types : [ "citoyens"],
        url : baseUrl+'/costum/default/communityadmin',
        invite : true,
        table : {
            name: {
                name : "Membres"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Validation pour être membres",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            }
        },
        actions : {
            admin : true,
            roles : true,
            //relaunchInviation : true,
            disconnect : true
        }
    };
    ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};

adminPanel.views.badges = function(){

	var data={
		title : "Etiquetages (badges)",
		types : [ "badges"],
		table : {
			type:{
				name : "Type",
				notLink : true
			},
			name: {
                name : "Nom",
                notLink : true
            },
			description:{
                name : "Description"
            },
            category:{
                name : "Categorie"
            },
            tags:{
                name : "Mots clés"
            }
            
			//pdf : true,
			//status : true
		}
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			update : true,
			delete : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.communityCter = function(){
	//alert("HZE");
	var data={
		title : "Membres",
		id : contextData.id,
		collection : contextData.type,
		types : [ "citoyens", "organizations"],
		url : baseUrl+'/costum/ctenat/communityadmin',
		table : {
			name: {
				name : "Membres"
			},
			roles : {
				name : "Roles"
			},
			admin : true
		}
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.table["actions"]={
			admin : true,
			roles : true
		};
	}
	ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};

adminPanel.views.importindicateur = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/importindicateur/', {}, function(){},"html");
};

adminPanel.views.importorga = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/importorga/', {}, function(){},"html");
};

adminPanel.views.candidatures = function(){
	//alert("candidatures");
	var data={
		title : "Gestion des actions",
		id : contextData.id,
		collection : contextData.type,
		slug : contextData.slug,
		url : baseUrl+'/costum/ctenat/answersadmin',
		table : {
			project: {
				name : "Projet"
			},
			organization: {
				name : "Porteur"
			},
			// name: {
			// 	name : "Personne référente"
			// },
			// email: {
			// 	name : "E-mail"
			// },
			actions: {
				name : "Domaine d'actions"
			},
			cibleDD: {
				name : "Cible DD"
			},
			privateanswer :{
				name : "Privé",
				class : "col-xs-1 text-center"
			},
			statusAnswer : {
				name : "Statuts",
				class : "col-xs-1 text-center"
			},
			comment : {
				name : "Commentaires"
			}
			// ,
			// pdf : {
			// 	name : "PDF"
			// }
		}
		// ,
  //       actions : {
  //           pdf : true
  //       }
	};
	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
		|| (typeof canEdit != "undefined" && canEdit) ){
		data.actions={
			statusAnswer : {
					list : costum.listStatutAction
     //            list : [ 
					// "Action validée",
					// "Action en maturation",
					// "Action lauréate",
					// "Action refusée",
					// "Action Candidate"
     //            ]
            },
			privateanswer : true,
			statusAnswer : true,
			pdf : true,
			deleteAnswer: true
		};


		data.csv = {
			urls : [
				{
					url : baseUrl+'/costum/ctenat/answersctercsv/slug/'+contextData.slug+"/"
				} 
			]
		};

		data.pdf = {
			urls : [
				{
					url : baseUrl+'/costum/ctenat/allanswers/slug/'+contextData.slug+"/"
				} 
			]
		};
		// data.table["statusAnswer"]={
		// 	name : "Statuts",
		// 	class : "col-xs-1"
		// };
	}
			
	ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};

var states = costum.listStatutAction;

// var states = {
// 	"Action validée" : {
// 		color : "green",
// 		icon : "fa-thumbs-up",
// 		lbl : "Action validée"
// 	},
// 	"Action en maturation" : {
// 		color : "#d8e54b",
// 		icon : " fa-hand-pointer-o",
// 		lbl : "Action en maturation"
// 	},
// 	"Action lauréate" : {
// 		color : "orange",
// 		icon : "fa-question-circle",
// 		lbl : "Action lauréate"
// 	},
// 	"Action refusée" : {
// 		color : "red",
// 		icon : "fa-times",
// 		lbl : "Action refusée"
// 	},
// 	"Action Candidate" : {
// 		color : "red",
// 		icon : "fa-times",
// 		lbl : "Action Candidate"
// 	}
// };
adminDirectory.bindCostum = function(aObj){
	mylog.log("adminDirectory.bindCostum ", aObj);


	$("#"+aObj.container+" .deleteAnswer").off().on("click", function(){
		id = $(this).data("id");
		bootbox.dialog({
		  title: "Confirmez la suppression de la fiche action",
		  message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
		  buttons: [
		    {
		      label: "Ok",
		      className: "btn btn-primary pull-left",
		      callback: function() {
		        getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
		        	//urlCtrl.loadByHash(location.hash);
		        	//pageProfil.views.home();
		        	aObj.search(0);
		        },"html");
		      }
		    },
		    {
		      label: "Annuler",
		      className: "btn btn-default pull-left",
		      callback: function() {}
		    }
		  ]
		});
	});

	$("#"+aObj.container+" .statusBtn").off().on("click", function(){
		mylog.log("adminDirectory.bindCostum .statusBtn ", $(this).data("id"), $(this).data("type"));
		var id = $(this).data("id");
		var type = $(this).data("type");
		var elt = aObj.getElt(id, type) ;
		var listStatus = {};
		var statusElt = "" ;

		if( typeof aObj.panelAdmin.actions.status.list != "undefined" ){
			$.each(aObj.panelAdmin.actions.status.list, function(key, value){
				listStatus[value] = value;
			});
		}

		if(	typeof elt != "undefined" && 
			typeof elt.source != "undefined" && 
			typeof elt.source.status != "undefined" &&
			typeof costum != "undefined" && 
			typeof costum.slug != "undefined" &&
			typeof elt.source.status[costum.slug] != "undefined" &&
			elt.source.status[costum.slug] ){
			statusElt = elt.source.status[costum.slug];
		}
			

		var form = {
			saveUrl : baseUrl+"/costum/ctenat/updatestatuscter",
			dynForm : {
				jsonSchema : {
					title : "Modifier les Statuts",
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal #collection").val(type);
						}
					},
					afterSave : function(data){
						mylog.dir(data);
						// if(typeof data.elt.source != "undefined"){
						// 	elt.source = data.elt.source ;
						// }
						// aObj.setElt(elt, id, type) ;
						// $("#"+type+id+" .status").html( aObj.values.status(elt, id, type, aObj));
						// dyFObj.closeForm();

						if( typeof data != "undefined" &&
							typeof data.project != "undefined" &&
							typeof data.project.source != "undefined" &&
							typeof data.project.source.status != "undefined" &&
							typeof data.project.source.status.ctenat != "undefined" &&
							data.project.source.status.ctenat == "Territoire lauréat"){
							$.ajax({
								url: baseUrl+"/survey/co/index/id/"+elt.slug+"/copy/ficheAction",
								type: 'POST',
								dataType: 'html',
								success: function (obj){
									toastr.success("L'appel à projet a été généré avec succès");
									dyFObj.closeForm();
									aObj.search(0);
								},
								error: function (error) {
									dyFObj.closeForm();
									toastr.error("Une erreur s'est produite. Contactez l'admin");
									aObj.search(0);
								}
							});
						}else{
							dyFObj.closeForm();
							aObj.search(0);
						}
						
						
					},
					properties : {
						//collection : dyFInputs.inputHidden(),
						id : dyFInputs.inputHidden(),
						path : dyFInputs.inputHidden(""),
						value : dyFInputs.inputSelect("Choisir un statut", "Choisir un statut", listStatus, {required : true}),
					}
				}
			}
		};

		var dataUpdate = {
			value : statusElt,
			collection : type,
			id : id,
			path : "source.status."+costum.slug
		};
		mylog.log("adminDirectory .statusBtn form", form);
		dyFObj.openForm(form, "sub", dataUpdate);
	});


	$("#"+aObj.container+" .prioritize").off().on("click", function(){
		mylog.log("adminDirectory .prioritize ", $(this).data("id"), $(this).data("type"));
		var id = $(this).data("id");
		var type = "answers";
		var elt = aObj.getElt(id, type) ;
		var listStatus = states;
		var statusElt = "" ;
		var answerId = $(this).data("id");
		var formId = $(this).data("formid");
		var userId = $(this).data("userid");
		var prioValue = elt.priorisation;

		if(	typeof elt != "undefined" && 
			typeof elt.source != "undefined" && 
			typeof elt.source.status != "undefined" &&
			typeof costum != "undefined" && 
			typeof costum.slug != "undefined" &&
			typeof elt.source.status[costum.slug] != "undefined" &&
			elt.source.status[costum.slug] ){
			statusElt = elt.source.status[costum.slug];
		}
			
		var dataUpdate = {
			value : statusElt,
			collection : type,
			id : id,
			formId : formId,
			answerId : answerId,
			answerSection : "priorisation" ,
			answers : prioValue,
			answerUser : userId 
		};

		var form = {
			saveUrl : baseUrl+"/costum/ctenat/prio",
			dynForm : {
				jsonSchema : {
					title : "Modifier les Statuts",
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal #collection").val(type);
						}
					},
					afterSave : function(data){
						mylog.dir(data);
						dyFObj.closeForm();
						aObj.search(0);
						//aObj.modals.prioritize(aObj);

						//var elt = adminGroup.getElt(answerId, "answers") ;
						// elt.priorisation = data.answer.priorisation;
						// aObj.setElt(elt, answerId, "answers") ;
						// mylog.log("adminDirectory.modals prioritize .prioritize res", "#"+"answers"+answerId+" .statusAnswer" );
						// $("#"+"answers"+answerId+" .statusAnswer").html(aObj.values.statusAnswer(elt, answerId, "answers", aObj));
						// dyFObj.closeForm();
					},
					properties : {
						//collection : dyFInputs.inputHidden(),
						id : dyFInputs.inputHidden(),
						formId : dyFInputs.inputHidden(""),
						answerId : dyFInputs.inputHidden(""),
						answerSection : dyFInputs.inputHidden(""),
						answers : dyFInputs.inputHidden(""),
						answers : dyFInputs.inputHidden(""),
						answers : dyFInputs.inputSelect("Choisir un statut", "Choisir un statut", listStatus, {required : true}),
					}
				}
			}
		};

		
		mylog.log("adminDirectory .statusBtn form", form);
		dyFObj.openForm(form, "sub", dataUpdate);
	});
	return str;
} ;

adminDirectory.values.statusAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values statusAnswer", e, id, type, aObj);
	var col = "white";
	var icon = "fa-star";
	var str = "" ;
	if( typeof e.priorisation != "undefined" && typeof states[e.priorisation] != "undefined" ){
		col = states[e.priorisation].color;
		icon = states[e.priorisation].icon;
		str = e.priorisation ;
	}
	// var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
	// 			'data-formid="'+e.formId+'" '+
	// 			'data-userid="'+e.user+'" ' +
	// 			'class="prioritize  btn btn-default" style="background-color:'+col+'">'+ 
	// 			'<i class="fa fa-2x '+icon+'"></i>'+
	// 		'</a>';
	mylog.log("adminDirectory.values statusAnswer end", str);
	return str;
};

adminDirectory.actions.statusAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions statusAnswer", e, id, type, aObj);

	var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
				'data-formid="'+e.formId+'" '+
				'data-userid="'+e.user+'" ' +
				'class="prioritize  col-xs-12 btn btn-default" >Changer le statut'+ 
			'</a>';
	return str;
};

adminDirectory.actions.deleteAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions.deleteAnswer", e, id, type, aObj);

	var str = '<a href="javascript:;" data-id="'+id+'"'+
				'class="deleteAnswer col-xs-12 btn btn-error" >Supprimer l\'action '+ 
			'</a>';
	return str;
};

adminDirectory.values.actions = function(e, id, type, aObj){
	mylog.log("adminDirectory.values actions", e, id, type, aObj);
	var str = "";
	
	if( typeof e.formId != "undefined" &&
		typeof e.answers != "undefined" && 
		typeof e.answers[ e.formId ] != "undefined" &&
		typeof e.answers[ e.formId ].answers != "undefined" &&
		typeof e.answers[ e.formId ].answers.caracter != "undefined" ){
		

		if(typeof e.answers[ e.formId ].answers.caracter.actionPrincipal != "undefined"){
			str += e.answers[ e.formId ].answers.caracter.actionPrincipal;
		}

		if(typeof e.answers[ e.formId ].answers.caracter.actionSecondaire!= "undefined" &&
			e.answers[ e.formId ].answers.caracter.actionSecondaire.length > 0 ){
			$.each(e.answers[ e.formId ].answers.caracter.actionSecondaire, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
		
	}
	else if( typeof e.domainAction != "undefined" ) {
		if(typeof e.domainAction == "string"){
			str += e.domainAction;
		}else if(typeof e.domainAction != "undefined" &&
			e.domainAction.length > 0 ){
			$.each(e.domainAction, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
	}
	mylog.log("adminDirectory.values actions return", str);
	return str;
};

adminDirectory.values.cibleDD = function(e, id, type, aObj){
	mylog.log("adminDirectory.values cibleDD", e, id, type, aObj);
	var str = "";
	
	if( typeof e.formId != "undefined" &&
		typeof e.answers != "undefined" && 
		typeof e.answers[ e.formId ] != "undefined" &&
		typeof e.answers[ e.formId ].answers != "undefined" &&
		typeof e.answers[ e.formId ].answers.caracter != "undefined" ){
		

		if(typeof e.answers[ e.formId ].answers.caracter.cibleDDPrincipal == "string"){
			str += e.answers[ e.formId ].answers.caracter.cibleDDPrincipal;
		}else if(typeof e.answers[ e.formId ].answers.caracter.cibleDDPrincipal!= "undefined" &&
			e.answers[ e.formId ].answers.caracter.cibleDDPrincipal.length > 0 ){
			$.each(e.answers[ e.formId ].answers.caracter.cibleDDPrincipal, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
		
	} 
	else if( typeof e.objectifDD != "undefined" ) {
		if(typeof e.objectifDD == "string"){
			str += e.objectifDD;
		}else if(typeof e.objectifDD != "undefined" &&
			e.objectifDD.length > 0 ){
			$.each(e.objectifDD, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
	}
	return str;
};


adminDirectory.actions.aap = function(e, id, type, aObj){
	mylog.log("adminDirectory.values aap", e, id, type, aObj);
	var str = '<button data-slug="'+e.slug+'" class="ssmla generateAAPBtn btn btn-open-projects col-xs-12" data-view="generateAAP"><i class="fa fa-pencil"></i> Générer AAP</button>';

	return str;
};

/*pageProfil.views.generateAAP = function(){
	//alert("Generating APP for "+contextData.slug);
	// ajaxPost('#central-container', baseUrl+"/survey/co/index/id/"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type+"/copy/ficheAction",
	// 	null,
	// 	function(){},"html");
	window.open(baseUrl+"/survey/co/index/id/"+contextData.slug+"/copy/ficheAction");
};*/

adminDirectory.values.privateanswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values privateanswer", e, id, type, aObj);
	var str = "";
	if( typeof e.project != "undefined" && typeof e.project.id != "undefined"){
		if( typeof e.project.preferences != "undefined" && 
			typeof e.project.preferences.private != "undefined" && 
			e.project.preferences.private === true ){
			str = '<span id="private'+id+'" class="label label-danger"> Privé </span>';
		}else{
			str = '<span id="private'+id+'" class="label label-success"> Public </span>';
		}
	}
	return str;
};

adminDirectory.actions.privateanswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions privateanswer", e, id, type);
	var val = false ;
	var str = "&nbsp;";
	if( typeof e.project != "undefined" && typeof e.project.id != "undefined"){
		if( typeof e.project.preferences != "undefined" && 
			typeof e.project.preferences.private != "undefined" && 
			e.project.preferences.private === true ){
			val = true;
		}
		str ='<button data-parentid="'+id+'" data-parenttype="'+type+'" data-id="'+e.project.id+'" data-type="'+e.project.type+'" data-private="'+val+'" data-path="preferences.private" class="col-xs-12 privateAnswerBtn btn bg-green-k text-white">Rendre public</button>';
	}
	
	return str ;
};
