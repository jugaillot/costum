<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class AnswersAdminAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$panelAdmin = $_POST;
		$limitMin=0;
		$stepLim=100;
		if(@$_POST["page"]){
			$limitMin=$limitMin+(100*($_POST["page"]-1));
		}

		if( !empty($_POST["admin"]) ){
			$cter = PHDB::find(Project::COLLECTION, array("source.key" => $_POST["slug"], "category" => "cteR"), array("slug"));
		} else {
			$cter[$_POST["id"]] = array("slug" => $_POST["slug"]);
		}

		$search="";
		if(!empty($_POST["name"])){
			$search = trim(urldecode($_POST['name']));
		}

		$panelAdmin["context"] = array(	"id" => $_POST["id"], 
										"collection" => $_POST["collection"], 
										"slug" => $_POST["slug"] );

		$searchRegExp = Search::accentToRegex($search);
		if( !empty($_POST["admin"]) ){
        	$query = array("source.key" => $_POST["slug"] ) ;
        	PHDB::remove( Form::ANSWER_COLLECTION , 
									[ "formId"=>$_POST["slug"], 
									  "answers"=> ['$exists'=>0] ] );
		}
        else {
        	$query = array("formId" => $_POST["slug"] ) ;
        	PHDB::remove( Form::ANSWER_COLLECTION , 
									[ "formId"=>$_POST["slug"], 
									  "answers.".$_POST["slug"].".answers.project.id"=> ['$exists'=>0] ] );
        }
        $querySearch =  array();
		if(!empty($_POST["name"])){
			foreach ($cter as $keyC => $valC) {
				array_push($querySearch,array( '$or' => array(
								array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$valC["slug"].".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$valC["slug"].".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
							)));
			}
			$querySearch = array('$or' => $querySearch);

		}else if(count($cter) > 1){
			$querySearch = array( '$or' => array(
								array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$_POST["slug"].".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$_POST["slug"].".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
							));
			
		}
		

		if(!empty($querySearch))
			$query = array('$and' => array( $query , $querySearch ) ) ;

		if(!empty($panelAdmin['filters'])){
			$query = Search::searchFilters($panelAdmin['filters'], $query);
		}
		
		$params["typeDirectory"]=[ Form::ANSWER_COLLECTION ];
		$params["results"] = array();
		// if(!empty($panelAdmin["types"])){
		// 	foreach ($panelAdmin["types"] as $key => $value) {
		//Rest::json($query); exit;
		
		$answers = PHDB::findAndLimitAndIndex ( Form::ANSWER_COLLECTION , $query, $stepLim, $limitMin);
		$answerList = Form::listForAdmin($answers) ;

		$params["results"]["count"][Form::ANSWER_COLLECTION] = PHDB::count( Form::ANSWER_COLLECTION , $query);

		foreach ($answerList as $key => $value) {
			//Rest::json(); exit;
			//Rest::json($params["results"]["answers"][$key]["answers"][$_POST["slug"]]["answers"]["project"]["id"]); exit;
			if( !empty($answerList[$key]) &&
				!empty($answerList[$key]["project"]) &&
				!empty($answerList[$key]["project"]["id"]) ){
				$p = PHDB::findOneById(Project::COLLECTION, $answerList[$key]["project"]["id"], array("preferences"));
				// Rest::json($p); exit;
				if(!empty($p["preferences"])){
					$answerList[$key]["project"]["preferences"] = $p["preferences"];
					//Rest::json($p); exit;
				}
			}
			$answerList[$key]["countComment"] = PHDB::count(Comment::COLLECTION, array("contextId"=>$key,"contextType"=>Form::ANSWER_COLLECTION));
		}

		$params["results"][Form::ANSWER_COLLECTION] = $answerList ;
		$params["panelAdmin"] = $panelAdmin;
		$page = "groupAdmin";
		if($tpl=="json")
			Rest::json( $params );
		else if(Yii::app()->request->isAjaxRequest)
			echo $controller->renderPartial("/custom/default/".$page,$params,true);
		
		
	}
}
