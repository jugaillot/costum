<?php
class SlideAction extends CAction{
	public function run($s=null) {
		$controller=$this->getController();
		
		$tpl = "costum.views.custom.ctenat.slide";
		if(isset($s) ) 
			$tpl = $tpl."s.".$s;
		
		if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,null,true);              
        else 
    		$controller->render($tpl,null);
        

	}
}