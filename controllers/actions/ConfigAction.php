<?php
class ConfigAction extends CAction
{
    
    public function run($id=null,$type=null,$slug=null, $view=null,$page=null,$test=null)
    { 	

        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
		
        Costum::init ($this->getController(),$id,$type,$slug,$view,$test,"costum/controllers/actions/ConfigAction.php");
        
        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial("costum.views.co.config", ["slug"=>$slug, "el"=> Slug::getElementBySlug($slug)], true);
        else
	  		$controller->render("costum.views.co.config",["slug"=>$slug, "el"=> Slug::getElementBySlug($slug)]);
		

    }
}


