<?php
class ParsingAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $params = CostumDesCostums::parsing($_POST);

        Rest::json($params);
    }
}