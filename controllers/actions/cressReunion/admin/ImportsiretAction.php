<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ImportsiretAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$params = array() ;
		$page = "importsiret";
		if(Yii::app()->request->isAjaxRequest)
			echo $controller->renderPartial("/custom/cressReunion/".$page,$params,true);
		
		
	}
}
