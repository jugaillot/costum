<?php
class IndexAction extends CAction
{
    //$id 
    // if given with a type it concerns an element
    // otherwise it's just costum can be the slugname or the digital id 
    //$type : 
        // if given with an id it concerns an element
    //$slug 
        // it's the slug of an element
    //$init this action can render a result for a page request
    
    public function run($id=null,$type=null,$slug=null, $view=null,$page=null,$test=null)
    { 	

        //TODO faire tout ca dans Costum::init()
    	//$activeCOs = array("cocampagne");
    	$controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";

		//$params = CO2::getThemeParams();
    	//Yii::app()->session['paramsConfig']=$params;

        if(!isset($id) && (!isset($slug) || empty($slug) )) {
          if(!empty(Yii::app()->session["costum"]["contextSlug"]))
            $slug = Yii::app()->session["costum"]["contextSlug"];
          else if(!empty(Yii::app()->session["costum"]["contextType"]) && !empty(Yii::app()->session["costum"]["contextId"])){
            $el = PHDB::findOne( Yii::app()->session["costum"]["contextType"] , 
                    [ "_id" => new MongoId( Yii::app()->session["costum"]["contextId"] ) ] );
            $slug = $el["slug"];
          }
        }
        //var_dump(Yii::app()->session["costum"]);exit;

        Costum::init ($this->getController(),$id,$type,$slug,$view,$test,"costum/controllers/actions/IndexAction.php");
        
        $canEdit = false;
        if( isset(Yii::app()->session["userId"])  && isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"]) )
            $canEdit = Authorisation::canEditItem(Yii::app()->session["userId"],@Yii::app()->session["costum"]["contextType"], @Yii::app()->session["costum"]["contextId"]);
        
        //TODO if no canEdit et test exist then 
          //redirect unTpl 

        $params = ["canEdit" => $canEdit];
        if( isset($test) ){
            $params["tpl"]=$id;
            $params["test"]=$test;
        }

        if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"]))
            $params["el"] = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
        

        if(isset(Yii::app()->session['costum']["hasOpenForm"])){
            
            if(isset(Yii::app()->session['costum']["isMultiForms"])){
                $params["formList"] = Yii::app()->session["costum"]["form"]["ids"];
                $params["formId"] = implode("|", $params["formList"]);

                  //when inputs are interdependant on other inputs
                  $budgetInputList = []; 
                  $allforms = [];
                  $stockBudgetInputs = false;
                  foreach ($params["formList"] as $i => $formId) 
                  {
                    
                    $f = PHDB::findOne(Form::COLLECTION, ["id"=>$formId]);
                    $allforms[$formId] = $f;
                    foreach ($f["inputs"] as $key => $inp) 
                    {
                      if( in_array($inp["type"], ["tpls.forms.cplx.budget"] ) )
                        $budgetInputList[$formId.".".$key] = $formId.".".$key; 
                      if( in_array($inp["type"], [
                                                  "tpls.forms.cplx.tpls.forms.cplx.financementFromBudget",
                                                  "tpls.forms.cplx.tpls.forms.cplx.suiviFromBudget",
                                                  "tpls.forms.cplx.tpls.forms.cplx.decideFromBudget"] ) )
                        $stockBudgetInputs = true;
                      
                    }
                  }
                  if(count($budgetInputList))
                    Yii::app()->session["budgetInputList"] = $budgetInputList;
                  
                  //use the cache 
                  Yii::app()->session["forms"] = $allforms;
                  

            } else {
                if(isset($_GET["form"]))
                    $params["formId"] = $_GET["form"];
                else 
                    $params["formId"] = (isset(Yii::app()->session["costum"]["form"]["id"])) ? Yii::app()->session["costum"]["form"]["id"] : Form::generateOpenForm( $el["slug"] );
            }
            
            
            // $answer = ( isset(Yii::app()->session["costum"]["form"]["canModifyAnswer"]) && isset($_GET['answer'])) ? PHDB::findOne( Form::ANSWER_COLLECTION,[ "_id" => new MongoId($_GET['answer']) ] ) : null;
            $answer = ( isset($_GET['answer'])) ? PHDB::findOne( Form::ANSWER_COLLECTION,[ "_id" => new MongoId($_GET['answer']) ] ) : null;
            $showForm = true;
            $generateAnswer = false;
            $myAnswers = PHDB::find( Form::ANSWER_COLLECTION,[ 
                            "formId"     => $params["formId"],
                            "parentSlug" => $params["el"]["slug"],
                            "user"       => Yii::app()->session["userId"] ] );

           

            if(
              !isset(Yii::app()->session["costum"]["form"]["oneAnswerPerPerson"]) ||
                count($myAnswers) == 0                          
              )
            {
              //si ya qu'une réponse  et  on la charge directe                      

              if(count($myAnswers) == 1 )
                $answer = $myAnswers[ array_keys($myAnswers)[0] ];
              else {
                //sinon on cherche la reponse incomplete 
                foreach ($myAnswers as $id => $ans) 
                {
                  //chercher les reponse incomplete
                  //var_dump(isset( $ans["answers"] ) );
                  if(!isset( $ans["answers"]) && $answer == null )
                    $answer = $ans;
                }
              }
              
              if( $answer == null && count( $myAnswers ) > 1 && !$canEdit )
                $showForm = false;

              //si aucune reponse existe 
              //et qu'on a des input complexe alors on génere une answer 
              if( !isset($_GET['answer']) && $answer == null  )
              {
                if(isset($params["formList"])){
                    $generateAnswer = true;
                }
                else if(isset($form["inputs"])){
                    foreach ($form["inputs"] as $key => $inp) 
                    {
                      if( stripos( $inp["type"] , "tpls.forms.cplx" ) !== false )
                        $generateAnswer = true;
                    }

                }
              }

              if( isset($_GET['answer']) && $_GET['answer'] == "new" )
                    $generateAnswer = true;

              //on pré genere un anwser que quand il n'y en pas 
              //pour permettre le save onBlur , ou s'il y a des cplx forms
              //il ne peut y avoir qu'un seul answer en cours simultanement 

              ///TODO purger ANSwers via CRON
              if( $generateAnswer )
              {
                //var_dump("create new" );
                $ans = [ 
                    "formId" => $params["formId"],
                    "user" => Yii::app()->session["userId"],
                    "parentSlug" => $params["el"]["slug"],
                    "created" => time()
                ];
                if(isset($params["formList"]))
                    $ans["formList"] = count($params["formList"]);
               PHDB::insert(Form::ANSWER_COLLECTION, $ans);
               $answer = PHDB::findOne( Form::ANSWER_COLLECTION, [ "_id" => new MongoId($ans["_id"]) ] );
              }
            
              // var_dump($params["el"]["slug"] );
              // var_dump($params["formId"] );
              // var_dump($answer );
              // var_dump(Yii::app()->getRequest()->getQuery('answer'));
              // exit;

              $params["answer"] = $answer;
              $params["showForm"] = $showForm;
               $params["allAnswers"] = PHDB::find( Form::ANSWER_COLLECTION,[ 
                                                   "formId"     => $params["formId"],
                                                   "parentSlug" => $params["el"]["slug"] ] );
               //var_dump($params["el"]["slug"]);exit;
            //var_dump($params["allAnswers"]);exit;
            } else {
                echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> Une seul réponse n'est possible.</h4>";
                echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/answer/".$myAnswers[0]."'>Votre réponse</a>";
            }

        }
        
  
        //use another page inside the same costum folder
    	if($page && Yii::app()->session['costum']["welcomeTpl"])
        {
            //pages are relative to the current context
            //will sit in the same destination foilder as the welcomeTpl of the costum
            $path = explode(".", Yii::app()->session['costum']["welcomeTpl"]);
            array_pop($path);
            $path[] = $page;
            if(Yii::app()->request->isAjaxRequest){
                echo $controller->renderPartial(implode(".", $path), $params, true);
            }
            else
              echo $controller->render(implode(".", $path), $params, true);
        }
	  	else if( isset(Yii::app()->session['costum']["welcomeTpl"])){
            
            
            //Yii::app()->session['costum']["paramx"] = ["canEdit"=>$canEdit];//$params;
            // var_dump(Yii::app()->session["costum"]["slug"]);
            // var_dump(Yii::app()->session["costum"]["welcomeTpl"]);
            // exit;
            if(Yii::app()->request->isAjaxRequest){
                echo $controller->renderPartial("co2.views.app.welcome", $params, true);
            }
            else
                $controller->render( "co2.views.app.welcome",$params );

        }
	  	else 
	  	    $controller->render("index",[ "canEdit" => $canEdit ] );
		
    	/*if(!empty($id)){
			if(strlen($id) == 24 && ctype_xdigit($id)  )
		        $c = PHDB::findOne( Costum::COLLECTION , array("_id"=> new MongoId($id)));
		    else 
		   		$c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $id));
	   	}else if(@$_GET["host"]){
	   		$c = PHDB::findOne( Costum::COLLECTION , array("host"=> $_GET["host"]));
	   	}*/

	  	//contient des éléments partagé par tout les costums
	  	//echo $this->renderPartial("costum.views.common",array("el"=>$el),true);

    }
}


