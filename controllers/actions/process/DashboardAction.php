<?php
class DashboardAction extends CAction
{
    public function run($sk=null,$answer=null)
    {
    	$controller = $this->getController();    	


    	$tpl = "costum.views.custom.process.dashboard";
    	
    	$answers = PHDB::find( Form::ANSWER_COLLECTION,[ //"formId"     => $params["formId"],
                                              			 "parentSlug" => Yii::app()->session['costum']["contextSlug"] ] );
    	
    	$title = "Observatoire des process Opal";
    	$blocks = [];

    	if(isset($answer)){
    		$answer = PHDB::findOne( Form::ANSWER_COLLECTION,[ "_id" => new MongoId( $answer ) ] );
    		$title = (isset($answer["answers"]["opalProcess1"]["titre"])) ? "Observatoire<br/>".$answer["answers"]["opalProcess1"]["titre"] : "Observatoire sppécifique";
    		$lists = [
				"chiffresDepense" =>[
					"title"=>"<i class='fa fa-2x fa-question-circle'></i><br/>Dépense",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"personne impliquées","icon"=>"group","type"=>"success"],
						["data"=>45,"name"=>"durée","type"=>"danger","icon"=>"clock"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresDecide" =>[
					"title"=>"<i class='fa fa-2x fa-thumbs-up'></i><br/>Décision",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"Pour","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"Contre","type"=>"danger","icon"=>"thumbs-down"],
						["data"=>45,"name"=>"Nombres de Décisions Validés","type"=>"danger","icon"=>"gavel"],
						["data"=>60,"name"=>"Nombres de Décisions Refusés","type"=>"danger","icon"=>"hand-stop-o"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFin" =>[
					"title"=>"<i class='fa fa-2x fa-money'></i><br/>Finances",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"Total à financer","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"Total financé","type"=>"danger","icon"=>"map-marker"],
						["data"=>60,"name"=>"Pourcentage financé","type"=>"danger","icon"=>"money"],
						["data"=>45,"name"=>"Resta à financer","type"=>"danger","icon"=>"money"],
						["data"=>45,"name"=>"Combien de financeurs","type"=>"danger","icon"=>"group"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFollow" =>[
					"title"=>"<i class='fa fa-2x fa-cogs'></i><br/>Suivi",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"nombre de tache","icon"=>"pie-chart","type"=>"success"],
						["data"=>24,"name"=>"nombre d'acteur","icon"=>"group","type"=>"success"],
						["data"=>45,"name"=>"Pourcentage d'avancement","type"=>"danger","icon"=>"fa-hourglass-half"],
						["data"=>34,"name"=>"Nombres de Travaux Validés","type"=>"danger","icon"=>"thumbs-up"],
						["data"=>5,"name"=>"Nombres de Taches Cloturés","type"=>"danger","icon"=>"fa-handshake-o"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"proposal" =>[
					"title"=>"Propositions",
					"data" => [32,65,6,21,10],
					"lbls" => ["Propositions 0","Propositions 1","Propositions 2","Propositions 3","Propositions 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.bar"
				],
				"projects" =>[
					"title"=>"Projects",
					"data" => [32,65,36,1,10],
					"lbls" => ["Projects 0","Projects 1","Projects 2","Projects 3","Projects 4"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"tasks" =>[
					"title"=>"Taches",
					"data" => [32,6,36,21,10],
					"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.line"
				],
				"finance" =>[
					"title"=>"Finance",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.scatter"
				],
			];
    	} else {
	    	/*echo count($answers)." Proposals";
	    	echo "<br/> <b>Proposals per status pie</b>";
	    	echo "<br/>---- validated Proposals";
	    	echo "<br/>---- pending decision Proposals";
	    	echo "<br/>---- pending financing Proposals";
	    	echo "<br/>---- Proposals with pending intents no commited ";
	    	echo "<br/>---- Proposals pending evaluations and intent proposals ";
	    	echo "<br/>---- no participation Proposals";
	    	echo "<br/> <b>Projects per status pie</b>";
	    	echo "<br/>---- active Projects";
	    	echo "<br/>---- finished Projects";
	    	echo "<br/> <b>Tasks per status pie</b>";
	    	echo "<br/>----  for open projects Tasks distribution";
	    	echo "<br/>----  for each worker Tasks distribution per project";
	    	echo "<br/>----  xxx Tasks";
	    	echo "<br/> <b>Fianncement per type or contexte pie</b>";
	    	echo "<br/>----  for open projects Finance distribution per presta/ commons";
	    	echo "<br/>----  for open projects Finance distribution per task types";
	    	echo "<br/>----  for each worker Finances distribution per project";
	    	echo "<br/>----  xxx Tasks";*/
	    	//count 
			
			$lists = [
				"chiffresProp" =>[
					"title"=>"<i class='fa fa-2x fa-question-circle'></i><br/>Propositions",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"validated Proposals","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"pending decision Proposals","type"=>"danger","icon"=>"map-marker"],
						["data"=>60,"name"=>"pending financing Proposals","type"=>"danger","icon"=>"money"],
						["data"=>24,"name"=>"Proposals with pending intents no commited","icon"=>"hourglass-half"],
						["data"=>45,"name"=>"no participation Proposals","icon"=>"group"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresProj" =>[
					"title"=>"<i class='fa fa-2x fa-lightbulb-o'></i><br/>Projets",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"active Projects","icon"=>"thumbs-up","type"=>"refresh"],
						["data"=>45,"name"=>"finished Projects","type"=>"danger","icon"=>"hourglass"],
						["data"=>45,"name"=>"pending Projects","type"=>"danger","icon"=>"pause-circle"],
						["data"=>60,"name"=>"for open projects Tasks distribution","type"=>"danger","icon"=>"cogs"],
						["data"=>24,"name"=>"for each worker Tasks distribution per project","icon"=>"pie-chart"],
						["data"=>24,"name"=>"Shitty Tasks identified","icon"=>"meh-o"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFin" =>[
					"title"=>"<i class='fa fa-2x fa-money'></i><br/>Finances",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"for open projects Finance distribution per presta/ commons","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"for open projects Finance distribution per task types","type"=>"danger","icon"=>"map-marker"],
						["data"=>60,"name"=>"for each worker Finances distribution per project","type"=>"danger","icon"=>"money"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresUsage" =>[
					"title"=>"<i class='fa fa-2x fa-line-chart'></i><br/>Usage",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"utilisateurs par projet","icon"=>"thumbs-up","type"=>"pie-chart"],
						["data"=>45,"name"=>"avergae user time spend","type"=>"danger","icon"=>"clock"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"proposal" =>[
					"title"=>"Propositions",
					"data" => [32,65,6,21,10],
					"lbls" => ["Propositions 0","Propositions 1","Propositions 2","Propositions 3","Propositions 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.bar"
				],
				"projects" =>[
					"title"=>"Projects",
					"data" => [32,65,36,1,10],
					"lbls" => ["Projects 0","Projects 1","Projects 2","Projects 3","Projects 4"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"tasks" =>[
					"title"=>"Taches",
					"data" => [32,6,36,21,10],
					"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.line"
				],
				"finance" =>[
					"title"=>"Finance",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.scatter"
				],
			];
			
		}
		
		foreach ($lists as $ki => $list) 
			{
				$kiCount = 0;
				foreach ($list["data"] as $ix => $v) {
					if(is_numeric($v))
						$kiCount += $v;
					else 
						$kiCount ++;
				}
				$blocks[$ki] = [
					"title"   => $list["title"],
					"counter" => $kiCount,
				];
				if(isset($list["tpl"])){
					$blocks[$ki] = $list;
				}
				else 
					$blocks[$ki]["graph"] = [
						"url"=>$list["url"],
						"key"=>"pieMany".$ki,
						"data"=> [
							"datasets"=> [
								[
									"data"=> $list["data"],
									"backgroundColor"=> Ctenat::$COLORS
								]
							],
							"labels"=> $list["lbls"]
						]
					];
			}
		$params = [
			"title" => $title,
    		"blocks" 	=> $blocks
    	];	
		
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}