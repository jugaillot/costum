<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SmarterreController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'searchurl'  		=> 'costum.controllers.actions.smarterre.SearchUrlAction',
            'crud'              => 'costum.controllers.actions.smarterre.CrudAction',
            'getthermatique'    => 'costum.controllers.actions.smarterre.GetThematiqueAction',
            'getsmarterritoriesaction' => 'costum.controllers.actions.smarterre.GetSmarterritoriesAction'
	    );
	}

	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
