<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    protected function beforeAction($action) {
		//parent::initPage();

    	//login auto from cookie if user not connected and checked remember
	    parent::connectCookie();
	    return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
			'index' 	=> 'costum.controllers.actions.IndexAction',
			'groupadmin'=> 'costum.controllers.actions.default.GroupAdminAction',
			'dashboard' => 'costum.controllers.actions.DashboardAction',
			'config'  	=> 'costum.controllers.actions.ConfigAction',
	    );
	}

	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
			echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
