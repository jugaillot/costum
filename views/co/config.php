<h1 class="text-center">Edit your template</h1>
<style type="text/css">
	.editBtnDiv{
		border: 1px solid #666;
		background-color: #ccc;
		margin-bottom: 5px;
		padding : 5px;
	}
</style>
<div class="col-xs-12">
	<div class="editBtnDiv text-center bg-dark">
		<a class='btn btn-xs btn-primary' href='/costum/co/index/slug/<?php echo $slug ?>/test/costumBuilder'>Changer de Template</a></div>
	</div>
<?php 
	if(isset(Yii::app()->session['costum']['dynForm'])){
		
		foreach (Yii::app()->session['costum']['dynForm']["jsonSchema"]["properties"] as $key => $inp) {
			echo '<div class="editBtnDiv col-xs-12">'.
					'<div class="col-xs-1 ">'.
						"<a class='btn btn-xs btn-primary editBtn' href='javascript:;' data-key='".$key."' data-path='".@$inp["path"]."' data-label='".@$inp["label"]."' ><i class='fa fa-pencil'></i></a></div>".
					'<div class=" col-xs-11">'.@$inp["label"]."</div>".
					"</div>";
		}
		
	}

  ?>
  <div class="editBtnDiv text-center bg-dark">
		<a class='btn btn-xs btn-primary' href='/costum/co/index/slug/<?php echo $slug ?>'>View Costum</a></div>
	</div>
</div>

<script type="text/javascript">
var configDynForm = <?php echo json_encode(Yii::app()->session['costum']['dynForm']); ?>;

var tplCtx = {
	id : "<?php echo $el["id"]?>",
	collection : "<?php echo $el["type"]?>"
}
$(".editBtn").off().on("click",function() { 
	var activeForm = {
        "jsonSchema" : {
            "title" : "Template config",
            "type" : "object",
            "properties" : {
                
            }
        }
    };
    activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
    
    tplCtx.key = $(this).data("key");
    tplCtx.path = $(this).data("path");
    
    activeForm.jsonSchema.save = function () {  
	tplCtx.value = $( "#"+tplCtx.key ).val();
    //alert("#"+tplCtx.key+" : "+$( "#"+tplCtx.key ).val());
	console.log("activeForm save tplCtx",tplCtx);
		if(typeof tplCtx.value == "undefined")
			toastr.error('value cannot be empty!');
		else {
			dataHelper.path2Value( tplCtx, function(params) { 
				$("#ajax-modal").modal('hide');
				location.reload();
			} );
		}

	}
	dyFObj.openForm( activeForm );
});

</script>