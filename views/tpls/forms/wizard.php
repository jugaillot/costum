<?php 
$defaultColor = "#354C57"; 
$structField = "structags";

$keyTpl = "wizard";

$paramsData = [ "title" => "",
                "color" => "",
                "background" => "",
                "nbList" => 2,
                "defaultcolor" => "#354C57",
                "tags" => "structags"
                ];

if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  Yii::app()->session["costum"]["tpls"][$keyTpl][$i];      
    }
}

?>
<div class="col-xs-12 margin-top-20">
<div id="<?php echo $wizid ?>" class="swMain">

    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;
            background-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;      
        }
    </style>

    <ul id="wizardLinks">   
    <?php
    // var_dump($listSteps); exit;
    $activeStep = 0;
    $nextStepValid =true;
    foreach ($formList as $k => $v) {
        $n = "todo";
        $p = null;
        
        $form = PHDB::findOne( Form::COLLECTION,[ "id"=>$v ] ); 
	    if( !empty($form) )
            $n =  $form["name"];
        
        echo "<li>";

            $d = '';
            $lbl = "?";
            // if(isset(Yii::app()->session["costum"]["form"]["hasStepValidations"]) )
            // 	$lbl = "";

            if( (!isset($answer["step"]) && !isset(Yii::app()->session["costum"]["form"]["hasStepValidations"])) || 
            	$k == 0 ||
            	(isset($answer["step"]) && $answer["step"] == "all" ) ||

            	(isset(Yii::app()->session["costum"]["form"]["hasStepValidations"]) && isset($answer["step"]) && $k <= array_search($answer["step"], $formList) ) ||
            	( isset(Yii::app()->session["costum"]["form"]["hasStepValidations"]) && !isset($answer["step"]) && $k <= Yii::app()->session["costum"]["form"]["hasStepValidations"]   )  ) 
            {
            	$d = 'class="done"';
            	$lbl = $k;
            	$l = 'showStepForm(\'#'.$v.'\')' ;
            	$activeStep = $k;
            }
            
            
            echo '<a onclick="'.$l.'" href="javascript:;" '.$d.' >';
            echo '<div class="stepNumber">'.$lbl.'</div>';
            echo '<span class="stepDesc">'.$n.'</span></a>';
        echo "</li>";    
    }
    ?>
    </ul>
    

    <?php  
    foreach ($formList as $k => $v) {
        $hide = ($k==$activeStep) ? "" : "hide";
    ?>
    <div id='<?php echo $v ?>' class='col-sm-offset-1 col-sm-10 sectionStep <?php echo $hide ?>' style="padding-bottom:40px">
    <?php 
	    $form = PHDB::findOne( Form::COLLECTION,[ "id"=>$v ] ); 
	    ?>

<script type="text/javascript">
formInputs["<?php echo $v ?>"] = <?php echo json_encode( $form['inputs'] ); ?>;
var formInputsHere = formInputs;
</script>

	    <?php
	    if( !empty($form) )
	    {   
	        echo '<h1 class="text-center" style="color:'.$color1.'" >'.@$form["name"].'</h1>';
            echo "<div class='text-center'>";
                if(isset(Yii::app()->session["costum"]["cms"][$form["id"]."desc"]))
                  echo htmlentities(Yii::app()->session["costum"]["cms"][$form["id"]."desc"]);
                else 
                    echo "<span style='color:#aaa'>* section description</span>";
                if($canEdit)
                  echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='".@$form["id"]."desc' data-type='textarea' data-markdown='1'  data-path='costum.cms.".@$form["id"]."desc' data-label='Expliquez les objectifs de cette étape ? '> <i class='fa fa-pencil'></i></a>";
            echo "</div>";

	        echo "<div class='markdown'>";
	        	echo $this->renderPartial("costum.views.custom.co.formSection",
	                      [ "formId" => $v,
	                        "form" => $form,
	                        "wizard" => true, 
	                        "answer"=>$answer,
	                        "showForm" => $showForm,
	                        "canEdit" => $canEdit,  
	                        "el" => $el ] ,true );    
	        echo "</div>";
	    } 
	    else 
	    { 
	    	echo "";
	    }
	?>        
    </div>
    <?php  
    }  

    
 ?>

    <script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.forms.wizard");
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    if( localStorage !== null && localStorage.wizardStep !== null)
         showStepForm(localStorage.wizardStep);
    
});

function showStepForm(id){
        $(".sectionStep").addClass("hide");
        $(id).removeClass("hide");      
        localStorage.setItem("wizardStep",id);
    }

       
    </script>


</div>

</div>