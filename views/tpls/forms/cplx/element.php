<?php if($answer){ 
	$keyTpl = "element";
	$kunik = $keyTpl.$key;?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		$paramsData = [ "type" => [
					    	Organization::COLLECTION => "Organization",
					    	Person::COLLECTION 		 => "Person",
					    	Event::COLLECTION 		 => "Event",
					    	Project::COLLECTION 	 => "Project",
							News::COLLECTION 		 => "News",
					    	Need::COLLECTION 		 => "Need",
					    	City::COLLECTION 		 => "City",
					    	Thing::COLLECTION 		 => "Thing",
					    	Poi::COLLECTION 		 => "Poi",
					    	Classified::COLLECTION   => "Classified",
					    	Product::COLLECTION 	 => "Product",
					    	Service::COLLECTION   	 => "Service",
					    	Survey::COLLECTION   	 => "Survey",
					    	Bookmark::COLLECTION   	 => "Bookmark",
					    	Proposal::COLLECTION   	 => "Proposal",
					    	Room::COLLECTION   	 	 => "Room",
					    	Action::COLLECTION   	 => "Action",
					    	Network::COLLECTION   	 => "Network",
					    	Url::COLLECTION   	 	 => "Url",
					    	Place::COLLECTION   => "Place",
					    	Ressource::COLLECTION   => "Ressource",
					    	Circuit::COLLECTION   	 => "Circuit",
					    	Risk::COLLECTION   => "Risk",
					    	Badge::COLLECTION   => "Badge",
					    ],
					    "limit" => 0 ];
		
		if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]) ) {
			if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["limit"]) ) 
				$paramsData["limit"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["limit"];
		}

		$properties = [
                "qui" => [
                    "label" => "Qui...?",
                    "placeholder" => "Qui...",
                ],
                "type" => [
                    "label" => "...Type ?",
                    "placeholder" => "...type...",
                ]
	        ];

		$editBtnL = (Yii::app()->session["userId"] == $answer["user"] 
					&& isset(Yii::app()->session["costum"]["form"]["params"][$kunik])
					&& ( $paramsData["limit"] == 0 || 
						!isset($answer["answers"][$kunik]) || 
						( isset($answer["answers"][$kunik]) && $paramsData["limit"] > count($answer["answers"][$kunik]) ))) 
			? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='answers.".$kunik.".' class='add".$keyTpl." btn btn-default'><i class='fa fa-plus'></i> Ajouter un élément </a>" 
			: "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$keyTpl."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset(Yii::app()->session["costum"]["form"]["params"][$kunik]['type']) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>";
				 ?>

			</td>
		</tr>	
		<?php if(isset($answer["answers"][$kunik]) && count($answer["answers"][$kunik])>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answer["answers"][$kunik]))
		{
			foreach ($answer["answers"][$kunik] as $q => $a) 
			{
				if( $paramsData["limit"] == 0 || $paramsData["limit"] > $q )
				{
					echo "<tr id='".$keyTpl.$q."' class='".$keyTpl."Line'>";
					foreach ($properties as $i => $inp) 
					{
						if( $i == "qui" && isset($a["slug"])) {
							$el = Slug::getElementBySlug($a["slug"]);
							echo "<td><a href='#page.type.".$el["type"].".id.".$el["id"]."' class='lbh-preview-element' >".$el["el"]["name"]."</a></td>";
						} else 
							echo "<td>".$a[$i]."</td>";
					}
				?>
				<td>
					<?php 
					$this->renderPartial( "costum.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => "answers.".$kunik.".".$q,
						"keyTpl"=>$keyTpl
						] );
					?>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
				</td>
				<?php 
					$ct++;
					echo "</tr>";
				}
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $keyTpl ?>.php");
	//can be hacked to apply further costumization
	//is used like a dynFormCostumIn in openForm
	costum.<?php echo $kunik ?> = {
		onload : {"actions" : { "setTitle" : "<?php echo $input["label"] ?>"}},
		afterSave : function(data) { 
			mylog.log("element afterSave",data)
			costum.<?php echo $kunik ?>.connectToAnswer(data);
		},
		connectToAnswer : function ( data ) { 
			mylog.log("costum.<?php echo $kunik ?>.connectToAnswer",data)
			tplCtx.value = {
				type : (data.type) ? data.type : "<?php echo (isset(Yii::app()->session["costum"]["form"]["params"][$kunik]['type'])) ? Yii::app()->session["costum"]["form"]["params"][$kunik]['type'] : ''; ?>",
				id : data.id,
				slug : data.map.slug
			};

		    mylog.log("save tplCtx",tplCtx);
		    
		    if(typeof tplCtx.value == "undefined")
		    	toastr.error('value cannot be empty!');
		    else {
		        dataHelper.path2Value ( tplCtx, function(params) { 
		            $("#ajax-modal").modal('hide');
		            location.reload();
		        } );
		    }
	    }
		// onload : {
  //           "actions" : {
  //           	"hide": {
  //           		"parentfinder" : 1
  //           	}
  //           }
  //       }
	};
	costum.searchExist = function (type,id,name,slug,email) { 
		mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
		var data = {
			type : type,
			id : id,
			map : { slug : slug }
		}
		costum.<?php echo $kunik ?>.connectToAnswer(data);
	};


	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $keyTpl ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            type : {
	                inputType : "select",
	                label : "Définir un type d'élément",
	                options :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
	                value : "<?php echo (isset(Yii::app()->session["costum"]["form"]["params"][$kunik]['type'])) ? Yii::app()->session["costum"]["form"]["params"][$kunik]['type'] : ''; ?>"
	            },
	            limit : {
	                label : "Combien d'éléments peuvent être ajoutés (0 si pas de limite)",
	                value : "<?php echo (isset(Yii::app()->session["costum"]["form"]["params"][$kunik]['limit'])) ? Yii::app()->session["costum"]["form"]["params"][$kunik]['limit'] : ''; ?>"
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    

    //adds a line into answer

    <?php if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]['type']) ) { ?>
    $(".add<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( "<?php echo Element::getControlerByCollection(Yii::app()->session["costum"]["form"]["params"][$kunik]['type']); ?>",null,null,null,costum.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $keyTpl ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( "<?php echo Yii::app()->session["costum"]["form"]["params"][$kunik]['type']; ?>",null, <?php echo $keyTpl ?>Data[$(this).data("key")]);
    });
    <?php } ?>

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $keyTpl ?>
        //then we load default values available in forms.inputs.<?php echo $keyTpl ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $keyTpl ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>