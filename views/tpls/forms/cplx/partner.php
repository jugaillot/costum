<?php if($answer){ ?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		
		$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$paramsData = [ "listQuestion" => [	"acquis" => "Acquis",
							                        "discussion" => "En discussion",
							                        "adiscuter" => "À discuter"],
							    "role" => [ "financeur" => "Financeur",
					                        "expertise" => "Expertise",
					                        "expertise" => "Expertise",
					                        "ressources" => "Ressources" ] ];

		if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]) ) 
			$paramsData =  Yii::app()->session["costum"]["form"]["params"][$kunik];
		$properties = [
                "qui" => [
                    "inputType" => "text",
                    "label" => "Qui...?",
                    "placeholder" => "Qui...",
                    "rules" => [ "required" => true ]
                ],
                "engagement" => [
                    "inputType" => "text",
                    "label" => "...s'engage à quoi ?",
                    "placeholder" => "...s'engage à quoi...",
                    "rules" => [ "required" => true ]
                ],
                "statut" => [
                    "inputType" => "select",
                    "label" => "A cette date, le partenariat est-il acquis ?",
                    "placeholder" => "A cette date est-il acquis? en discussion ? à discuter ?",
                    "options" => $paramsData["listQuestion"],
                    "rules" => [ "required" => true ]
                ],
                "next" => [
                    "inputType" => "text",
                    "label" => "Prochaine étape / action à entreprendre ?",
                    "placeholder" => "Prochaine étape / action à entreprendre",
                    "rules" => [ "required" => true ]
                ],
                "role" => [
                    "inputType" => "select",
                    "select2" => [
                        "multiple" => true
                    ],
                    "label" => "Role",
                    "placeholder" => "Role ?",
                    "options" => $paramsData["role"],
                    "rules" => [ "required" => true ]
                ]
	        ];
	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php if(isset($answer["answers"][$kunik]) && count($answer["answers"][$kunik])>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answer["answers"][$kunik])){
			foreach ($answer["answers"][$kunik] as $q => $a) {

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i]))
							echo implode(",", $a["role"]);
						else
							echo $a[$i];
					}
					echo "</td>";
				}
					
			?>
			<td>
				<?php 
					$this->renderPartial( "costum.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => "answers.".$kunik.".".$q,
						"kunik"=>$kunik
						] );
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}
		 ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Les partenaires et leurs engagements réciproques",
	        "description" : "Etapes clefs de la fiche action",
	        "icon" : "fa-group",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "description" : "Liste de question possible",
	        "icon" : "fa-cog",
	        "properties" : {
	            listQuestion : {
	                inputType : "properties",
	                labelKey : "Clef de la question ",
	                labelValue : "Label de la question affiché",
	                label : "Liste de question possible",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.listQuestion
	            },
	            role : {
	                inputType : "properties",
	                labelKey : "Clef du role affiché",
	                labelValue : "Label du role affiché",
	                label : "Liste de role",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.role
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>