<?php if($answer){ 
$debug = false;
$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

$paramsData = [ 
	"group" => [ 
		"Feature", 
		"Costum", 
		"Chef de Projet", 
		"Data", 
		"Mantenance" ],
	"nature" => [
        "investissement" => "Investissement",
        "fonctionnement" => "Fonctionnement"
    ],
    "amounts" => [
    	"price" => "Price"
    ],
    "estimate" => false 
    ];

if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["group"]) ) 
	$paramsData["group"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["group"];
if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["nature"]) ) 
	$paramsData["nature"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["nature"];
if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["amounts"]) ) 
	$paramsData["amounts"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["amounts"];
if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["estimate"]) ) 
	$paramsData["estimate"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["estimate"];

// if(isset($answers)){
// 	foreach ($answers as $q => $a) {
// 		if(isset($a["group"]))
// 			$paramsData["group"][] = $a["group"];
// 	}
// }


$properties = [
		"group" => [
            "placeholder" => "Groupé",
            "inputType" => "tags",
            "data" => $paramsData["group"],
            "rules" => [ "required" => true ],
            "maximumSelectionLength" => 1
        ],
        "nature" => [
            "placeholder" => "Nature de l’action",
            "inputType" => "select",
            "options" => $paramsData["nature"],
            "rules" => [ "required" => true ]
        ],
        "poste" => [
            "inputType" => "text",
            "label" => "Poste de dépense",
            "placeholder" => "Poste de dépense",
            "rules" => [ "required" => true  ]
        ]
    ];
    foreach ($paramsData["amounts"] as $k => $l) {
    	$properties[$k] = [ "inputType" => "text",
				            "label" => $l,
				            "propType" =>"amount",
				            "placeholder" => $l,
				            //"rules" => [ "required" => true, "number" => true ]
				        ];
    }
    if($debug)var_dump($answers);
    if($debug)var_dump($paramsData);
?>	

<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php 
		
		if( count($answers)>0 ){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answers)){
			foreach ($answers as $q => $a) {

				$tds = "";
				foreach ($properties as $i => $inp) {
					$tds .= "<td>";

					if( $i == "price" ) {
						if(!empty($a["price"]))
							$tds .= "<span id='price".$q."'>".$a["price"]."€</span>";
						if( $paramsData["estimate"] ) 
						{ 
							$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-key='".$key."' data-form='".$form["id"]."' data-pos='".$q."'  class='btn btn-xs btn-primary btnEstimate margin-left-5 padding-10'><i class='fa fa-plus'></i></a>";
							if( isset($a["estimates"] ))	
							{
								foreach ( $a["estimates"] as $uid => $esti ) 
								{
									$selected = ( isset($esti["selected"]) ) ? "success" : "default"; 
									$tds .= "<br/><a href='javascript:;' data-id='".$answer["_id"]."' data-uid='".$uid."' data-price='".$esti["price"]."' data-key='".$key."' data-form='".$form["id"]."' data-pos='".$q."'  class='btn btn-xs btn-".$selected." btnEstimateSelected margin-left-5 padding-10'>".$esti["price"]."€ | ".$esti["days"]."j | ".$esti["name"]."</a>";
								}
							}
						} 	
					}
					else if(isset($a[$i]))
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
					echo $tds;
				?>
					<td>
						<?php 
							$this->renderPartial( "costum.views.tpls.forms.cplx.editDeleteLineBtn" , [
								"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
								"id" => $answer["_id"],
								"collection" => Form::ANSWER_COLLECTION,
								"q" => $q,
								"path" => "answers.".$kunik.".".$q,
								"kunik"=>$kunik ] ); ?>

						<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
					</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}

$totalMap = [];
foreach ( $properties as $i => $inp ) {
	if( isset($inp["propType"]) && $inp["propType"] == "amount" )
		$totalMap[$i] = 0;
}

if(isset($answers)){
	foreach ( $answers as $q => $a ) {	
		foreach ($totalMap as $i => $tot) {
			if(isset($a[$i]))
				$totalMap[$i] = $tot + $a[$i];
		}
	}
}

$total = 0;
foreach ( $totalMap as $i => $tot ) {
	if( $tot != 0 )
		$total = $total + $tot ;
}

if($total > 0){

	

	echo "<tr class='bold'>";
	echo 	"<td colspan=".(count( $paramsData["amounts"] )+2)." style='text-align:right'> TOTAL : </td>";
	echo 	"<td colspan=2>".trim(strrev(chunk_split(strrev($total),3, ' ')))." €</td>";
	echo "</tr>";

	Yii::app()->session["totalBudget"] = $total;

}

	

?>
		</tbody>
	</table>
</div>

<?php if( $paramsData["estimate"] ) {  ?>
<div class="form-estimate" style="display:none;">
  	Proposition de prix : <br/>
	<input type="text" id="priceEstimate" name="priceEstimate" style="width:100%;">
	Durée : <br/>
	<input type="text" id="daysEstimate" name="daysEstimate" style="width:100%;">
</div>
<?php } ?>

<script type="text/javascript">

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answers)) ? $answers : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Budget prévisionnel",
            "icon" : "fa-money",
            "text" : "Décrire ici les principaux postes de dépenses : à quoi correspondent les coûts ? <br/>Préciser ce qui relève des dépenses de fonctionnement et des dépenses d’investissement.<br/>Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	        	var today = new Date();
	            tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });

	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "description" : "Liste de question possible",
	        "icon" : "fa-cog",
	        "properties" : {
	            group : {
	                inputType : "array",
	                label : "Liste des groups",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.group
	            },
	            nature : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des natures possibles",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.nature
	            },
	            amounts : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des prix(ex:par année)",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.amounts
	            },
	            estimate : {
	                inputType : "checkboxSimple",
                    label : "estimate Prices",
                    params : {
                        onText : "Oui",
                        offText : "Non",
                        onLabel : "Oui",
                        offLabel : "Non",
                        labelText : "estimate Prices"
                    },
                    checked : sectionDyf.<?php echo $kunik ?>ParamsData.estimate
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        		 mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


<?php if( $paramsData["estimate"] ) {  ?>
	
	$('.btnEstimateSelected').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		tplCtx.price = $(this).data("price");
		$(this).removeClass('btn-default').addClass("btn-success");

		tplCtx.pathBase = "answers";
    	if( notNull(formInputs [tplCtx.form]) )
    		tplCtx.pathBase = "answers."+tplCtx.form;    

    	tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+$(this).data("uid")+".selected"; 	
        tplCtx.value = true;

		mylog.log("btnEstimateSelected save",tplCtx);
  	 	dataHelper.path2Value( tplCtx, function(){
  	 		tplCtx.path = tplCtx.pathBase+"."+tplCtx.key+"."+tplCtx.pos+".price"; 	
	        tplCtx.value = tplCtx.price;

			mylog.log("btnEstimateSelected save",tplCtx);
	  	 	dataHelper.path2Value( tplCtx, function(){
	  	 		$("#price"+tplCtx.pos).html( tplCtx.price+"€" );
	  	 	 } );
  	 	} );

  	 	

		
	});

    $('.btnEstimate').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.key = $(this).data("key");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-estimate").html(),
	        title: "Voter pour partie",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {

                    	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId; 

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = {
			            	price : $(".bootbox #priceEstimate").val(),
			            	days : $(".bootbox #daysEstimate").val(),
			            	name :  userConnected.name,
			            	date : today
			            };

				    	mylog.log("btnEstimate save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModalRel );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});
<?php } ?>


});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>