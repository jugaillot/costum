<?php 
/*
pour chaque ligne de dépense , on peut 
[X] associer un maitre d'oeuvre et ces type de travaux
[X] le maitre d'oeuvre peut déclaré le degré d'avancement
[X] l'administrateur poeut déclarer le total versé
[X] system de recettage ou validation globale par ligne de depense
[X] lister des sous tache ou todos
[X] connecté les todos à la progression de la ligne concerné

[/] edit les todos
[/] connecté les todos à la progression globale
[/] assigné une date de fin à une todo
[/] assigné les todos à d'autres

[ ] system de validation des todos
[ ] associer une url d'un outil de suivi externe
[ ] no reload sur la colonne total 
[ ] no reload sur la validation 
[ ] contraindre les actions à des roles  
*/

if($answer){ 
	$copy = "opalProcess1.depense";
	if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
		$copy = Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"];
	//var_dump($copy);
	$copyT = explode(".", $copy);
	$copyF = $copyT[0];
	$copy = $copyT[1];

	$budgetKey = $copyT[1];
	$answers = null;	
	//var_dump($budgetKey);
	if($wizard){
		if( $budgetKey ){
			if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0)
			$answers = $answer["answers"][$copyF][$budgetKey];
		} else if( isset($answer["answers"][$copyF][$budgetKey]) && count($answer["answers"][$copyF][$budgetKey])>0 )
			$answers = $answer["answers"][$copyF][$kunik];
	} else {
		if($budgetKey)
			$answers = $answer["answers"][$budgetKey];
		else if(isset($answer["answers"][$kunik]))
			$answers = $answer["answers"][$kunik];
	}

$editBtnL =  "";

$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";


$paramsData = [ 
	"limitRoles" =>["worker","maitreoeuvre"]
];


if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["tpl"]) ) 
	$paramsData["tpl"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["tpl"];
if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
	$paramsData["budgetCopy"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"];
if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"]) ) 
	$paramsData["limitRoles"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"];
if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
	$paramsData["budgetCopy"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"];

$communityLinks = Element::getCommunityByTypeAndId(Yii::app()->session["costum"]["contextType"],Yii::app()->session["costum"]["contextId"]);
$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );

$orgs = [];

foreach ($organizations as $id => $or) {
	if( isset($or["links"]["memberOf"][Yii::app()->session["costum"]["contextId"]]["roles"]) )
		$roles = $or["links"]["memberOf"][ Yii::app()->session["costum"]["contextId"] ]["roles"];
	if( $paramsData["limitRoles"] && !empty($roles))
	{
		foreach ($roles as $i => $r) {
			if( in_array($r, $paramsData["limitRoles"]) )
				$orgs[$id] = $or["name"];
		}		
	}
}
//var_dump($orgs);exit;
$listLabels = array_merge($orgs);//Ctenat::$financerTypeList,

$properties = [
    "worker" => [
        "placeholder" => "Maitre d'oeuvre",
        "inputType" => "select",
        "list" => "workerList",
        "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur"
    ],
    "workType" => [
        "inputType" => "text",
        "label" => "Type de travaux",
        "placeholder" => "Type de travaux",
        "rules" => [ "required" => true ]
    ],
    "poste" => [
        "inputType" => "text",
        "label" => "Contexte",
        "placeholder" => "Contexte",
        "rules" => [ "required" => true ]
    ],
    "todo" => [
        "inputType" => "text",
        "label" => "Todo Liste",
        "placeholder" => "Todo Liste",
        "rules" => [ "required" => true,"number" => true  ]
    ],
    "progress" => [
        "inputType" => "text",
        "label" => "Avancement Travaux",
        "placeholder" => "Avancement Travaux",
        "rules" => [ "required" => true,"number" => true  ]
    ],
    "total" => [
        "inputType" => "text",
        "label" => "Total",
        "placeholder" => "Total",
        "rules" => [ "required" => true,"number" => true ]
    ],
    "validation" => [
        "inputType" => "text",
        "label" => "Validation",
        "placeholder" => "Validation",
        "rules" => [ "number" => true  ]
    ]
];


	?>	
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">

	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info;
				if( !isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["budgetCopy"]) ) 
					echo "<br/><span class='text-red text-center'><i class='fa fa-warning fa-2x'></i> THIS FIELD HAS TO BE CONFIGURED FIRST ".$editParamsBtn."</span>"; ?>
			</td>
		</tr>	
		<?php if($answers){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		$totalProgress = 0;
		$bigTotal = 0;
		$totalPayed = 0;
		$validWork = 0;
		$payedWork = 0;
		$totalTodos = 0;
		$totalTodosDone = 0;
		if($answers){
			foreach ($answers as $q => $a) {

				$trStyle = "";
				$tds = "";
				$todoDone  = 0;
				$todo = 0;
				foreach ($properties as $i => $inp) 
				{
					$tds .= "<td>";
					
					if( $i == "worker"  )
					{
						$valbl = "?";
						$class= "btn btn-default";
						if( isset( $a["worker"] ) )
						{
							$o = PHDB::findOne(Organization::COLLECTION,["_id"=>new MongoId($a["worker"]["id"])],["name","slug"]);	
							$valbl = $o["name"];
							$class="";
						}	
						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='".$class." btnWorker margin-left-5 padding-10'>".$valbl."</a>";
					}

					if( $i == "todo"  )
					{
						$valbl = "<i class='fa fa-plus'></i>";
						$class= "btn btn-default";
						if( isset( $a["todo"] ) ){
							$valbl = count( $a["todo"] );
							$totalTodos += count($a["todo"]);
							foreach ($a["todo"] as $tix => $do) {
								if($do["done"]=="1")
									$todoDone++;
								else 
									$todo++;
							}
							$totalTodosDone += $todoDone;
							$valbl = $todoDone."/".count( $a["todo"] );
						}
						$tds .= "<a href='javascript:;' id='btnTodo".$q."' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='".$class." btnTodo margin-left-5 padding-10'>".$valbl."</a>";
					}
					else if( $i == "line" && isset( $a["financer"]["line"] ) ) 
						$tds .= $a["financer"]["line"];
					else if( $i == "workType" && isset( $a["worker"]["workType"] ) ) 
						$tds .= $a["worker"]["workType"];
					else if( $i == "progress"){
						if(isset($a["todo"])){
							$progress = floor($todoDone*100/count($a["todo"]));
						}
						else {
							$progress = (!empty($a["progress"]) ) ? (int)$a["progress"] : 0;
						}
						$totalProgress += $progress;
						$percol = "warning";
						if( $progress == 100 ){
							$percol = "success";
						}
						$tds .= "<a href='javascript:;' id='todoPerc".$q."' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class=' btnProgress margin-left-5 padding-10'>".$progress."%</a>";
						$tds .= '<div class="progress btnProgress"  data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer" >'.
						  '<div id="todoProgress'.$q.'" class="progress-bar progress-bar-'.$percol.'" style="width:'.$progress.'%">'.
							    '<span class="sr-only">'.$progress.'% Complete</span>'.
						  '</div>'.
						'</div>';
					}
					else if( $i == "total"){ 
						$total = 0;
						$totalPayedHere = 0;
						$amounts = (isset(Yii::app()->session["costum"]["form"]["params"][$budgetKey]["amounts"])) ? Yii::app()->session["costum"]["form"]["params"][$budgetKey]["amounts"] : ["price" => "Price"] ; 
				        foreach ( $amounts as $k => $l) {
					    	if(!empty($a[$k]))
								$total += (int)$a[$k];	
					    }
						
						$bigTotal += $total;
						$color = "default";
						if(isset($a["payed"])){
							if($a["payed"]["status"] == "total"){
								$color = "success";
								$totalPayedHere = $total;
								$payedWork++;
							}
							else if($a["payed"]["status"] == "partly"){
								$color = "warning";
								$totalPayedHere = $a["payed"]["amount"];
							}
							else if($a["payed"]["status"] == "accompte"){
								$color = "warning";
								$totalPayedHere = $a["payed"]["amount"];
							}
							$totalPayed += $totalPayedHere;
						}
						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btnPay margin-left-5 padding-10'>".$total."€</a>";

						$percol = "default";
						$payedPercent = 0;
						if($totalPayedHere != 0){
							$payedPercent = $totalPayedHere * 100 / $total;
							$percol = "warning";
						}
						
						if( $payedPercent == 100 ){
							$percol = "success";
						}
						$tds .= '<div class="progress btnPay" data-id="'.$answer["_id"].'" data-budgetpath="'.$copy.'" data-form="'.$copyF.'" data-pos="'.$q.'"  style="cursor:pointer" >'.
						  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$payedPercent.'%">'.
							    '<span class="sr-only">'.$payedPercent.'% Complete</span>'.
						  '</div>'.
						'</div>';
					}
					else if( $i == "validation"){ 
						$color = "default";
						$valbl = "?";
						$tool= "En attente de validation";
						if( isset($a["validFinal"]) ){
							if( $a["validFinal"]["valid"] == "validated" ){
								$color = "success";
								$valbl = "V";
								$trStyle = "background-color:#e5ffe5";
								$validWork++;
								$tool="Validé sans réserve";
							} else if( $a["validFinal"]["valid"] == "reserved" ){
								$color = "warning";
								$valbl = "R";
								$tool="Validé avec réserves";
							} else if( $a["validFinal"]["valid"] == "refused" ){
								$color = "danger";
								$valbl = "NV";
								$tool="Non validé";
							}
						}

						$tds .= "<a href='javascript:;' data-id='".$answer["_id"]."' data-budgetpath='".$copy."' data-form='".$copyF."' data-pos='".$q."'  class='btnValidateWork btn btn-xs btn-".$color." margin-left-5 padding-10 tooltips' data-toggle='tooltip' data-placement='left' data-original-title='".$tool."'>".$valbl."</a>";
					}else if( isset( $a[$i] ) && is_array($a[$i]) ) 
						$tds .= implode(" , ", $a[$i]);
					else if( isset( $a[$i] ) ) 
						$tds .= $a[$i];
					
					$tds .= "</td>";
				}
					
			?>
			
			<?php 
				$ct++;
				echo "<tr id='".$kunik.$q."' class='".$kunik."Line text-center' style='".$trStyle."'>";
				echo $tds;?>
				<td>
					<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
				</td>
			</tr>
			<?php 
			}
		}
?>
		</tbody>
	</table>

<?php 
$percol = "warning";
$totalProgress = (!empty($answers)) ? $totalProgress / count($answers) : 0;
if( $totalProgress == 100 ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Pourcentage d'avancement Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$totalProgress.'%">'.
	    '<span class="sr-only">'.$totalProgress.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>Pourcentage d'avancement</td>
			<td><?php echo floor($totalProgress) ?>%</td>
		</tr>
		<tr>
			<td>Resta à faire </td>
			<td><?php echo floor(100-$totalProgress) ?>%</td>
		</tr>
		<tr>
			<td>Nombres de Travaux Validés </td>
			<td><?php echo $validWork."/".count($answers) ?></td>
		</tr>
		<tr>
			<td>Nombres de Taches Cloturés </td>
			<td><?php echo $totalTodosDone."/".$totalTodos ?></td>
		</tr>
		
	</tbody>
</table>

<?php 
$percol = "warning";
$bigTotalPercent = (!empty($bigTotal)) ? $totalPayed * 100 / $bigTotal : 0;
if( $bigTotalPercent == 100 ){
	$percol = "success";
}
echo "<h4 style='color:".(($titleColor) ? $titleColor : "black")."'>Suivi des dépenses Globale</h4>".
'<div class="progress " style="cursor:pointer" >'.
  '<div class="progress-bar progress-bar-'.$percol.'" style="width:'.$bigTotalPercent.'%">'.
	    '<span class="sr-only">'.$bigTotalPercent.'% Complete</span>'.
  '</div>'.
'</div>'; ?>

<table class="table table-bordered table-hover  ">
	<tbody class="">
		<tr>
			<td>BUDGET TOTAL</td>
			<td><?php echo trim(strrev(chunk_split(strrev($bigTotal),3, ' '))) ?>€</td>
		</tr>
		<tr>
			<td>TRAVAUX PAYÉS</td>
			<td><?php echo trim(strrev(chunk_split(strrev($totalPayed),3, ' '))) ?> €</td>
		</tr>
		<tr>
			<td>DELTA</td>
			<td><?php echo trim(strrev(chunk_split(strrev($bigTotal-$totalPayed),3, ' '))) ?> €</td>
		</tr>
		<tr>
			<td>Nombres de Travaux Cloturés </td>
			<td><?php echo $payedWork."/".count($answers) ?></td>
		</tr>
	</tbody>
</table>

</div>

<div class="form-worker" style="display:none;">
	<select id="worker" style="width:100%;">
		<option>Choisir un maitre d'oeuvre</option>
		<?php foreach ($orgs as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
	</select>
	<br>
	<span class="bold">Type de travaux effectués :</span> <br/>
	<input type="text" id="workType" name="workType" style="width:100%;">
	<br><br>
	<span class="bold">Organisme qui effectue les travaux , s'il n'existe pas, créez le et ajoutez le à la communauté ici 
		<a class="btn btn-primary">Ajouter un maitre d'oeuvre</a>
	</span>
	<br>
</div>

<div class="form-progress" style="display:none;">
	<select id="progress" style="width:100%;">
		<option> DEGRÉ D'AVANCEMENT DE CE CHANTIER </option>
		<?php foreach ([25,50,75,100] as $v => $f) {
			echo "<option value='".$f."'>".$f."%</option>";
		} ?>
	</select>
</div>

<div class="form-pay" style="display:none;">
  	<select id="status" style="width:100%;">
		<option> État du paiement </option>
		<?php foreach (["accompte" => "Accompte avancé",
						"partly" => "Facture payées",
						"total" => "Soldé" ] as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
		</select>
		<br/><br/>

	<span class="bold">Total des Montants payés :</span> <br/>
	<input type="text" id="amount" name="amount" style="width:100%;">
</div>

<div class="form-validate-work" style="display:none;">
  	<select id="validWork" style="width:100%;">
		<option> Valider ces travaux </option>
		<?php foreach (["validated" => "Validé sans réserve",
						"reserved" => "Validé avec réserves",
						"refused" => "Non validé" ] as $v => $f) {
			echo "<option value='".$v."'>".$f."</option>";
		} ?>
		</select>
</div>

<style type="text/css">
	.strike {text-decoration: line-through;}
	input[type=checkbox]
	{
	  /* Double-sized Checkboxes */
	  -ms-transform: scale(0.6); /* IE */
	  -moz-transform: scale(0.6); /* FF */
	  -webkit-transform: scale(0.6); /* Safari and Chrome */
	  -o-transform: scale(0.6); /* Opera */
	  transform: scale(0.6);
	  padding: 10px;
	  vertical-align: middle;
	}
	#todo-list{ list-style: none}
	#todo-list li{ border-bottom: 1px solid #ccc;padding: 5px;}
</style>
<div class="form-todo" style="display:none;">
	<!-- Url externe : <br/>
	<input type="text" id="todoUrl" name="todoUrl" style="width:100%;">
	<br/> -->
	<form id="form-add-todo" class="form-add-todo">
		<!-- <label for="todo">To do:</label> -->
		<input type="text" id="new-todo-item" class="new-todo-item col-xs-6" name="todo" placeholder="Quoi" />
		<input type="date" id="new-todo-item-date" class="new-todo-item-date col-xs-4" name="when"  placeholder="Pour quand"/>
		<input type="text" id="new-todo-item-who" class="new-todo-item-who col-xs-2" name="who"  placeholder="Qui"/>
		<!-- <input type="submit" id="add-todo-item" class="add-todo-item" value="ADD" /> -->
	</form>

	<form id="form-todo-list">
		<ul id="todo-list" class="col-xs-12 todo-list"></ul>
	</form>
</div>


<?php 
if( isset(Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"])){
	//if( Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		$this->renderPartial( "costum.views.".Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"] , 
		[ "totalFin"   => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">
if(typeof costum.lists == "undefined")
	costum.lists = {};
//costum.lists.financerTypeList = <?php //echo json_encode(Ctenat::$financerTypeList); ?>;
costum.lists.workerList = <?php echo json_encode($orgs); ?>;


var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$copy])) ? $answer["answers"][$copy] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            // financerTypeList : {
	            //     inputType : "properties",
	            //     labelKey : "Clef",
	            //     labelValue : "Label affiché",
	            //     label : "Liste des type de Fiannceurs",
	            //     values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            // } , 
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                value :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            },
	            budgetCopy : {
	            	label : "Input Bugdet",
	            	inputType : "select",
	            	options :  costum.lists.budgetInputList
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //mylog.log(".edit<?php echo $kunik ?>Params",tplCtx,sectionDyf.<?php echo $kunik ?>ParamsData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
    $('.btnValidateWork').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-validate-work").html(),
	        title: "Validation des travaux",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".validFinal";	   
			        	
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			valid : $(".bootbox #validWork").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnValidateWork save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModalRel );
				  	 }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

    $('.btnWorker').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-worker").html(),
	        title: "Choix du maitre d'oeuvre",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".worker";   

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			id : $(".bootbox #worker").val(),
		        			name : $(".bootbox #worker option:selected").text(),
		        			workType : $(".bootbox #workType").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnFinancer save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModal );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});
	$('.btnProgress').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		prioModal = bootbox.dialog({
	        message: $(".form-progress").html(),
	        title: "Degrés d'avancement",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".progress";   

			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = $(".bootbox #progress").val();
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnProgress save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModalRel );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

	$('.btnPay').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		mylog.log("btnPay open",tplCtx);
		prioModal = bootbox.dialog({
	        message: $(".form-pay").html(),
	        title: "Statuts des réglements",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".payed";   
						
			        	var today = new Date();
						today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
			            tplCtx.value = { 
		        			status : $(".bootbox #status").val(),
		        			amount : $(".bootbox #amount").val(),
		        			user : userId,
		        			date : today
		        		};
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnPay save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModalRel );
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.modal("show");
	});

	$('.btnTodo').off().click(function() { 
		tplCtx.pos = $(this).data("pos");
		tplCtx.budgetpath = $(this).data("budgetpath");
		tplCtx.collection = "answers";
		tplCtx.id = $(this).data("id");
		tplCtx.form = $(this).data("form");
		mylog.log("btnTodo open",tplCtx);

		prioModal = bootbox.dialog({
	        message: $(".form-todo").html(),
	        title: "Todo Liste",
	        size: "large",
	        show: false,
	        buttons: {
                success: {
                    label: trad.save,
                    className: "btn-primary",
                    callback: function () {
			            
			        	var formInputsHere = formInputs;
			        	tplCtx.path = "answers";
			        	if( notNull(formInputs [tplCtx.form]) )
			        		tplCtx.path = "answers."+tplCtx.form;    

			        	tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".todo";   
						
			        	
				        tplCtx.value = [];
				        var todoDone = 0;
			            $('.bootbox #todo-list').find('li').each(function() {
			            	var t = { 
			        			what : $(this).find(".liText").text(),
			        			uid : $(this).data("uid"),
			        			who : $(this).data("who"),
			        			when : $(this).data("when"),
			        			created : $(this).data("created"),
			        			done : ( $(this).find(".todo-item-done").is(":checked") ) ? "1" : "0"
			        		}
			        		if( $(this).find(".todo-item-done").is(":checked") )
			        			todoDone++;
							tplCtx.value.push(t);
						});
						answerObj.answers[tplCtx.form][tplCtx.budgetpath][tplCtx.pos].todo = tplCtx.value;
						$("#btnTodo"+tplCtx.pos).html(todoDone+"/"+tplCtx.value.length);
						$("#todoProgress"+tplCtx.pos).css("width",Math.floor(todoDone*100/tplCtx.value.length)); 
						$("#todoPerc"+tplCtx.pos).html(Math.floor(todoDone*100/tplCtx.value.length)+"%");
		        		delete tplCtx.pos;
		        		delete tplCtx.budgetpath;
				    	mylog.log("btnPay save",tplCtx);
				  	 	dataHelper.path2Value( tplCtx, closePrioModal);
			        }		
                },
                cancel: {
                  label: trad.cancel,
                  className: "btn-secondary",
                  callback: closePrioModal
                }
              },
		        onEscape: closePrioModal
	    });
	    prioModal.on('shown.bs.modal', function (e) {
			$(".bootbox #add-todo-item").off().on('click', function(e){
				e.preventDefault();
				addTodoItem()
			});
			$(".bootbox #new-todo-item, .bootbox #new-todo-item-when, .bootbox #new-todo-item-who").off().on('keypress', function(e){
				var keycode;
				if (window.event) {keycode = window.event.keyCode;e=event;}
				else if (e){ keycode = e.which;}
				if(keycode=="13")
					addTodoItem();
			});
			$(".bootbox #todo-list").on('click', '.todo-item-delete', function(e){
				var item = this; 
				deleteTodoItem(e, item)
			})
			$(".bootbox #todo-list").on('click', '.todo-item-edit', function(e){
				var item = this; 
				editTodoItem(e, item)
			})

			$(".bootbox .todo-item-done").on('click', completeTodoItem);

			todoPath = "answerObj.answers."; 
			if( notNull(formInputs [tplCtx.form]) )
				todoPath = "answerObj.answers."+tplCtx.form+"."; 
			todoPath += tplCtx.budgetpath+"["+tplCtx.pos+"].todo";
			mylog.log("build todos path ",todoPath);
			if( notNull(todoPath ) )
			{
				var todos = eval( todoPath );
				mylog.log("build todos",todos);
				$.each(todos, function(ix,todo) { 
					buildTodo( todo.what, ( ( parseInt(todo.done) ) ? "checked" : "" ),todo.uid,todo.who,todo.created,todo.when );
				})
			}
	    })
	    prioModal.modal("show");
	});

	
});

function addTodoItem() { 
	var today = new Date();
	today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
	var when = ($(".bootbox #new-todo-item-date").val()) ? $(".bootbox #new-todo-item-date").val() : "";
	var who = ($(".bootbox #new-todo-item-who").val()) ? $(".bootbox #new-todo-item-who").val() : userConnected.name ; 
  	buildTodo($(".bootbox #new-todo-item").val(), "",userId, who, today,when);
 	$(".bootbox #new-todo-item, .bootbox #new-todo-item-when").val("");
}

function buildTodo(todoItem,checked,uid,who,created,when){
	if(typeof uid == "undefined") uid = userId;
	if(typeof who == "undefined") who = userConnected.name;
	if(typeof when == "undefined" || when == "" )
		when = "";
	if(typeof created == "undefined") {
		var today = new Date();
		created = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();	
	}
	$(".bootbox #todo-list").append(
			"<li class='col-xs-12' data-uid='"+uid+"' data-created='"+created+"' data-who='"+who+"'  data-when='"+when+"' >"+
				"<div class='col-xs-1'>"+
					"<a class='btn btn-xs btn-danger todo-item-delete' href='javascript:;'><i class='fa fa-times-rectangle'></i></a>"+
					" <a class='btn btn-xs btn-primary todo-item-edit' href='javascript:;'><i class='fa fa-pencil'></i></a>"+
				"</div>"+
				"<div class='col-xs-1'><input type='checkbox' name='todo-item-done' class='todo-item-done' value='" + todoItem + "' "+checked+" /></div> " + 
	             "<div class='col-xs-6 liText todo-item-edit' data-who='"+who+"'  data-when='"+when+"'>"+todoItem +"</div>"+
	             "<div class='col-xs-4 todo-item-edit' data-who='"+who+"'  data-when='"+when+"'>"+when+" | "+who +"</div>"+
             "</li>");
}

function deleteTodoItem(e, item) {
  e.preventDefault();
  $(item).parent().parent().fadeOut('slow', function() { 
    $(item).parent().parent().remove();
  });
}
function editTodoItem(e, item) {
  e.preventDefault();
  $(".bootbox #new-todo-item").val($(item).parent().parent().find(".liText").text());
  $(".bootbox #new-todo-item-when").val( $(item).data("when") );
  $(".bootbox #new-todo-item-who").val( $(item).data("who") );
  
}

                           
function completeTodoItem() {  alert("strike");
  $(this).parent().toggleClass("strike");
}


function closePrioModal(){
	prioModal.modal('hide');
}
function closePrioModalRel(){
	closePrioModal();
	location.reload();
}
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>