<?php if($answer){ 
	?>
<div class="form-group">
	<table class="table table-bordered table-hover  directoryTable" id="<?php echo $kunik?>">
		
	<?php 
		$editBtnL = (Yii::app()->session["userId"] == $answer["user"]) ? " <a href='javascript:;' data-id='".$answer["_id"]."' data-collection='".Form::ANSWER_COLLECTION."' data-path='".$answerPath."' class='add".$kunik." btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
		
		$editParamsBtn = ($canEdit) ? " <a href='javascript:;' data-id='".$el["_id"]."' data-collection='".Yii::app()->session["costum"]["contextType"]."' data-path='costum.form.params.".$kunik."' class='previewTpl edit".$kunik."Params btn btn-xs btn-danger'><i class='fa fa-cog'></i> </a>" : "";

		$communityLinks = Element::getCommunityByTypeAndId(Yii::app()->session["costum"]["contextType"],Yii::app()->session["costum"]["contextId"]);
		$persons = Link::groupFindByType( Person::COLLECTION,$communityLinks,["name","links"] );
		$organizations = Link::groupFindByType( Organization::COLLECTION,$communityLinks,["name","links"] );
		$financers = array_merge( $persons, $organizations );

		$orgs = [];
		if( !empty($or["links"]["memberOf"][Yii::app()->session["costum"]["contextId"]]["roles"]) ) {
			foreach ($financers as $id => $or) {
				$roles = $or["links"]["memberOf"][Yii::app()->session["costum"]["contextId"]]["roles"];
				if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"]) && !empty($roles))
				{
					foreach ($roles as $i => $r) {
						if( in_array($r, Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"]) )
							$orgs[$id] = $or["name"];
					}		
				}
			}
		}
		//var_dump($orgs);exit;
		$listLabels = array_merge(Ctenat::$financerTypeList,$orgs);

		$paramsData = [ 
			"financerTypeList" => Ctenat::$financerTypeList,
			"limitRoles" =>["Financeur"]
		];

		if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]) ) {
			if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["tpl"]) ) 
				$paramsData["tpl"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["tpl"];
			if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["financerTypeList"]) ) 
				$paramsData["financerTypeList"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["financerTypeList"];
			if( isset(Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"]) ) 
				$paramsData["financersList"] =  Yii::app()->session["costum"]["form"]["params"][$kunik]["limitRoles"];
		}

		$properties = [
              "financerType" => [
                "placeholder" => "Type de Financement",
                    "inputType" => "select",
                    "list" => "financerTypeList",
                    "rules" => [
                        "required" => true
                    ]
                ],
                "financer" => [
                    "placeholder" => "Quel Financeur",
                    "inputType" => "select",
                    "list" => "financersList",
                    "subLabel" => "Si financeur public, l’inviter dans la liste ci-dessous (au cas où il n’apparait pas demandez à votre référent territoire de le déclarer comme partenaire financeur"
                ],
                "title" => [
                    "inputType" => "text",
                    "label" => "Fonds, enveloppe ou budget mobilisé",
                    "placeholder" => "Fonds, enveloppe ou budget mobilisé",
                    "subLabel" => "préciser l’intitulé et la nature du financement",
                    "rules" => [ "required" => true ]
                ],
                "amount2019" => [
                    "inputType" => "text",
                    "label" => "2019 (euros HT)",
                    "placeholder" => "2019 (euros HT)",
                    "rules" => [ "required" => true,"number" => true ]
                ],
                "amount2020" => [
                    "inputType" => "text",
                    "label" => "2020 (euros HT)",
                    "placeholder" => "2020 (euros HT)",
                    "rules" => [ "required" => true,"number" => true  ]
                ],
                "amount2021" => [
                    "inputType" => "text",
                    "label" => "2021 (euros HT)",
                    "placeholder" => "2021 (euros HT)",
                    "rules" => [ "number" => true  ]
                ],
                "amount2022" => [
                    "inputType" => "text",
                    "label" => "2022 (euros HT)",
                    "placeholder" => "2022 (euros HT)",
                    "rules" => [ "number" => true  ]
                ]
        ];

	?>	
	<thead>
		<tr>
			<td colspan='<?php echo count( $properties)+2?>' ><h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label.$editQuestionBtn.$editParamsBtn.$editBtnL?></h4>
				<?php echo $info ?>
			</td>
		</tr>	
		<?php if(isset($answer["answers"][$kunik]) && count($answer["answers"][$kunik])>0){ ?>
		<tr>
			</th>
			<?php 
			
			foreach ($properties as $i => $inp) {
				echo "<th>".$inp["placeholder"]."</th>";
			} ?>
			<th></th>
		</tr>
		<?php } ?>
	</thead>
	<tbody class="directoryLines">	
		<?php 
		$ct = 0;
		
		if(isset($answer["answers"][$kunik])){
			foreach ($answer["answers"][$kunik] as $q => $a) {

				echo "<tr id='".$kunik.$q."' class='".$kunik."Line'>";
				foreach ($properties as $i => $inp) {
					echo "<td>";
					if(isset($a[$i])) {
						if(is_array($a[$i]))
							echo implode(",", $a["role"]);
						else
							echo $a[$i];
					}
					echo "</td>";
				}
					
			?>
			<td>
				<?php 
					$this->renderPartial( "costum.views.tpls.forms.cplx.editDeleteLineBtn" , [
						"canEdit"=>($canEdit||Yii::app()->session["userId"] == $answer["user"]),
						"id" => $answer["_id"],
						"collection" => Form::ANSWER_COLLECTION,
						"q" => $q,
						"path" => "answers.".$kunik.".".$q,
						"kunik"=>$kunik
						] );
					?>
				<a href="javascript:;" class="btn btn-xs btn-primary openAnswersComment" onclick="commentObj.openPreview('answers','<?php echo $answer["_id"]?>','<?php echo $answer["_id"].$key.$q ?>', '<?php echo @$a['step'] ?>')"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>$answer["_id"],"contextType"=>"answers", "path"=>$answer["_id"].$key.$q))?> <i class='fa fa-commenting'></i></a>
			</td>
			<?php 
				$ct++;
				echo "</tr>";
			}
		}

$totalMap = [];
foreach ( $properties as $i => $inp ) {
	if(stripos($i, "amount") !== false )
		$totalMap[$i] = 0;
}

if(isset($answer["answers"][$kunik])){
	foreach ( $answer["answers"][$kunik] as $q => $a ) {	
		foreach ($totalMap as $i => $tot) {
			if(isset($a[$i]))
				$totalMap[$i] = $tot + $a[$i];
		}
	}
}

$total = 0;
foreach ( $totalMap as $i => $tot ) {
	if( $tot != 0 )
		$total = $total + $tot ;
}

if($total > 0){

	echo "<tr class='bold'>";
		echo "<td></td>";
		echo "<td>TOTAL : </td>";
		foreach ($totalMap as $i => $tot) {
			if( $tot != 0 )
				echo "<td>".(($tot == 0) ? "" : trim(strrev(chunk_split(strrev($tot),3, ' ')))."€")."</td>";
		}
		echo "<td></td>";
	echo "</tr>";

	echo "<tr class='bold'>";
	echo 	"<td colspan=5 style='text-align:right'>FINANCEMENT TOTAL : </td>";
	echo 	"<td colspan=2>".trim(strrev(chunk_split(strrev($total),3, ' ')))." €</td>";
	echo "</tr>";

}

	

?>
		</tbody>
	</table>
</div>

<?php 
if( isset(Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"])){
	//if( Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"] == "tpls.forms.equibudget" )
		$this->renderPartial( "costum.views.".Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"] , 
		[ "totalFin"   => $total,
		  "totalBudg" => Yii::app()->session["totalBudget"]["totalBudget"] ] );
	// else 
	// 	$this->renderPartial( "costum.views.".Yii::app()->session["costum"]["form"]["params"]["financement"]["tpl"]);
}
 ?>

<script type="text/javascript">
if(typeof costum.lists == "undefined")
	costum.lists = {};
costum.lists.financerTypeList = <?php echo json_encode(Ctenat::$financerTypeList); ?>;
costum.lists.financersList = <?php echo json_encode($orgs); ?>;

var <?php echo $kunik ?>Data = <?php echo json_encode( (isset($answer["answers"][$kunik])) ? $answer["answers"][$kunik] : null ); ?>;
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

$(document).ready(function() { 
	
	sectionDyf.<?php echo $kunik ?> = {
		"jsonSchema" : {	
	        "title" : "Plan de Financement",
            "icon" : "fa-money",
            "text" : "Décrire ici les financements mobilisés ou à mobiliser. Les coûts doivent être en <b>hors taxe</b>.",
	        "properties" : <?php echo json_encode( $properties ); ?>,
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>.jsonSchema.properties , function(k,val) { 
	        		tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};

	sectionDyf.<?php echo $kunik ?>Params = {
		"jsonSchema" : {	
	        "title" : "<?php echo $kunik ?> config",
	        "icon" : "fa-cog",
	        "properties" : {
	            financerTypeList : {
	                inputType : "properties",
	                labelKey : "Clef",
	                labelValue : "Label affiché",
	                label : "Liste des type de Fiannceurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.financerTypeList
	            } , 
	            limitRoles : {
	                inputType : "array",
	                label : "Liste des roles financeurs",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.limitRoles
	            },
	            tpl : {
	                label : "Sub Template",
	                values :  sectionDyf.<?php echo $kunik ?>ParamsData.tpl
	            }
	        },
	        save : function () {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	        		if(val.inputType == "array")
	        		 	tplCtx.value[k] = getArray('.'+k+val.inputType);
	        		else if(val.inputType == "properties")
	        		 	tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
	        		else
	        		 	tplCtx.value[k] = $("#"+k).val();
	        	 });
	            mylog.log("save tplCtx",tplCtx);
	            
	            if(typeof tplCtx.value == "undefined")
	            	toastr.error('value cannot be empty!');
	            else {
	                dataHelper.path2Value( tplCtx, function(params) { 
	                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
	                    location.reload();
	                } );
	            }

	    	}
	    }
	};


    mylog.log("render","/modules/costum/views/tpls/forms/<?php echo $kunik ?>.php");

    //adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection"); 
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>,null, <?php echo $kunik ?>Data[$(this).data("key")]);
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        //if no params config on the element.costum.form.params.<?php echo $kunik ?>
        //then we load default values available in forms.inputs.<?php echo $kunik ?>xxx.params
        //mylog.log(".editParams",sectionDyf.<?php echo $kunik ?>Params,calData);
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    
});
</script>
<?php } else {
	//echo "<h4 class='text-red'>CALENDAR works with existing answers</h4>";
} ?>