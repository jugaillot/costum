<?php
/**
 * TPLS QUI PERMET D'AFFICHER UN CAROUSEL SUR LES ÉVÈNEMENTS EN COURS DU JOUR + 30
 * BASÉ SUR LE MODELE FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMATRÉ LES LIMITATIONS, DE BASE ELLE SERA À 3
 * @params : 
 *  $limit : limitation concernant le nombre d'affichage d'évènement
 *  $background : définir la couleur du fond
 *  $color : définir la couleur du texte
 */
$keyTpl = "eventCarousel";
$defaultcolor = "white";
$structags = "tags"; 
$limit = (isset(Yii::app()->session["costum"]["tpls"]["eventCarousel"]["nbLimit"])) ? Yii::app()->session["costum"]["tpls"]["eventCarousel"]["nbLimit"] : 3;

$defaultcolor = "white";
$tags = "structags";
$paramsData = [ "title" => "",
                "color" => "",
                "background" => "",
                "defaultcolor" => "white",
                "tags" => "structags",
                "nbLimit" => 3
                ];

if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  Yii::app()->session["costum"]["tpls"][$keyTpl][$i];      
    }
}

?>
<style>
.resume-infoc{
    text-align: left;
    z-index: 10;
    position: absolute;
    padding-top: 7%;
    padding-left: 3%;
    background-color:
    rgba(0,0,0,0.7);
    width: 40%;
    padding-bottom: 100%;
}
#arrow-caroussel{
    margin-top: -9%;
    font-size: 3vw;
    color:white;
    position: absolute;
    width: 40%;
    text-align: center;
}
.carousel-inner{
    height : 500px;
}
.h2-day{
    font-size: 3vw;
}
@media (max-width:768px){
    .carousel-inner{
        height : 200px;
    }
    #arrow-caroussel{
        font-size: 3vw;
        color:white;
    }
}
</style>
<div id="carouselEvent">
    <div id="carousel"  class="carousel slide" data-ride="carousel">
    <?php if(isset(Yii::app()->session["costum"]["tpls"]["eventCarousel"])){
        
        //A EXTERMINER CETTE PARTIE LA DANS UN MODÈLE À PART
        date_default_timezone_set('UTC');

        //On récupère sur ce mois-ci les évènements
        $date_array = getdate ();
        $numdays = $date_array["wday"];

        $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
        $startDate = strtotime(date("Y-m-d H:i"));

        $where = array("source.key"     => Yii::app()->session["costum"]["contextSlug"],
                    "startDate"      => array('$gte' => new MongoDate($startDate)),
                    "endDate"        => array('$lt' => new MongoDate($endDate)));

        $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where, $limit);
        if($allEvent){
        ?>
         <div class="carousel-inner">
         <?php 
         end($allEvent);
         $lk = key($allEvent);
         foreach($allEvent as $k => $v){
            $bannerImg = (isset($v["profilMediumImageUrl"])) ? "/ph".$v["profilMediumImageUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/templateCostum/no-banner.jpg";
            $date = date(DateTime::ISO8601, $v["startDate"]->sec);
             ?>
             <div class="<?php if($k === $lk) echo "item active"; else echo "item"; ?>">
             <div class="resume-infoc col-xs-3 col-sm-3">
                <span class="text-white" style="font-size: 3vw;"> <?= $v["name"]; ?> </span> <br>
                <span class="text-white" style="font-size: 2.5vw;"> Le  <?= date("d/m/Y" , strtotime($date)); ?> </span>
             </div>
    
             
             <img src="<?= $bannerImg; ?>"  alt="<?= $v["name"]; ?>" style="width:100%">
             <!-- ARROW -->
            </div>
         <?php } ?>
         </div>
            <?php if(count($allEvent) > 1) { ?>
                <div id="arrow-caroussel">
               <!-- Début svg --> 
               <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.1 54.9" style="width:35%;">
    <defs><style>.cls-3{fill:none;}.cls-2{fill:#fff;}</style></defs>
    <title>fleche</title>
    <a href="#carousel" data-slide="next"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/><polygon class="cls-2" points="87.57 43.75 119.64 43.75 119.64 54.9 147.08 27.45 119.64 0 119.64 11.14 87.57 11.14 87.57 43.75"/></a>
    <a href="#carousel" data-slide="prev"><rect class="cls-3" x="131.87" y="340.95" width="78.36" height="54.9" transform="translate(210.23 395.84) rotate(180)"/><polygon class="cls-2" points="64.53 11.14 32.46 11.14 32.46 0 5.01 27.45 32.46 54.9 32.46 43.75 64.53 43.75 64.53 11.14"/></a>
</svg>
            <!-- <div class="background-carousel">
            .
            </div> -->
            <div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6">

               <!-- Fin svg --> 
                </div>
            </div> <?php }
        } 
        echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]); 
    ?>
    <script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "title" : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                "background" : {
                    label : "Couleur du fond",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                defaultcolor : {
                    label : "Couleur",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                },
                nbLimit : {
                    label : "Nombre de post",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.nbPost
                },
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
    <?php 
    } ?>
</div>