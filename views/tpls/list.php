<style type="text/css">
	.statsList{list-style: none;}
	.statsList li{text-align: right;padding:10px 15px 10px 5px;border-bottom: 1px solid #666; color:<?php echo $color ?>;}
	.statsList li span{margin-left:15px;}
</style>
<ul class="statsList col-xs-12">
	<?php foreach ($data as $i => $v) {
		?>
		<li  class="col-xs-12">
			<?php 
			if( isset($v["href"]) ) 
				echo "<a href='".$v["icon"]."'>";
			
			if( isset($v["icon"]) ) 
				echo " <i class=' fa fa-".$v["icon"]."'></i> ";
			echo $v["name"]." ";
			if( isset($v["data"]) ) {
				$dataType = (isset($v["type"])) ? $v["type"] : "primary";
				echo "<span class='label label-".$dataType."'>".$v["data"]."</span>";
			}

			
			if( isset($v["href"]) ) 
				echo "</a>";
			 ?>
		</li>
		<?php 
	} ?>	
</ul>