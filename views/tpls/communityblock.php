<?php
$defaultColor = "green";
$structField = "structags";
?>
<div id="community" class="container tplsCommunity">
    <!-- LA ZONE EST MINÉE -->
    <?php
    if(isset(Yii::app()->session["costum"]["tpls"]["community"])){
        foreach(Yii::app()->session["costum"]["tpls"]["community"]["data"] as $key => $value){
            ?>
        <div class="col-md-4 communityCms">
            <div class="communityCms-<?= $value["title"]; ?>">
                <img src="<?= Yii::app()->getModule("costum")->assetsUrl.$value["img"]; ?>" class="img-responsive" style="text-align: center">
                <h2 class="text-center"> <?= $value["title"]; ?></h2>
            </div>
        </div>
        <?php
        }  
        if($canEdit){ ?>
            <a class='btn btn-xs btn-danger editTpl' href='javascript:;' data-id='<?= Yii::app()->session["costum"]["contextId"]; ?>' data-collection='<?= Yii::app()->session["costum"]["contextType"]; ?>' data-key='communityblock' data-path='costum.tpls.communityblock'><i class='fa fa-pencil'></i></a>
         <a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-id='<?= Yii::app()->session["costum"]["contextId"]; ?>' data-collection='<?= Yii::app()->session["costum"]["contextType"]; ?>' data-key='communityblock' data-path='costum.tpls.communityblock'><i class='fa fa-trash'></i></a>
 <?php }  
    } ?>
</div>
