<?php
/**
 * TPLS QUI PERMET AFFICHAGE DES 3 DERNIERS NOUVEAUTÉS
 * MODÈLE INSPIRER DU COSTUM FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMS LA COULEUR DE INFO
 */
$keyTpl = "blockevent";
$defaultcolor = "white";
$tags = "structags";
$bk = (@Yii::app()->session["costum"]["tpls"]["blockevent"]["background"] && !empty(Yii::app()->session["costum"]["tpls"]["blockevent"]["background"])) ? Yii::app()->session["costum"]["tpls"]["blockevent"]["background"] : "green";
$color = (@Yii::app()->session["costum"]["tpls"]["blockevent"]["color"] && !empty(Yii::app()->session["costum"]["tpls"]["blockevent"]["color"])) ? Yii::app()->session["costum"]["tpls"]["blockevent"]["color"] : "white";

$paramsData = [ "title" => "",
                "color" => "",
                "background" => "",
                "defaultcolor" => "white",
                "tags" => "structags"
                ];

if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset(Yii::app()->session["costum"]["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  Yii::app()->session["costum"]["tpls"][$keyTpl][$i];      
    }
}
?>
<style>
    .img-event{
        height : 200px;
        width: 500px;
    }
    .card-event{
        margin-top: -30%;
        background: <?= $bk; ?>;
        color : <?= $color; ?>;
    }
</style>

<div id="event-block" class="container"> 
<?php
    // A crée dans un modèle a part ce code
    date_default_timezone_set('UTC');

    //On récupère sur ce mois-ci les évènements
    $date_array = getdate ();
    $numdays = $date_array["wday"];

    $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
    $startDate = strtotime(date("Y-m-d H:i"));

    // Préparation de la requête
    $where = array("source.key"     => Yii::app()->session["costum"]["contextSlug"],
                   "startDate"      => array('$gte' => new MongoDate($startDate)),
                   "endDate"        => array('$lt' => new MongoDate($endDate)));
    // var_dump($where);exit;

    $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where, 3);
    // var_dump($allEvent);exit;
    if($allEvent){
        // $date = new DateTime();
    foreach($allEvent as $key => $value){
        // $date = new Date($value["startDate"]);
        $img = (isset($value["profilImageUrl"])) ? $value["profilImageUrl"] : "/images/templateCostum/no-banner.jpg";
        // var_dump(date(DateTime::ISO8601, $value["startDate"]->sec));exit;
        $date = date(DateTime::ISO8601, $value["startDate"]->sec);
        ?>
        <div class="col-md-4">
            <div class="col-xs-12">
                <img src="<?= Yii::app()->getModule("costum")->assetsUrl.$img; ?>" class="img-event img-responsive">
                <div class="col-xs-6 card-event">
                    <?= $value["name"]; ?> <br>
                    <?= date("d/m/Y" , strtotime($date)); ?> <br>
                    <?=  Yii::t("common",$value["type"]); ?>
                </div>
            </div>
        </div>
    <?php }
    }
    else echo "Aucun évènement n'est prévu";
    ?>

    <?php 
    echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]); 
    ?>
        <script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "title" : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                "background" : {
                    label : "Couleur du fond",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                defaultcolor : {
                    label : "Couleur",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
</div>