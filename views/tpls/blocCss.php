<?php 
$keyTpl = "css";

     $paramsData = [ "menuTop"                => "",
                     "loader"                 => "",
                     "menuApp"                => "",
                     "progress"               => ""];
        // var_dump($paramsData);exit;
        if( isset(Yii::app()->session["costum"][$keyTpl]) ) {
        foreach ($paramsData as $i => $v) {
            // var_dump(Yii::app()->session["costum"][$keyTpl][$i]);
            if(isset(Yii::app()->session["costum"][$keyTpl][$i])) 
                $paramsData[$i] =  Yii::app()->session["costum"][$keyTpl][$i];      
            }
        }
        // var_dump($paramsData);exit; 
        // var_dump(Yii::app()->session["costum"]["slug"]); 
?>
<?php
if($canEdit){
    ?>

<a class='btn btn-default edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= Yii::app()->session["costum"]["contextId"]; ?>' data-collection='<?= Yii::app()->session["costum"]["contextType"]; ?>' data-key='<?php echo $keyTpl ?>' data-path='costum.<?= $keyTpl ?>'>
            <i class="fa fa-eyedropper" aria-hidden="true"></i> CSS
        </a>
<?php } ?>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "menuTop][background]" : {
                    "inputType" : "colorpicker",
                    "label" : "Menu Top",
                    "value" :  sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.background
                },
                "loader][ring1][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Loader ring 1",
                    "value" :  sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring1.color
                },
                "loader][ring2][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Loader ring 2",
                    "value" :  sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring2.color
                },
                "menuApp][background]": {
                    "inputType" : "colorpicker",
                    "label" : "Menu App",
                    "value" : sectionDyf.<?= $keyTpl ?>ParamsData.menuApp.background
                },
                "menuApp][button][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Menu App Button",
                    "value" : sectionDyf.<?= $keyTpl ?>ParamsData.menuApp.button.color
                },
                "progress][value][background]": {
                    "inputType" : "colorpicker",
                    "label" : "Progress",
                    "value" : sectionDyf.<?= $keyTpl ?>ParamsData.progress.value.background
                }
                // "htmlConstruct][appRendering]" : {
                //     "inputType" : "select",
                //     "label" : "Choix du position menu",
                //     "options" : {
                //                 "horizontal" : "Horizontal",
                //                 "vertical" : "Vertical"
                //     }
                // },
                // "slug" : {
                //     "inputType" : "hidden",
                //     "value" : "<?=Yii::app()->session["costum"]["slug"]; ?>"
                // }
            },
            save : function () { 
                // console.log(document.body.getElementsByTagName("input"));
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(k.indexOf("[") && k.indexOf("]")) 			 	
                        kt = k.split("[").join("\\[").split("]").join("\\]");
                    // console.log($("#"+kt).val()); 
                    tplCtx.value[k] = $("#"+kt).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>