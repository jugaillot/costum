<footer class="text-center col-xs-12 pull-left no-padding" style="background: <?= Yii::app()->session["costum"]["css"]["menuTop"]["background"] ?>;">
     <div class="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-center col-footer col-footer-step">
                  <p class="text-white">Logiciel libre <a href="https://communecter.org" target="_blank"><img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-co.png" class="img-responsive" style="width: 10%;"></a><p> <p class="text-white">Propuslé par <a href="https://www.open-atlas.org/" target="_blank"><img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-openatlas.png" class="img-responsive" style="width: 20%;"></a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
