<?php 
$bannerImg = @Yii::app()->session["costum"]["banner"] ? Yii::app()->baseUrl.Yii::app()->session["costum"]["banner"] : Yii::app()->getModule("costum")->assetsUrl."/images/templateCostum/no-banner.jpg";

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );
  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  $cssAndScriptFilesModule = array(
      '/js/default/profilSocial.js',
      '/js/default/editInPlace.js',
  );
  
  HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
    $poiList = array();
    if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
        $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
    
        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                               "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                               "type"=>"cms") );
    } 

    //  var_dump($poiList);
    // var_dump(Yii::app()->session["costum"]["contextId"]);
    ?>
<style>
    section{
        background: #f6f6f6;
    }
    #contentBanner{
        height: 500px;
        background: yellow;
        background-image : url(<?= $bannerImg; ?>);
        background-size : cover;
    }
    .info{
        background: white;
        border: 1px silver solid;
        padding: 2%;
        box-shadow: 0px 0px 5px -2px #878786;
    }
    #agenda{
        background: <?= Yii::app()->session["costum"]["css"]["menuTop"]["background"]; ?>;
        color: white;
        padding: 5%;
        margin-top: -3%;
    }
    .bloc{
        background: <?= Yii::app()->session["costum"]["css"]["menuTop"]["background"]; ?>;
        color: white;
        padding: 3%;
    }
    h1{
        font-size: 45px;
    }
    .user-image-buttons{
        background:silver;
        border-radius: 10px;
    }
    @media (max-width:768px){
        h1{
            font-size: 35px;
        }
    }
</style>

<!-- header -->

<div id="contentBanner" class="col-xs-12 col-md-12 no-padding">
<?php
// var_dump($el);exit;
$params = [  "tpl" => "candidatGenerique","slug"=>Yii::app()->session["costum"]["slug"],"canEdit"=>$canEdit,"el"=>$el ];
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin",$params,true); 
// echo $this->renderPartial("costum.views.tpls.blocApp", array("canEdit" => $canEdit)); 
?>

<!-- Edit Banniere -->
<?php // $this->renderPartial("co2.views.element.modalBanner", 
 // array("type"=> $el["type"], "id"=>(string) $el["_id"], "name"=>$el["name"],"profilBannerUrl"=> @Yii::app()->session["costum"]["banner"])); ?>
</div>

<!-- info -->
<div class="col-xs-12 col-md-12" style="background: <?= Yii::app()->session["costum"]["css"]["menuTop"]["background"]; ?>; height:105px;"></div>

<div class="col-xs-12 col-md-12" style="margin-top: -50px; z-index: 1000;">
        <div class="info container text-center">
   
            <p class="f-para"><?= $this->renderPartial("costum.views.tpls.text", array("poiList" => $poiList, "tag" => "info"));
    ?><p>
       
        </div>
</div>


<!-- Agenda -->
<div id="agenda" class="col-xs-12 col-md-12 text-center">
    <h1><i class="fa fa-calendar"></i> L'Agenda</h1>
</div>
<div class="container col-xs-12" style="margin-top: 2%; margin-bottom: 2%;">
    <?php
    $params = array(
        "poiList"=>$poiList,
        "listSteps" => array("1","2","3","4","5"),
        "el" => $el,
        "color1" => "blue"
    );
    echo $this->renderPartial("costum.views.tpls.wizardAgenda",$params);  ?>
</div>

<!-- Je veux participer --> 
<div id="participe" class="bloc col-xs-12 col-md-12 text-center">
    <h1><i class="fa fa-connectdevelop"></i> Je veux participer</h1>
</div>

<div class="col-xs-12 col-md-12">
    <div class="container text-center" style="margin-top: 2%; margin-bottom : 2%">
    <p class="f-para"><?= $this->renderPartial("costum.views.tpls.text", array("poiList" => $poiList, "tag" => "participate"));
    ?><p>
        </div>
</div>

<!-- Actu -->
<div id="actu" class="bloc col-xs-12 col-md-12 text-center">
    <h1>L'actualité</h1>
</div>
<?= $this->renderPartial("costum.views.tpls.news", array("canEdit" => $canEdit)); ?>

<!-- ESPACE ADMIN --> 

<hr>
<div class="container">
<a href="javascript:;" class="addTpl btn btn-success" data-key="blockevent" data-id="<?= Yii::app()->session["costum"]["contextId"]; ?>" data-collection="<?= Yii::app()->session["costum"]["contextType"]; ?>"><i class="fa fa-plus"></i> Ajouter un template</a>
</div>

<script>
var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
var contextType=<?php echo json_encode($el["type"]); ?>;
var contextName=<?php echo json_encode($el["name"]); ?>;
 jQuery(document).ready(function() {
 contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
 });
</script>