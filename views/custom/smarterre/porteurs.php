<?php
$colorSection1 = @Yii::app()->session["costum"]["cms"]["colorSection1"];
    $colorSection2 = @Yii::app()->session["costum"]["cms"]["colorSection2"];
?>


<!--<center><h1>Coming Soon, En cours de design</h1></center>-->


<div class="col-xs-12 col-md-12 col-lg-12 row">
	<h1 class="col-xs-12 col-md-12 col-lg-12 text-center">Les porteurs du projet</h1>
	<div id="examples" class="col-xs-12 content-section-docs text-center">
		<!-- <div class="container-docs"> -->
			<div class='col-xs-12 col-sm-4 shadow2 margin-bottom-10' style='min-height:300px;height:415px; overflow-y:hidden;'>
				<div class='col-xs-12 col-md-10'>
					<img style="margin-left:1vw" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/porteurs/logo-main.jpg' class='img-responsive'/>
				</div>

				<div class='col-xs-12'>
					<a href='https://www.alternatiba.re/' class='col-xs-12 elipsis' target='_blank'>
						<h3>Alternatiba Péi</h3>
					</a>
					<span class='col-xs-12'>Alternatiba Péi est une association née du mouvement de mobilisation citoyenne sur le climat en métropole et créée en 2014 à La Réunion.
					</span>
				</div>
			</div>


				<div class='col-xs-12 col-sm-4 shadow2 margin-bottom-10' style='min-height:300px;height:415px; overflow-y:hidden;'>
					<div class='col-xs-12'>
						<img style="margin-top: 5vw;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/porteurs/logo-openatlas.png' class='img-responsive'/>
					</div>
					<div class='col-xs-12'>
						<a href='https://www.alternatiba.re/' class='col-xs-12 elipsis' target='_blank'>
							<h3>Open Atlas</h3>
						</a>
						<span class='col-xs-12'>Alternatiba Péi est une association née du mouvement de mobilisation citoyenne sur le climat en métropole et créée en 2014 à La Réunion.
						</span>
					</div>
				</div>
			</div>
		<!-- </div> -->
	</div>
</div>
