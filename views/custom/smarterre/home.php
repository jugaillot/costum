<?php
$cssAnsScriptFilesTheme = array(
       '/plugins/jsonview/jquery.jsonview.js',
    '/plugins/jsonview/jquery.jsonview.css',
    '/plugins/jQuery/jquery-2.1.1.min.js',
    '/plugins/jQuery/jquery-1.11.1.min.js',
    '/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
    '/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jQuery-Knob/js/jquery.knob.js',
      '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
      //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js',
    );


    $colorSection1 = @Yii::app()->session["costum"]["cms"]["colorSection1"];
    $colorSection2 = @Yii::app()->session["costum"]["cms"]["colorSection2"];
?>
<style type="text/css">

.mySlides {
	display:none;
}


#head{
	position:absolute;
	z-index:1;
	text-align:center;
	font-weight: bold;
	margin-top: 8.5%;
	color: white;
	font-size: x-large;
	text-shadow: black 0.1em 0.1em 0.2em;
}

.logo{
	width: 19%;
	position: absolute;
	z-index: 40000;
	left: 40%;
	top: 33%;
}

.hexa{
  width: 40%;
  margin-left: 30%;
  margin-top:-5%;
}

.text{
  text-align: center;
  margin-top: 20px;
}


div.item img{
	margin:auto;
}

.hex{
  -webkit-filter:brightness(50%);
}

.on{
  display: inline;
  filter: invert(100%);
}

#dessus{
  stroke: white;
  stroke-width:9;
}

text{
  font-family: arial !important;
}
</style>
	<div style="padding: 0px 1px;margin-top: -8.8%" class="containerCarousel col-xs-12 col-lg-12">
    	<div id="docCarousel" class="carousel slide" data-ride="carousel">

    		<p style="font-family: 'fb';" id="head" class="col-xs-12">EMULATEUR DE LIENS TERRITORIAUX</p>

    		<img class="logo" src="<?php echo Yii::app()->getModule("costum")->assetsUrl;?>/images/smarterre/Logosmarterre.png">

			<div class="carousel-inner">
        		<div class="item active">
        			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg'> 
        		</div>

	        	<div class="item">
	        		<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle2.jpg'>
	        	</div>

	        	<div class="item">
	        		<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle3.jpg'>
	        	</div>
    		</div>
    			<a class="left carousel-control" href="#docCarousel" data-slide="prev">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#docCarousel" data-slide="next">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
  				</a>
    	</div>
  	</div> 


<div style="margin-top: -7%;" class="hexa col-xs-12 ">
  <?php echo $this->renderPartial("costum.views.custom.smarterre.elements.hexa"); ?>
</div>
    <div class="text col-xs-12">
      	<p class="textPres" style="font-family: 'ml';font-size: 1.2vw">Apporter de la croissance aux organisations<br>
        locales avec une vision humaine, intégrer<br> 
        un projet de société locale pour les <br> 
        générations futures qui prend en compte <br> 
        l'obsolescence et la finitude des ressources.
    	</p>
    </div>
<div class="container">
    <div style="background-color: <?php echo $colorSection1?>" class="col-xs-12">
        <h1 style="color: white" class="text-center">Les smarterritoires</h1>
    </div>

    <div style="margin-top: 3vw;" id="resultSmaterritories">
      
    </div>
</div>         

<div id="bonhomme">
  <?php echo $this->renderPartial("costum.views.custom.smarterre.elements.bonhomme"); ?>
</div>

<!-- <div class="col-xs-12 col-lg-12"> -->
  <a href="#smarterre" class="lbh"><?php echo Yii::t("home","") ?>
    <img style="width: 100%;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/smarterre/phototerritoire.jpg">
    <?php echo $this->renderPartial("costum.assets.images.smarterre.bloc_smarterre"); ?>
  </a>
<!-- </div> -->

<script>
jQuery(document).ready(function($) {
    afficheSmarterritories();
});


  $(".lien").mouseover(function(){
    
  //    blColor(this);

      var a = this.href.animVal;
      var url = a.split("#");

      
      blColor(url[1]);
      
});

$(".lien").mouseleave(function(){
    
    blReset();
});

    //Passage sur humain
    $("#humain").mouseover(function(){
      
      bMenuColor("humain");
  });
  $("#humain").mouseleave(function(){
      
      bResetColor("humain");
  });


  //Passage sur territoire
  $("#territoire").mouseover(function(){
      
      bMenuColor("territoire");
  });
  $("#territoire").mouseleave(function(){
      
      bResetColor("territoire");
  });

  function bMenuColor(c){
      if(c == "humain"){
          $(".clterritoire").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"0.2"
                  },200);
              });
          });
      }
      else if(c == "territoire"){
          $(".clhumain").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"0.2"
                  },200);
              });
          });
      }
  }

function bResetColor(c){
  if(c == "humain"){
    $(".clterritoire").each(function(){
        $(this).children().each(function(){
            $(this).animate({
                opacity:"1"
            },200);
        });
    });
  }
  else if(c == "territoire"){
    // mylog.log("Enter cdt");
    $(".clhumain").each(function(){
        $(this).children().each(function(){
            $(this).animate({
                opacity:"1"
            },200);
        });
    });
  }
}

  function blColor(url){
      
      $("g").each(function(k,v){
          if(url+"-on" != v.classList[1] && $(v).attr("class") != "st3" && $(v).attr("class")){
            
            $(this).children().each(function(){
              $(this).animate({
                  opacity: "0.2"
              }, 200);
            });
          }
      });
  }

  function blReset(){
          
          $("g").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity: "1"
                  }, 200);
              });
          });
      }

var icones = [];   
var i = 0;
var resultat = [];

$("#menuApp").children("a").each(function(){
	icones.push($(this).children("i").attr('id', i++));
});


function afficheSmarterritories(){
    mylog.log("----------------- Affichage évènement");

    $.ajax({
        type : "POST",
        url : baseUrl + "/costum/smarterre/getsmarterritoriesaction",
        dataType : "json",
        async : false,
        success : function(data){
            mylog.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

            var fleches = ph + "/images/smarterritoireautre/fleche-plus-noire.svg";
            
            var i = 1;
            
            if(data.result == true){
                
                $(data.element).each(function(key,value){
                    // mylog.log("data.element",data.element);
                     /**
                    Phase de dev
                     **/
                    var imgMedium = (value.imgMedium != "none") ? "/ph/"+value.imgMedium : url;
                    var img = (value.img != "none") ? "/ph" + value.img : url;
                    var costum = baseUrl+"/costum/co/index/slug/"+value.slug ;
                    /**
                    Phase de prod
                    **/
                    // var imgMedium = (value.imgMedium != "none") ? value.imgMedium : url;
                    // var img = (value.img != "none") ? value.img : url;
                    
                    str += '<div class="card text-center">';
                    str += '<div id="event-affiche" class="card-color col-md-4">';
                    str += '<div style="margin-top: 3vw;background:<?php echo $colorSection1; ?>;border-radius: 24px;height: 305px;" id="affichedate" class="info-card text-center">';
                    str += '<div id="afficheImg" class="img-hexa">';
                    str += '<a target="_blank" href="'+costum+'">Aller sur le costum</a><br><br>';
                    str += '<div class="hexagon hexagon1"><div class="hexagon-in1"><div class="hexagon-in2" style="background-image: url('+imgMedium+');"></div></div></div></div>';
                    // str += '<img style="width:10vw;" src="'+imgMedium+'">';
                    str += '</div>';
                    str += value.name+'</br>'+value.type;
                    str += '</div>';
                    str += '</div>';
                    str += '</div>';
                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#resultSmaterritories").html(str);
            // mylog.log(str);
        },
        error : function(e){
            // mylog.log("error : ",e);
        }
    });
}
</script>