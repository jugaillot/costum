<div class="col-xs-12">
    <h2 class="col-xs-12 text-yellow text-center" style="font-size:30px;">Continuer à Participer à la cartographie du 62</h2>
    <!--<div class="col-xs-12">
        <span class="col-xs-12 bg-yellow" style="color: black; border-radius: 10px; text-align:center; font-size: 18px; padding: 10px;">!! Attention un contenu non localisé sur le 62 ne sera pas validé !!</span>
    </div>-->
</div>
<div class="col-xs-12">
<!--<a href="#element.invite" class="lbhp  text-yellow margin-bottom-10"><i class="fa fa-user "></i> <span>Inviter quelqu’un</span></a>-->
	<h3 class="text-yellow">Lieux de vie et commerces de proximité)</h3>
	<span>Un lieu convivial et vivant (café, salle de jeu, salle de sport, autres)</span>
	<a href="javascript:;" class="addLifePlace text-yellow"">Ajouter</a><br/>
<span>Un commerce de proximité (garage automobile, fleuriste, commerçant, artisant, bien-être)</span>
<a href="javascript:;" class="addbusinessOrganization text-yellow"">Ajouter</a><br/>
<span>Un lieu ou organisation socio-culturel (théâtre, centre social, médiathèque, cinéma)</span>
<a href="javascript:;" class="addCulturalOrganization text-yellow">Ajouter</a><br/>
<h3 class="text-yellow">Infrastructures/mobiliers/arts/espaces urbains et architecture</h3>
<span>Une statue, une vieille bâtisse, une peinture murale, un terrain multisport, un espace vert, un arbre, un étang, ...</span>
<a href="javascript:;" class="openPoi text-yellow"">Ajouter</a><br/>
<h3 class="text-yellow">Un événement</h3>
<span>Une manifestation ouvert à tou.te.s, festive, culturelle, ateliers découverte, rencontres apolitiques, organisée par une association, un collectif d’habitants, une collectivité ou une entreprise.</span>
<a href="javascript:;" class="openEvent text-yellow"">Ajouter</a>
<h3 class="text-yellow">Initiatives et projets</h3>
<span>Une action existante, dont vous avez connaissance et que vous voudriez mettre en avant, qui favorise le vivre ensemble, qui respecte l’humain et l’environnement, qui lutte contre des problématiques sociales et sociétales (déchets, alimentation, logement, cadre de vie, santé,  démocratie/participation citoyenne, transport, éducation, énergie, culture, environnement, sport, numérique, économie, …)</span>
<a href="javascript:;" class="openProjet text-yellow"">Ajouter</a>
<h3 class="text-yellow">Une idée pour améliorer le quotidien</h3>
<span>Une idée, une question, une proposition qui favoriserait le vivre ensemble, qui contribuerait à inspirer davantage de respect envers le vivant et l’environnement, qui aiderait à lutter contre des problématiques sociales et sociétales (déchets, alimentation, logement, cadre de vie, santé,  démocratie/participation citoyenne, transport, éducation, énergie, culture, environnement, sport, numérique, économie, …)</span>
<a href="javascript:;" class="openProposal text-yellow"">Ajouter</a>
</div>
<div class="col-xs-12 margin-20 text-center">
    <span><a style="color:#2E90F3;" href="#reglement" target="_blank">Voir les conditions générales</a></span>
</div>    
<div class="modal fade" role="dialog" id="modalAddSuccess" style="background-color: rgba(250,250,250,0.5);">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header text-white">
                <h4 class="modal-title text-yellow"><i class="fa fa-check"></i> Ajout enregistré avec succès</h4>
            </div>
            <div class="modal-body center text-dark hidden" id="modalRegisterSuccessContent"></div>
            <div class="modal-body center text-dark">
                <h4 class="no-margin"><br><i class="fa fa-check-circle"></i> Vous pouvez d'ores et déjà modifier cette fiche en allant sur l'onglet <a href="#references" class="lbh text-yellow">Mes contributions</a></h4>
                <div class="panel-point">
                	<!--<sapn>
                	<br>
                    L'attribution de(s) point(s) sera faite lorsque le contenu aura été validé par les administrateurs
                	<br><br>
                	Dès lors, l'item rajouté sera par ailleurs visible de la cartographie et de l'agenda
                	<br>
                	</sapn>-->
                </div>    
            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Continuer</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	$(".addLifePlace").click(function(){
		var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.organization.dynFormCostum);
		dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
		dyFCostumChtitube.beforeBuild.properties.tags={
			inputType : "tags",
			minimumInputLength : 0,
			placeholder : "Sélectioner les mots-clés et autant de mots-clés qui caractérise ce lieu",
			values : ["Bar", "Café", "SalleDeJeux", "Livres", "Restaurant"],
			label : "Ajouter des mots clés",
			value : "LieuDeVie"
		};

		dyFCostumChtitube.onload.actions.setTitle = "Ajouter un lieux convivial";
		dyFObj.openForm("organization", null,null,null, dyFCostumChtitube)
	});
    $(".addbusinessOrganization").click(function(){
        var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.organization.dynFormCostum);
        dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
        dyFCostumChtitube.beforeBuild.properties.tags={
            inputType : "tags",
            minimumInputLength : 0,
            placeholder : "Sélectioner les mots-clés et autant de mots-clés qui caractérise ce lieu",
            values : ["Artisant", "Boulangerie", "Epicerie", "Vrac", "Fleuriste", "Garage"],
            label : "Ajouter des mots clés",
            value : "CommerceDeProximite"
        };
        dyFCostumChtitube.onload.actions.setTitle = "Ajouter un commerce de proximité";
        dyFObj.openForm("organization", null,null,null, dyFCostumChtitube)
    });
    $(".addCulturalOrganization").click(function(){
        var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.organization.dynFormCostum);
        dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
        dyFCostumChtitube.beforeBuild.properties.tags={
            inputType : "tags",
            minimumInputLength : 0,
            placeholder : "Sélectioner les mots-clés et autant de mots-clés qui caractérise ce lieu",
            values : ["Culture", "Social", "Insertion", "Théâtre", "Cinéma", "Garage", "Mairie", "Conseil de quartier"],
            label : "Ajouter des mots clés",
            value : "LieuCulturel, LieuSocial"
        };

        dyFCostumChtitube.onload.actions.setTitle = "Ajouter un lieu/organisation culturel/le ou social/e ";
        dyFObj.openForm("organization", null,null,null, dyFCostumChtitube)
    });
    $(".openProjet").click(function(){
        var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.project.dynFormCostum);
        dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
        dyFCostumChtitube.onload.actions.setTitle = "Ajouter une initiative";
        dyFObj.openForm("project", null,null,null, dyFCostumChtitube)
    });
    $(".openEvent").click(function(){
        var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.event.dynFormCostum);
        dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
        dyFCostumChtitube.onload.actions.setTitle = "Ajouter un événement";
        dyFObj.openForm("event", null,null,null, dyFCostumChtitube)
    });
    $(".openPoi").click(function(){
        var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.poi.dynFormCostum);
        dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
        dyFCostumChtitube.onload.actions.setTitle = "Ajouter un point d'intérêt";
        dyFObj.openForm("poi", null,null,null, dyFCostumChtitube)
    });
    $(".openProposal").click(function(){
        var dyFCostumChtitube=jQuery.extend(true, {}, typeObj.proposal.dynFormCostum);
        dyFCostumChtitube=costum.chtitube.dynForm.setCommonDynForm(dyFCostumChtitube);
        dyFCostumChtitube.onload.actions.setTitle = "Ajouter une idée, un sondage, une proposition";
        dyFObj.openForm("proposal", null,null,null, dyFCostumChtitube)
    });
});

</script>
