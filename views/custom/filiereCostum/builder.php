<?php
$canEdit = Authorisation::canEditItem(Yii::app()->session["userId"],@Yii::app()->session["costum"]["contextType"], @Yii::app()->session["costum"]["contextId"]);
if($canEdit){
    $initValues = [];
	$ct = 1;
    ?>
    <?= $this->renderPartial("costum.views.tpls.acceptAndAdmin"); ?>
    <form id="formTpls">
    <?php 
        foreach ( $form["inputs"] as $key => $input) { 
            $editTplsBtn = ($canEdit) ? " <a class='btn btn-xs btn-danger editTpl' href='javascript:;' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='".$key."' data-path='inputs.".$key."'><i class='fa fa-pencil'></i></a>".
            "<a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='tpls".$key."' data-path='inputs.".$key."'><i class='fa fa-trash'></i></a>" : "";
            
            $tpl = $input["type"];

            if(in_array($tpl, ["textarea","markdown","wysiwyg"]))
                $tpl = "tpls.forms.textarea";
            else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags"]))
                $tpl = "tpls.forms.text";
    
            if( stripos( $tpl , "tpls.forms." ) !== false )
                $tplT = explode(".", $input["type"]);
    
            $p = [ 
                "input" 	=> $input,
                "type"		=> $input["type"], 
                "answer" 	=> $answer ,
                "label" 	=> $ct." - ".$input["label"] ,
                "titleColor"=> "#16A9B1",
                "info" 		=> isset($input["info"]) ? $input["info"] : "" ,
                "placeholder" => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
                "form"		=> $form,
                "key"		=> $key, 
                "canEdit"	=> $canEdit,
                "editTplsBtn" => $editTplsBtn,
                "el" => $el  ];

                if(isset($tplT[2]) && $tplT[2] == "select")
                $p["options"] = $input["options"];
            
            if($input["type"] == "tags") 
                $initValues[$key] = $input;
    
            echo "<div id='tpls".$key."'>";
                $this->renderPartial( "costum.views.".$tpl , $p );
            echo "</div>";
            
             $ct++; 
        }
    ?>
<?php
if ($canEdit) { ?>
	<a href="javascript:;" class="addTpl btn btn-danger"><i class="fa fa-plus"></i> Ajouter une section</a>
	<?php } 

	$id = (!empty($answer)) ? " data-id='".$answer["_id"]."' " : "";
	?>

	<a href="javascript:;" id="openFormSubmit" <?php echo $id ?> class=" btn btn-primary">Envoyer</a>
    </form>
<?php } ?>