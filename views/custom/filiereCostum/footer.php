<?php
$bfooter =  @Yii::app()->session["costum"]["css"]["footer"]["background"] ? Yii::app()->session["costum"]["css"]["footer"]["background"] : "white";
$cfooter = @Yii::app()->session["costum"]["css"]["footer"]["color"] ? Yii::app()->session["costum"]["css"]["footer"]["color"] : "black";
?>

<footer class="col-xs-12" style="background : <?php echo $bfooter; ?>;">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-12  col-footer col-footer-step dfooter">
            <div class="col-sm-6 col-xs-6 col-lg-6" style="color : <?php echo $cfooter; ?>">  

            
                Logiciel libre
            <a href="https://communecter.org" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-co.png" class="logo-footer img-responsive"></a>
            Propuslé par 
            <a href="http://www.open-atlas.org/" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-openatlas.png" class="logo-footer img-responsive"></a>       
            
            <?php
            $left = @Yii::app()->session["costum"]["htmlConstruct"]["footer"]["html"]["left"]["urls"];
            if(@$left){
                foreach($left as $key => $value){
                    ?>
                <a href="<?php echo $value["url"]; ?>" target="_blank" class="afooter" style="color : <?php echo $cfooter; ?>"><?php echo $value["name"]; ?> </a>
                <?php }
            }
            ?>
            
            </div>
            <div class="col-sm-6 col-xs-6 col-lg-6" style="text-align : right;">
                <?php
                $right = @Yii::app()->session["costum"]["htmlConstruct"]["footer"]["html"]["right"]["urls"];
                if(@$right){
                    echo "Financé par ";
                    foreach($right as $key => $value){
                        ?>
                        <a href="<?php echo $value["url"]; ?>" target="_blank"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?><?php echo $value["img"]; ?>" class="img-responsive logofooter"></a>
                <?php    }
                } ?>
            </div>
        </div>
    </div>
</footer>