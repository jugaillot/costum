<?php

//Yii::import("parsedown.Parsedown", true);
//$Parsedown = new Parsedown();
//var_dump($answers); exit;?>
<style type="text/css">
	
h1 {
	font-size: 24px;
}
.img-band{
margin-top: -50px;
}
.titleEncart{
width: 80%;border:1px solid #616161;margin:auto;text-align:center;
}
h3{
	color:#50b796;
	text-transform:uppercase;
}

.comment-text{
	white-space: pre-line;
}
.noborder{
	border:1px solid white;
}
.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

table {
    color: #616161;
	text-align: center;
}

table td, table th {
    border: 1px solid #616161;
}
table th {
	border-width: 2px;
}
table tr td{
	font-size: 10pt;
}
table tr th{
	font-size: 12px;
	font-weight: bold;
}
table.no-boder th, table.no-boder td{
	border:0px solid white;
}
</style>
<div class="col-xs-8 titleEncart" >
	<h2 style="color:#2c6aa1;text-transform:uppercase;"><?php echo @$badge["name"] ?></h2>
	<h4 style="">Orientation stratégique</h4>
	<h4 style=""><?php echo @$projetParent["name"] ?></h4>
	<h4 style="color:#2c6aa1;text-transform:uppercase;">Version du <?php echo @$date ?></h4>
</div>
<div>
	<h3 style="color:#16A9B1;">Principaux enjeux et objectifs de l’orientation</h3>
	<div class="col-xs-12">
		<p><?php echo str_replace("\n","<br>",@$badge["description"]) ?></p>
	</div>
</div>
<div>
	<h3 style="color:#16A9B1;">Synergie et articulation avec d’autres démarches territoriales</h3>
	<div class="col-xs-12">
		<p><?php echo str_replace("\n","<br>",@$badge["synergie"]) ?></p>
	</div>
</div>
<div>
	<h3 style="color:#16A9B1;">Listes des actions de l'orientation</h3>
	<?php if(isset($answerList) && !empty($answerList)){ 
		foreach ($answerList as $q => $a) { ?>	
	<div class="col-xs-12">
		<h4 style="color:#2c6aa1;"><?php echo @$a["name"] ?></h4>
		<span><b>Porteur(s) de l'action</b> : <?php echo @$a["porteur"] ?></span><br/>
		<span><b>Partenaire(s) de l'action</b> : <?php echo @$a["partner"] ?></span><br/>
		<span><b>Moyen prévisionnel mobilisé (euros)</b> : <?php echo @$a["budget"] ?></span><br/>
	</div>
	<?php } } else{ ?> 
		<span>Aucune action liée</span>
	<?php } ?>
</div>		
