<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/jquery-counterUp/waypoints.min.js",
		"/plugins/jquery-counterUp/jquery.counterup.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>
<style type="text/css">
	.counterBlock{height:200px;}
</style>
<div class="col-xs-12 text-explain  bg-white" >


  <div class="col-md-4  text-center padding-10"  >
    <div class="counterBlock"  >
        <h1><span class="counter"><?php echo $population ?></span></h1>
        <h3>Habitants</h3>
        <div class="col-md-4 col-sm-offset-4">
        <i class="fa fa-users" style="font-size: 4em;color:#2D99D4;"></i>
        </div>
      </div>
  </div> 

  <div class="col-md-4  text-center  padding-10"  >
    <div class="counterBlock" style="border-left:1px dashed #6CC3AC;border-right:1px dashed #6CC3AC;" >
      <h1><span class="counter"><?php echo $actions ?></span></h1>
      <h3>Actions</h3>
      <div class="col-md-4 col-sm-offset-4">
        <i class="fa fa-lightbulb-o" style="font-size: 4em;color:#16A57D;"></i>
        <?php if(!empty($actionsRea)){ ?>
        <?php } ?>
      </div>
      <?php if( !empty($actionsRea)){ ?>
      <div class="col-md-12 padding-10">
        <span class="text-red" style="font-size:1.3em"> Dont <?php echo $actionsRea ?> action<?php echo ($actionsRea>1) ? "s" : "" ?> réalisée<?php echo ($actionsRea>1) ? "s" : "" ?></span>
      </div>
      <?php } ?>
    </div>
  </div>
<?php /* ?>
  <div class="col-md-4  text-center  padding-10"  >
    <div class="counterBlock" style="border-left:1px dashed #6CC3AC;border-right:1px dashed #6CC3AC;" >
      <h1><span class="counter"><?php echo $contributeurs?></span></h1>
      <h3>Contributeurs</h3>
      <div class="col-md-4 col-sm-offset-4">
      	<i class="fa fa-handshake-o" style="font-size: 4em;color:#282C69;"></i>
        
      </div>
    </div>
  </div>
*/ ?>
  <div class="col-md-4  text-center  padding-10"  >
    <h1><span class="counter"><?php echo $finance ?></span></h1>
    <h3><?php echo $financeLbl ?> €</h3>
    <div class="counterBlock" id="millionPie" >
    
    </div>
  </div>


</div>

<script type="text/javascript">
var pieMillionCTErrData = 
{
	datasets: [{
		data: [ <?php echo $public ?>,<?php echo $private ?> ],
		backgroundColor: ["#0A2F62","#38A1E0"],
	}],
	labels: [ 'Public','Privé']
};
jQuery(document).ready(function() {
	mylog.log("render","/modules/costum/views/custom/ctenat/cterrHPChiffres.php");

	$('.counter').counterUp({
      delay: 10,
      time: 2000
    });
    $('.counter').addClass('animated fadeInDownBig');
    $('h3').addClass('animated fadeIn');
    
    ajaxPost('#millionPie', baseUrl+'/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMillionCTErr', 
          null,
          function(){  },"html");

});
</script>