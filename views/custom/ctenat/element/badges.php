<?php $cssAnsScriptFilesModule=array( 
		'/plugins/sly/jquery.easing.1.3.js',
				'/plugins/sly/sly.min.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));
?>
<style type="text/css">
	body { background: #e8e8e8; }
.container { margin: 0 auto; }

/* Example wrapper */
.wrap {
	position: relative;
	margin-top: 25px;
}

/* Frame */
.frame {
	height: 250px;
	/*line-height: 250px;*/
	overflow: hidden;
}
.frame ul {
	list-style: none;
	margin: 0;
	padding: 0;
	height: 100%;
	font-size: 50px;
}
.frame ul li {
	float: left;
	width: 227px;
	height: 100%;
	margin: 0 1px 0 0;
	padding: 0;
	border-right : 1px solid #ddd;
	color:#22252A;
	text-align: center;
	cursor: pointer;
}
.frame ul li.active {
	color: #fff;
    font-weight: 800;
    background: #5fad88;
}
.frame ul li .contentFilRougeBadge{
	vertical-align: middle;
    display: inline-grid;
    padding-top: 20px;
}
/* Scrollbar */
.scrollbar {
	margin: 0 0 1em 0;
	height: 4px;
	background: #ccc;
	line-height: 0;
}
.scrollbar .handle {
	width: 100px;
	height: 100%;
	background: #5fad88;
	cursor: pointer;
}
.scrollbar .handle .mousearea {
	position: absolute;
	top: -9px;
	left: 0;
	width: 100%;
	height: 20px;
}
.contentChildFilRouge .img-circle{
	height: 200px;
    width: 200px;
    margin: auto;
}
/* Pages */
/*.pages {
	list-style: none;
	margin: 20px 0;
	padding: 0;
	text-align: center;
}
.pages li {
	display: inline-block;
	width: 14px;
	height: 14px;
	margin: 0 4px;
	text-indent: -999px;
	border-radius: 10px;
	cursor: pointer;
	overflow: hidden;
	background: #fff;
	box-shadow: inset 0 0 0 1px rgba(0,0,0,.2);
}
.pages li:hover {
	background: #aaa;
}
.pages li.active {
	background: #666;
}

/* Controls */
.controls { margin: 25px 0; text-align: center; }

/* One Item Per Frame example*/
/*.oneperframe { height: 300px; line-height: 300px; }
.oneperframe ul li { width: 1140px; }
.oneperframe ul li.active { background: #333; }

/* Crazy example */
/*.crazy ul li:nth-child(2n) { width: 100px; margin: 0 4px 0 20px; }
.crazy ul li:nth-child(3n) { width: 300px; margin: 0 10px 0 5px; }
.crazy ul li:nth-child(4n) { width: 400px; margin: 0 30px 0 2px; }
</style>

		<div class="wrap col-xs-12 col-md-10 col-md-offset-1">
			<!--<h2>Centered <small>- activated or middle item is centered when possible</small></h2>-->
			<div class="scrollbar">
				<div class="handle">
					<div class="mousearea"></div>
				</div>
			</div>
			<div class="frame shadow2" id="centered" style="overflow: hidden;">
				<ul class="clearfix slidee">
				<?php if(@$badges){ 
				foreach($badges as $k=> $v){ 
						if($el["category"]!="cteR" || $v["category"]=="strategy"){
							$imgPath = (@$v["profilMediumImageUrl"] && !empty($v["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumbnail-default.jpg' ?>

							<li data-value="<?php echo $k ?>" data-container="<?php echo @$htmlContainer ?>"><div class="contentFilRougeBadge">
								<img class="img-circle" src="<?php echo $imgPath ?>"/><br/>
								<span class=""><?php echo @$v["name"] ?></span>
							</div></li>
			
					<?php } }
				}  ?>
				</ul>
			</div>

			<!--<div class="controls center">
				<button class="btn prev"><i class="icon-chevron-left"></i> prev</button>
				<button class="btn next">next <i class="icon-chevron-right"></i></button>
			</div>-->
		</div>
	<!--<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 no-padding" id="frame">
		<ul class="slidee">
		<?php /*if(@$badges){ 
				foreach($badges as $k=> $v){ 
					$imgPath = (@$v["profilMediumImageUrl"] && !empty($v["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_history.png' ?>

					<div class="col-xs-12 col-sm-3 contentFilRougeBadge" data-value="<?php echo $k ?>">
						<img class="img-circle" src="<?php echo $imgPath ?>"/>
						<span class="col-xs-12"><?php echo $v["name"] ?></span>
					</div>
		
				<?php }
			} */ ?>
		</ul>-->
		<!--<div class="col-xs-12 col-sm-3 contentFilRougeBadge">
			<img class="img-circle" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/badges/agriculture.png" ?>"/>
			<span class="col-xs-12">Agriculture et biodiversité</span>
		</div>
		<div class="col-xs-12 col-sm-3 contentFilRougeBadge">
			<img class="img-circle" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/badges/ingenierie.png" ?>"/>
			<span class="col-xs-12">Ingénieurie et architecture bioclimatique</span>
		</div>
		<div class="col-xs-12 col-sm-3 contentFilRougeBadge">
			<img class="img-circle" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/badges/energie.png" ?>"/>
			<span class="col-xs-12">Maitrise de l'energie</span>
		</div>
		<div class="col-xs-12 col-sm-3 contentFilRougeBadge">
			<img class="img-circle" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl()."/images/ctenat/badges/ecomobilite.png" ?>"/>
			<span class="col-xs-12">Ecomobilités</span>
		</div> -->
	<!--</div>
	</div>
-->	<div class="col-xs-12 contentFilRouge contentChildFilRouge col-md-10 col-md-offset-1 margin-top-20"></div>
		
<script type="text/javascript">
	var auth=<?php echo json_encode(Authorisation::isInterfaceAdmin()); ?>;
	var keyContainer = <?php echo json_encode(@$htmlContainer); ?>;
	jQuery(document).ready(function() {
		//descHtml = dataHelper.markdownToHtml($(".description-markdown-filRouge").html());
		//$(".description-markdown-filRouge").html(descHtml);
		var $frame = $(keyContainer+' #centered');
		var $wrap  = $frame.parent();

		// Call Sly on frame
		$frame.sly({horizontal: 1,
			slidee : keyContainer+' .slidee',
			itemNav: 'centered',
			smart: 1,
			activateOn: 'click',
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 1,
			speed: 300,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1});
		if($(keyContainer+' .slidee li.active').length > 0)
			appendDescBadges(null, keyContainer);
		//else
		//	$(keyContainer+" #centered").parent().html("<span>Pas d'orientations stratégiques pour le moment</span>");
		$(keyContainer+' .slidee li').click(function(){
			//$(".contentChildFilRouge").fadeOut();
			appendDescBadges($(this).data("value"), $(this).data("container"));
		});
		

		//sly.activate();
	});
	function appendDescBadges(k, htmlContent){
		idB=(!notEmpty(k)) ? $(htmlContent+' .slidee li.active').data("value") : k;
		nameB=badges[idB].name;
		descB=(notEmpty(badges[idB].description)) ? badges[idB].description : "";
		imgB = (notEmpty(badges[idB].profilMediumImageUrl)) ? baseUrl+'/'+badges[idB].profilMediumImageUrl : parentModuleUrl+'/images/thumbnail-default.jpg';
		str=//'<hr/>'+
			'<div class="col-sm-4">'+
					'<img class="img-circle" src="'+imgB+'"/>'+
				'</div>'+
				'<div class="col-sm-8 text-left contentBadgeHome">'+
					'<h4 class="col-xs-12 text-gray">'+nameB+'</h4>';
					if(contextData.category=="cteR" && canEdit){
						str+="<div class='col-xs-12'>";
							str+='<a href="javascript:;" class="generate-pdf-orientation pull-left margin-right-10" data-id="'+idB+'" data-type="badge"><i class="fa fa-file"></i> Générer un pdf</a>';
							str+='<a href="javascript:;" class="edit-badge-cter pull-left margin-right-10" data-id="'+idB+'" data-type="badge"><i class="fa fa-pencil"></i> Editer</a>';
							str+='<a href="javascript:;" class="delete-badge-cter pull-left text-red margin-right-10" data-id="'+idB+'" data-type="badge"><i class="fa fa-trash"></i> Supprimer</a>';
						str+="</div>";
					}
					if(contextData.category=="cteR")
						str+='<span class="bold col-xs-12">Principaux enjeux et objectifs de l’orientation</span>';
					str+='<span class="col-xs-12 description-badge markdown">'+descB+'</span>';
					if(contextData.category=="cteR" && typeof badges[idB].synergie != "undefined"){
						str+='<span class="bold col-xs-12">Synergie et articulation avec d’autres démarches territoriales</span>';
						str+='<span class="col-xs-12 description-badge markdown">'+badges[idB].synergie+'</span>';
					}
					if(contextData.category=="cteR"){
						str+="<div class='col-xs-12 linksActions'>";
							str+='<span class="bold col-xs-12 no-padding">Liste des actions concernant l\'orientation</span>';
						
						if(typeof badges[idB].links != "undefined" && typeof badges[idB].links.projects != "undefined"){
							 countOrganizer=Object.keys(badges[idB].links.projects).length;
					          $.each(badges[idB].links.projects, function(e,v){
					            imgIcon = (typeof v.profilThumbImageUrl != "undefined" && v.profilThumbImageUrl!="" ) ? baseUrl+"/"+v.profilThumbImageUrl: parentModuleUrl + "/images/thumb/default_"+v.type+".png";  
					            str+='<a href="#page.type.'+v.type+'.id.'+e+'" class="lbh-preview-element tooltips" ';
					            if(countOrganizer>1) str+= 'data-toggle="tooltip" data-placement="top" title="'+v.name+'"';
					            str+=">"+
					              '<img src="'+imgIcon+'" class="img-circle margin-right-10" style="width: 50px;height: 50px;" width="35" height="35"/>';
					            if(countOrganizer==1) str+=v.name;
					            str+="</a>";     
					          });
						}
						str+="</div>";
						str+="<div class='col-xs-12'>";
							str+='<span class="bold col-xs-12 no-padding">Liste des documents</span>';
						str+="</div>";
						str+="<div class='col-xs-12 file-pdf-"+idB+"'>";
						if(typeof badges[idB].files != "undefined" && Object.keys(badges[idB].files).length > 0){
							$.each(badges[idB].files, function(e,v){
								str+="<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id='"+e+"'>"+
									"<a href='"+baseUrl+"/"+v.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ v.name+"</a>"+
									"<a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='"+e+"'><i class='fa fa-trash'></i> <?php echo Yii::t("common","Delete") ?></a>"+
				
								"</div>";
							});
						}
						str+="</div>";
					}
				str+='</div>';
		$(htmlContent+" .contentChildFilRouge").html(str);
		$(htmlContent+" .contentChildFilRouge .description-badge").each(function(){
			descHtml = dataHelper.markdownToHtml($(this).html());
			$(this).html(descHtml);	
		});
		documentManager.bindEvents();
		$(".generate-pdf-orientation").click(function(){
			coInterface.showLoader(".loadingPdf", "Création du pdf en cours");
			$(".loadingPdf .processingLoader").removeClass("col-xs-12 margin-top-50");
			var domTragetPdfOrientation=$(this).data("id");
            $.ajax({
			  type: "POST",
			  url: baseUrl+"/costum/ctenat/generateorientation/id/"+$(this).data("id"),
			  data: {},
			  success: function(data){
			  		toastr.success("Le copil a été créé avec succès");
			  		$(".file-pdf-"+domTragetPdfOrientation).prepend("<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+
									"<a href='"+baseUrl+"/"+data.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ data.fileName+"</a>"+
								"</div>");
			  		// do something in the background*/
					dialog.modal('hide');

			  },
			  dataType: "json"
			});
		});
		$(".edit-badge-cter").click(function(){
			var dyfbadge=jQuery.extend(true, {}, typeObj.badge.dynFormCostum);
				//var dyfbadge=typeObj['badge']["dynFormCostum"]);
				dyfbadge.beforeBuild.properties.parent = {
					inputType : "finder",
					label : "Porteur de l'orientation stratégique",
					//multiple : true,
	                initMe:false,
	                placeholder:"Rechercher le porteur de l'orientation ?",
					rules : { required : false, lengthMin:[1, "parent"]}, 
					initType: ["projects"],
					openSearch :true
				};
				if(typeof contextData != "undefined" && notNull(contextData) && notNull(contextData.id)){
					dyfbadge.beforeBuild.properties["linksProjects"] = {
						inputType : "finder",
						label : "Actions liées à l'orientation",
						//multiple : true,
		                initMe:false,
		                placeholder:"Quelles actions correspondent à cette orientation ?",
		                initContext:false,
		                initContacts:false,
						rules : { required : false}, 
						initType: ["projects"],
						search : {"links" : [{ type:"projects", id:contextData.id }], "private":true},
						openSearch :true
					};
					dyfbadge.formData ="costum.ctenat.badges.formData";
					dyfbadge.prepData = function(data){
						if(typeof data.map.links != "undefined" && data.map.links.projects != "undefined"){
							data.map.linksProjects=data.map.links.projects;
							delete data.map.links.projects;
						}
						return data;
					}
                
				}
				dyfbadge.beforeBuild.properties.name.label="Titre de l'orientation stratégique";
				dyfbadge.beforeBuild.properties.description={"label":"Principaux enjeux et objectifs de l’orientation"};
				dyfbadge.beforeBuild.properties.synergie = {
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "Synergie et articulation avec d’autres démarches territoriales",
                    "rules" : {
                        "maxlength" : 2000
                    }
                };
                dyfbadge.onload.actions.setTitle="Ajouter une orientation stratégique à votre CTE";
				dyfbadge.onload.actions.hide["categoryselect"]=1;
				dyfbadge.onload.actions.presetValue["category"]="strategy";
				
				//return false;
				dyFObj.editElement('badges', $(this).data("id"), null,dyfbadge);
		});
		$(".delete-badge-cter").click(function(){
				directory.deleteElement("badges", $(this).data("id"));
		});
	}
</script>