<style type="text/css">
.questionList{list-style-type: none;}
.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>
<?php 
if( !isset(Yii::app()->session["costum"]["form"]["startDate"]) || 
    ( time() > strtotime(Yii::app()->session["costum"]["form"]["startDate"]) && 
      ( !isset(Yii::app()->session["costum"]["form"]["endDate"]) || time() <= strtotime(Yii::app()->session["costum"]["form"]["endDate"])))
  )
	{
	    if( isset(Yii::app()->session["userId"]))
	    {
	        //[X] ouvrir sur une answer /answer/xxx
	        //[X] revenir sur un answer pour modification
	        //[X] ouvrir la premiere fois et demander à participer 
	        //[X] ouvrir directement sur le formulaire ouvert et ca génére un answer vide 
	        //[ ] choisir une answer dans la liste pour lire
	    
	        if($showForm)
	        {	
	          	$params =[ "formId" => $formId,
	           			"form" => $form,
	           			"answer" => $answer,
	           			"canEdit" => $canEdit,	
	           		  	"el" => $el ];
	          	if( isset($wizard) ){
	          		$params["saveOneByOne"] = true;
	          		$params["wizard"] = true;
	          	}

	           	echo $this->renderPartial("costum.views.custom.co.formbuilder",$params ,true ); 
	        } else if( count($myAnswers) == 0 ) {
	            echo "<h4>Bienvenue vous n'avez pas encore participer.</h4>";
	            
	            $countTotalAnswers = PHDB::count( Form::ANSWER_COLLECTION,[ "formId"     => $formId,
	                                                             "parentSlug" => $el["slug"] ] );
	            $countdistinctUsers = count(PHDB::distinct(  Form::ANSWER_COLLECTION,"user",[ "formId"     => $formId,
	                                                             "parentSlug" => $el["slug"] ] ));
	            if($countTotalAnswers == 0)
	              echo "<br/>Soyez le premier à répondre!!!";
	            else
	              echo "<br/>".$countTotalAnswers." réponses déposées par ".$countdistinctUsers." personnes";

	            echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/new/true'>Participer</a>";
	   		} else {
	            echo "<h4>Vous avez deja participer, vous pouvez retoucher vos ".PHDB::count( Form::ANSWER_COLLECTION,[ "formId"     => $formId,
                                             "parentSlug" => $el["slug"],
                                             "user"       => Yii::app()->session["userId"] ] )." réponses dans la liste.</h4>";
	            echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/new/true'>Ajouter une réponse</a>";
	        }
	    
	}
    
} else {
  if(time() < strtotime(Yii::app()->session["costum"]["form"]["startDate"]) ) 
    echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> Les réponses commenceront le ".Yii::app()->session["costum"]["form"]["startDate"]."</h4>";
  else if ( time() > strtotime(Yii::app()->session["costum"]["form"]["endDate"]))
    echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> Le temps de réponses est écoulé depuis le ".Yii::app()->session["costum"]["form"]["endDate"]."</h4>";
}
?>


<script type="text/javascript">

jQuery(document).ready(function() {
	mylog.log("render","/modules/costum/views/custom/co/formSection.php");
});

</script>