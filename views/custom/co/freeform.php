<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 
$poiList = array();
if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                           "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                           "type"=>"cms") );
}  

?>



<div>


<div class="">


<style type="text/css">
  @font-face{
      font-family: "montserrat";
       src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
       url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
  }.mst{font-family: 'montserrat'!important;}

  @font-face{
      font-family: "CoveredByYourGrace";
      src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
  }.cbyg{font-family: 'CoveredByYourGrace'!important;}
    
    
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    border-radius: 10px;
    padding: 10px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    font-size: 1.5em
    /*min-height:100px;*/
  }
  .btn-main-menuW{
    background: white;
    color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    border:none;
    cursor:text ;
  }
  .btn-main-menu:hover{
    border:2px solid <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    background-color: white;
    color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
  }
  .btn-main-menuW:hover{
    border:none;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){
    h1, h2, h3, h4, h5, h6 {
        display: block;
        font-size: 1.9em;
    }
  }
  #customHeader #newsstream .loader{
    display: none;
  }

</style>

<?php
	$formId = (isset(Yii::app()->session["costum"]["form"]["id"])) ? Yii::app()->session["costum"]["form"]["id"] : Form::generateOpenForm( $el["slug"] );
	$params = [  "tpl" => "freeform",
				"slug"=>$el["slug"],
				"canEdit"=>$canEdit,
				"el"=>$el  ];
	echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params, true ); 
?>  

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    
    <?php 
    $banner = Yii::app()->getModule("co2")->assetsUrl."/images/banniere-Campagne-Acoeur.jpg";
    if(@Yii::app()->session["costum"]["metaImg"]){
    	//ex this.profilBannerUrl
		if(substr_count(Yii::app()->session["costum"]["metaImg"], 'this.') > 0 && isset($el)){
			$field = explode(".", Yii::app()->session["costum"]["metaImg"]);
			if( isset( $el[ $field[1] ] ) )
			  	$banner = Yii::app()->getRequest()->getBaseUrl(true).$el[$field[1]] ;
		}
		else if(strrpos(Yii::app()->session["costum"]["metaImg"], "http" ) === false && strrpos(Yii::app()->session["costum"]["metaImg"], "/upload/" ) === false ) {
			$banner = Yii::app()->getModule("co2")->getAssetsUrl().Yii::app()->session["costum"]["metaImg"] ;
        }
		else 
			$banner = Yii::app()->session["costum"]["metaImg"];
    }
    ?>
    <img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo $banner ?>'/> 

  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:<?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>; max-width:100%; float:left;">

    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 


      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: <?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>; ">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="font-family: montserrat; margin-top:-200px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12  ourvalues" style="text-align:center;">
	            <h2 class="mst col-xs-12 text-center">
                <br>
                <?php 
                  if(isset(Yii::app()->session["costum"]["cms"]["title1"])){
                    echo htmlentities(Yii::app()->session["costum"]["cms"]["title1"]);
                  } else { ?> Open Form <?php } 
                  if($canEdit)
                    echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='title1' data-path='costum.cms.title1'><i class='fa fa-pencil'></i></a>";
                  ?>
                </h2>

                <p class="mst" style="color:<?php echo Yii::app()->session["costum"]["colors"]["dark"]; ?>">
                  <?php 
                  if(isset(Yii::app()->session["costum"]["cms"]["subtitle1"])){
                    echo htmlentities(Yii::app()->session["costum"]["cms"]["subtitle1"]);
                  } else { ?>
                  Les réponses de votre réseau<br/>
                  <?php } 
                  if($canEdit)
                    echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='subtitle1' data-path='costum.cms.subtitle1'><i class='fa fa-pencil'></i></a>";
                  ?>
                 <br/>
                </p>

                <h2 class="mst" style="color:<?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>" >JE PARTICIPE</h2>
	                
	            
	            <br/>
              	<div class="col-xs-12" style="margin-bottom:40px;">
	              	<div class="col-xs-12 col-sm-4">
                        <?php if( Person::logguedAndValid() ){ ?>
	                	<span class="col-xs-12 text-center btn-main-menu btn-main-menuW ">Je suis Connecté !! <br>et je veux l'utopie</span>
                        <?php } else { ?>
                        <button  data-toggle="modal" data-target="#modalLogin" class="col-xs-12 btn-main-menu text-center" styl >JE PARTICIPE</button>
                        <?php } ?>
		            </div>

		            <div class="col-xs-12 col-sm-4">
	                	<a href="alert('open share it panel')" class="col-xs-12 btn-main-menu btn-main-menuW text-red" style="font-size: 1.5em; padding-top: 20px;"  >En partageant <br>ce sondage</a>
		            </div>

		            <div class="col-xs-12 col-sm-4">
	                	<a href="#results" class="col-xs-12 btn-main-menu">Voir les résultats</a>
		            </div>
             	</div>

            </div>
          </div>

        </div><br/>

      </div>

		
        <style type="text/css">
        	.monTitle{
        		border-top: 1px dashed <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>; 
        		border-bottom: 1px dashed <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
                margin-top: -20px;
        	}
        </style>
        <?php $formSmallSize = (!isset(Yii::app()->session["costum"]["form"]["hideAnswers"])) ? 6 : 12; ?>
    	<div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>
    		<div class="col-md-12 col-sm-12 col-xs-12 " style="background-color: white; ">
    		  <h1 class="margin-top-20 monTitle  text-red padding-20 text-center cbyg" >SONDAGE</h1>
          <div class="text-center">
          <?php if(isset(Yii::app()->session["costum"]["form"]["startDate"])){ ?>
            Démarrage : <?php echo Yii::app()->session["costum"]["form"]["startDate"]; ?>
            <?php } 

            if(isset(Yii::app()->session["costum"]["form"]["endDate"])){ ?>
            et se termine : <?php echo Yii::app()->session["costum"]["form"]["endDate"]; ?>
            <?php } ?>
            </div>
    		</div>

            <div class="row margin-top-20   padding-20" style="background-color: white; ">

                <div class="col-xs-12">

                    <div class="padding-20 btnParticiper margin-top-20 Montserrat text-center" style="font-size:1.2em ">
                        <?php 
		                  if(isset(Yii::app()->session["costum"]["cms"]["textIntro"])){
		                    echo htmlentities(Yii::app()->session["costum"]["cms"]["textIntro"]);
		                  } else { ?> Quelle est votre objectif avec ce formulaire <?php } 
		                  if($canEdit)
		                    echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='textIntro' data-type='textarea' data-markdown='1'  data-path='costum.cms.textIntro' data-label='Expliquez les objectifs de ce formulaire ? '><i class='fa fa-pencil'></i></a>";
		                  ?>
                    </div> 
                    
                    <?php 
                    /*************************************
                    SHOW or EDIT FORM
                    *************************************/
                    if($showForm){
                      $form = PHDB::findOne( Form::COLLECTION,[ "id"=>$formId ] );
                      echo $this->renderPartial("costum.views.custom.co.formSection",
                                    [ "formId" => $formId,
                                      "form" => $form,
                                      "canEdit" => $canEdit,  
                                      "showForm" => true,  
                                      "answer" => $answer,  
                                      "el" => $el ] ,true );
                    } else {
                        echo "<h4 class='text-center text-red'><i class='fa fa-warning'></i> Une seul réponse n'est possible.</h4>";
                        echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/answer/".$myAnswers[0]."'>Votre réponse</a>";
                    } 
                    ?>

				</div>
            </div>
		</div>

	
		<?php 
		if( !isset( Yii::app()->session["costum"]["form"]["hideAnswers"] ) ) 
        {
			/************************************* 
	        ANSWERS
	    *************************************/
      //[ ] show all answers 
      //[ ] show only my answers
      //[ ] show between 2 dates
      //[ ] block editing if End date passed 
        if( isset($myAnswers) && !isset(Yii::app()->session["costum"]["form"]["canReadOtherAnswers"]))
    	    $answers = $myAnswers;
        else
            $answers = PHDB::find( Form::ANSWER_COLLECTION , ["formId"=>$formId] );

		echo $this->renderPartial("costum.views.custom.co.answerList",
                     		[  	"formId" => $formId,
                     			"form" => $form,
                     			"slug" => $el["slug"],
                     			"allAnswers" => $answers,
                     			"canEdit" => $canEdit ] ,true ); 
		}
		 ?>
		
    </div>
  </div>
</div>

<script type="text/javascript">
//to edit costum page pieces 
var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;
 var configDynForm = <?php echo json_encode(Yii::app()->session['costum']['dynForm']); ?>;
//information and structure of the form in this page  
var formInputs = <?php echo json_encode($form['inputs']); ?>;
var tplCtx = {};

jQuery(document).ready(function() {
    mylog.log("render","/modules/costum/views/custom/co/freeform.php");
    
    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
    
});


</script>

