<?php
$cssAnsScriptFilesModule = array(
    '/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));

$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
?>
<style type="text/css">
.simple-pagination li a, .simple-pagination li span {
    border: none;
    box-shadow: none !important;
    background: none !important;
    color: #2C3E50 !important;
    font-size: 16px !important;
    font-weight: 500;
}
.simple-pagination li.active span{
	color: #d9534f !important;
    font-size: 24px !important;	
}
</style>
<div class="panel panel-white col-lg-offset-1 col-lg-10 col-xs-12 no-padding">
	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<div id="" class="" style="width:80%;  display: -webkit-inline-box;">
	                <input type="text" class="form-control" id="input-search-table" 
	                        placeholder="search by name or by #tag, ex: 'commun' or '#commun'">
	    <button class="btn btn-default hidden-xs menu-btn-start-search-admin btn-directory-type">
	        <i class="fa fa-search"></i>
	    </button>
	    </div>
    </div>
    <a class="pull-right" href="<?php echo Yii::app()->baseUrl.'/api/organization/get/format/csv/key/siteDuPactePourLaTransition' ?>" target="_blank" id="csv"><i class="fa fa-2x fa-table text-green"></i></a>
	<div class="panel-heading border-light">
		<h4 class="panel-title"><i class="fa fa-globe fa-2x text-green"></i> Filtered by types : </h4>
		<?php foreach ($typeDirectory as $value) { ?>
			<span class="filter<?php echo $value ?> text-purple btncountsearch"> <?php echo $value ?> <span class="badge badge-warning countPeople" id="count<?php echo $value ?>"> <?php echo @$results["count"][$value] ?></span></a>
		<?php } ?>
		<a href="javascript:;" class="filter btn btn-xs btn-default applyNoValidate"> <?php echo $value ?> <span class="badge badge-warning countPeople" id=""> Collectif non validé</span></a>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20 text-center"></div>
		<div class="panel-body">
		<div>	
			<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">
				<thead>
					<tr>
						<th>Type</th>
						<th>Name</th>
						<?php //if( Yii::app()->session[ "userIsAdmin"] && Yii::app()->controller->id == "admin" ){?>
						<th>Email</th>
						<?php //} ?>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody class="directoryLines">
					<?php 
					$memberId = Yii::app()->session["userId"];
					$memberType = Person::COLLECTION;
					$tags = array();
					$scopes = array(
						"codeInsee"=>array(),
						"codePostal"=>array(),
						"region"=>array(),
					);
					
				
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>
</div>
<script type="text/javascript">
var openingFilter = "<?php echo ( isset($_GET['type']) ) ? $_GET['type'] : '' ?>";
var directoryTable = null;
var contextMap = {
	"tags" : <?php echo json_encode($tags) ?>,
	"scopes" : <?php echo json_encode($scopes) ?>,
};
var results = <?php echo json_encode($results) ?>;
var initType = <?php echo json_encode($typeDirectory) ?>;
var betaTest= "<?php echo @Yii::app()->params['betaTest'] ?>";
var isCostumAdmin= <?php echo json_encode(@Yii::app()->session['isCostumAdmin']) ?>;
var isSuperAdmin= <?php echo json_encode(@Yii::app()->session['userIsAdmin']) ?>;
var icons = {
	organizations : "fa-group",
};
var searchAdmin={
	text:null,
	page:"",
	type:initType[0]
};
jQuery(document).ready(function() {
	setTitle("Espace administrateur : Répertoire","cog");
	initViewTable(results);
	$(".applyNoValidate").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			delete searchAdmin.toBeValidated;
		}else{
			$(this).addClass("active");
			searchAdmin.toBeValidated=true;
		}
		startAdminSearch();
	});
	$("#input-search-table").keyup(function(e){
        if(e.keyCode == 13){
            searchAdmin.page=0;
            searchAdmin.text = $(this).val();
            //if(searchAdmin.text=="")
            //	searchAdmin.text=true;
            startAdminSearch();
            // Init of search for count
            //if(searchAdmin.text===true)
            //	searchAdmin.text=null;
         }
    });
    initPageTable(results.count.organizations);

});

function initPageTable(number){
	numberPage=(number/100);
	$('.pageTable').pagination({
        items: numberPage,
        itemOnPage: 15,
        currentPage: 1,
        hrefTextPrefix:"?page=",
        cssStyle: 'light-theme',
        onInit: function () {
        },
        onPageClick: function (page, evt) {
            searchAdmin.page=(page-1);
            startAdminSearch();
        }
    });
}
function initViewTable(data){
	$('#panelAdmin .directoryLines').html("");
	
	$.each(data,function(type,list){
		$.each(list, function(key, values){
			entry=buildDirectoryLine( values, type, type, icons[type]);
			$("#panelAdmin .directoryLines").append(entry);
		});
	});
	bindAdminBtnEvents();
	}
function refreshCountBadgeAdmin(count){
	$.each(count, function(e,v){
		$("#count"+e).text(v);
	});
}
function startAdminSearch(initPage){

    $('#panelAdmin .directoryLines').html("Recherche en cours. Merci de patienter quelques instants...");

    $.ajax({ 
        type: "POST",
        url: baseUrl+"/costum/pacte/groupadmin/tpl/json",
        data: searchAdmin,
        dataType: "json",
        success:function(data) { 
	          initViewTable(data.results);
	          bindAdminBtnEvents();
	          if(initPage)
	          	initPageTable(data.results.count[searchAdmin.type]);
        },
        error:function(xhr, status, error){
            $("#searchResults").html("erreur");
        },
        statusCode:{
                404: function(){
                    $("#searchResults").html("not found");
            }
        }
    });
}

function buildDirectoryLine( e, collection, type, icon/* tags, scopes*/ ){
		strHTML="";
		if(typeof e._id =="undefined" || ((typeof e.name == "undefined" || e.name == "") && (e.text == "undefined" || e.text == "")) )
			return strHTML;
		actions = "";
		classes = "";
		id = e._id.$id;
		var status=[];
		/* **************************************
		* ADMIN STUFF
		***************************************** */
		if(userId != "" 
			|| isCostumAdmin || isSuperAdmin){
			
		}
		isValidated=(typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && e.source.toBeValidated) ? false : true;
		/* **************************************
		* TYPE + ICON
		***************************************** */
	strHTML += '<tr id="'+type+id+'">'+
		'<td class="'+collection+'Line '+classes+'">'+
			'<a href="#page.type.'+type+'.id.'+id+'" class="lbh" target="_blank">';
				if (e && typeof e.profilThumbImageUrl != "undefined" && e.profilThumbImageUrl!="")
					strHTML += '<img width="50" height="50" alt="image" class="img-circle" src="'+baseUrl+e.profilThumbImageUrl+'">'+e.type;
				else 
					strHTML += '<i class="fa '+icon+' fa-2x"></i> '+type;
			strHTML += '</a>';
		strHTML += '</td>';
		
		/* **************************************
		* NAME
		***************************************** */
		if(typeof e.name != "undefined")
			title=e.name;
		else if(typeof e.text != "undefined")
			title=e.text;
		strHTML += '<td><a href="#page.type.'+type+'.id.'+id+'" class="lbh" target="_blank">'+title+'</a></td>';
		
		/* **************************************
		* EMAIL for admin use only
		***************************************** */
		strHTML += '<td>'+e.email+'</td>';

		
		strHTML += '<td class="center status">';
			if(isValidated)
				strHTML+="<span class='badge bg-green-k'><i class='fa fa-check'></i> Groupe validé</span>";
			else
				strHTML+="<span class='badge bg-orange'><i class='fa fa-check'></i> En attente de validation</span>";
		strHTML += '</td>';
		/* **************************************
		* ACTIONS
		***************************************** */
		strHTML += '<td class="center">'+ 
			'<div class="btn-group">';
				if(!isValidated){
					strHTML+='<button data-id="'+id+'" data-type="'+type+'" class="margin-right-5 validateSourceBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider le groupe</button>';
				}
		strHTML+='</div>'+
	'</td>';
	
	strHTML += '</tr>';
	return strHTML;
}

function bindAdminBtnEvents(){
	mylog.log("validateSourceBtn");
	$(".validateSourceBtn").off().on("click", function(){
		$(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
		var $this=$(this);
		var params={
			id:$(this).data("id"),
			type:$(this).data("type")
		};
		$.ajax({
	        type: "POST",
	        url: baseUrl+"/costum/pacte/validategroup",
	        data : params
	    })
	    .done(function (data)
	    {
	        if ( data && data.result ) {
	        	toastr.success("Le groupe est validée");
	        	$this.remove();
	        	$("#"+$this.data("type")+$this.data("id")+" .status").html("<span class='badge bg-green-k'><i class='fa fa-check'></i> Groupe validé</span>");
	        	/*$("#"+params.type+params.id).fadeOut();
	        	countB=parseInt($("#count-"+setKey).text());
	        	if(action=="remove")
	        		countB--;
	        	else
	        		countB++;
	        	$("#count-"+setKey).text(countB);*/
	        	//window.location.href = baseUrl+"/"+moduleId;
	        } else {
	           toastr.error("Un problème est survenu lors de la validation");
	        }
	    });
	});
		

}
</script>