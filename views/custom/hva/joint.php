<div class=" col-xs-10 col-xs-offset-1 padding-10">
	<span>
		Vous souhaitez rendre <b>votre activité, vos initiatives plus visibles et plus lisibles</b> et ainsi de renforcer votre ancage sur les deux communautés de communes du Limouxin et des Pyrénées Audoises.<br/> <br/>

		Ce site vous offre un <b>mode de communication plus rapide et plus efficace</b> entre vous acteurs porteurs d'initiatives (contributeurs du site) et tous visiteurs du site.<br/> <br/>

		Il procure à chacun un <b>regard plus précis, plus exhaustif et plus pertinent</b> sur toutes les activités de la HVA dans le but d'un mieux vivre ensemble.<br/> <br/>

		Il permet aussi des <b>mises en relation plus efficientes entre vous</b> les acteurs porteurs d'initiatives.<br/> <br/>

		Ce site est construit autour d'une <b>charte</b> qui rassemble certaines valeurs afin de promouvoir une information indépendante, cohérente et de confiance. <br/>
		<b>Créer un compte sur <a href="https://www.portailhva.org/" >www.portailhva.org</a> implique votre acceptation implicite de la charte ci dessous : </b> <br/> <br/>

		<center> <b> CHARTE DU PORTAIL HVA </b> </center><br/> <br/>

		Ce portail regroupe et présente les initiatives et leurs acteurs qui œuvrent dans la Haute Vallée de l'Aude avec comme <b>engagements communs</b> de : <br/> <br/>

		<ul>
			<li><b>Valoriser et renforcer les ressources et les potentiels</b> de la haute vallée de l'Aude dans une perspective de “transition” vers une société plus humaine, plus solidaire et plus respectueuse de l’environnement.</li>
			<li><b>Faciliter la découverte et la promotion ainsi que la mise en réseau</b> de toutes les initiatives locales qui sont utiles socialement, écologiquement et culturellement.</li>
			<li><b>Impulser une véritable participation citoyenne</b> en encourageant à échanger et à agir collectivement pour un mieux vivre ensemble.</li>
			<li><b>Favoriser le pluralisme d'une expression </b> libre, démocratique, non prosélyte et qui ne promeut aucune forme de domination (économique, sexiste, raciste, ...).</li>
			<li><b>Porter un message de progrès social et écologique</b> dont le but premier ne soit pas purement lucratif.</li>
			<li><b>Contribuer</b> par son implication personnelle et sa responsabilisation individuelle à la vie 	harmonieuse et pérenne du portail.</li>

		</ul>  <br/> <br/>

		<center> <b> Procédure d'inscription </b> </center><br/> <br/>


		<ul>
			<li><b>1.</b> Créez <b>votre compte personnel</b> en cliquant sur le bouton <i>"se connecter"</i> ci dessous.</li>
			<li><b>2.</b> Créez <b>le profil de votre structure</b> en vous rendant sur la page « Répertoire ».</li>
			<li><b>3.</b> Puis, cliquez sur le bouton vert « <b>créer une organisation</b> » en haut à droite de la page. <br/> <i>Vous pouvez avec votre compte personnel créer plusieurs profils d’organisation dont vous êtes administrateur.</i> </li>
			<li><b>4.</b> Après <b>validation de votre proposition</b><b> par notre équipe, votre structure apparaitra <b>dans le répertoire</b>.</li>
			<li><b>5.</b> Dans la page "agenda, vous pourrez proposer <b>des événements</b> en cliquant sur le bouton "créer un évènement". Dans la page "annonces, vous pourrez proposer <b>des annonces</b>  en cliquant sur le bouton "créer une annonce". Dans la page répertoire, vous pourrez modifier votre profil en cliquant sur le bouton vert à droite "éditer"</li>

		</ul>  <br/> <br/>


		<center> <b> Important </b> </center><br/> <br/>

		Ce portail <b>existe et n'est pérenne</b> que grâce à vos <b>contributions rédactionnelles mais aussi financières</b> dont <b>le montant minimum annuel est de 20€.</b>  Ce montant est à s’acquitter en envoyant un chèque à l’ordre Réseau des Initiatives de la Haute Vallée de l’Aude à l’adresse suivante : Bennavail Georges, 1 chemin de camières, 11260 Rouvenac.  <br/> <br/>
		<b>Votre compte ne sera actif qu’après réception de votre paiement.</b><br/> <br/>
	</span>
	<span class="text-center">
		

		<?php
		
		$elt = Slug::getElementBySlug("hva", array("_id", "name") );
		?>

		<button id="connectHVA" class="letter-green font-montserrat btn-menu-connect margin-left-10 margin-right-10 menu-btn-top" data-toggle="modal" data-target="#modalLogin" style="font-size: 17px; background-color: #5b2649 !important; color: white !important; padding: 8px 15px !important;">
                <i class="fa fa-sign-in"></i> 
                <span class="hidden-xs"><small style="width:70%;">Créer un compte</small></span>
        </button>
		<!-- <a id="connectUserHVA" href="#" data-isco="false" data-id="<?php echo $elt['id'] ; ?>" class="customBtnFull customTabTrigger projectBgColor projectBgColorHover hidden btn btn-default bg-green">Devenir membre</a>

		<a id="connectOrgaHVA" href="javascript:;" data-form-type="inscription" class="addBtnFoot btn-open-form btn btn-default  bg-blue margin-bottom-10 hidden"><i class="fa fa-newspaper-o "></i> <span>Inscrit ton organisation</span></a>
		<span id="connectMsgHVA" class="hidden">En attente de validation</span> -->
	</span>
</div>
<script type="text/javascript">
	
$(document).ready(function() {
	coInterface.bindButtonOpenForm();
	$('#connectUserHVA').click(function(){
		var id = $(this).data("id");
		var isco = $(this).data("isco");
		if(isco == false){
			links.connectAjax('organizations',id,userId,'citoyens','contributors', null, function(){
				$('#connectUserHVA').html("Désinscrire");
				$('#connectUserHVA').data("isco", true);
				$("#connectUserHVA").addClass("hidden");
				$("#connectOrgaHVA").addClass("hidden");
				$("#connectMsgHVA").removeClass("hidden");
			});
		}else{
			links.disconnectAjax('organizations',id,userId,'citoyens','contributors', null, function(){
				$('#connectUserHVA').html("Devenir membre");
				$('#connectUserHVA').data("isco", false);
				$("#connectUserHVA").removeClass("hidden");
				$("#connectOrgaHVA").addClass("hidden");
				$("#connectMsgHVA").addClass("hidden");
			});
		}
		
	});

	if(typeof userId != "undefined" && userId != null && userId != ""){
		$("#connectHVA").addClass("hidden");
		// if(costum.isMember === true){
		// 	$("#connectOrgaHVA").removeClass("hidden");
		// 	$("#connectUserHVA").addClass("hidden");
		// 	$("#connectMsgHVA").addClass("hidden");
		// }else{
		// 	$("#connectUserHVA").removeClass("hidden");
		// 	$("#connectOrgaHVA").addClass("hidden");
		// 	$("#connectMsgHVA").addClass("hidden");
		// }
	}
});
</script>