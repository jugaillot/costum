<?php 
$bannerImg = @Yii::app()->session["costum"]["banner"] ? Yii::app()->baseUrl.Yii::app()->session["costum"]["banner"] : Yii::app()->getModule("costum")->assetsUrl."/images/templateCostum/no-banner.jpg";
$logo = @Yii::app()->session["costum"]["logo"] ? Yii::app()->session["costum"]["logo"] : null;
$shortDescription = @Yii::app()->session["costum"]["description"] ? Yii::app()->session["costum"]["description"] : null;
$bkagenda = @Yii::app()->session["costum"]["tpls"]["blockevent"]["background"] ? Yii::app()->session["costum"]["tpls"]["blockevent"]["background"] : "white";
$titlenews = @Yii::app()->session["costum"]["tpls"]["news"]["title"] ? Yii::app()->session["costum"]["tpls"]["news"]["title"] : "Actualité";

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );
  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  $cssAndScriptFilesModule = array(
      '/js/default/profilSocial.js',
      '/js/default/editInPlace.js',
  );
  
  HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
    //   HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
      
    // $elementParams=@Yii::app()->session['paramsConfig']["element"];
    
    // if(isset(Yii::app()->session["costum"])){
    //  $cssJsCostum=array();
    //  //if(isset($elementParams["js"]))
    //  //  array_push($cssJsCostum, '/js/'.Yii::app()->session["costum"]["slug"].'/about.js');
    //  //if(isset($elementParams["css"]))
    //  //  array_push($cssJsCostum, '/css/'.Yii::app()->session["costum"]["slug"].'/about.css');
    //  if(!empty($cssJsCostum))
    //      HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    // }
  $poiList = array();
  if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
      $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
  
      $poiList = PHDB::find(Poi::COLLECTION, 
                      array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                             "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                             "type"=>"cms") );
  } 


//   $id = Yii::app()->session["costum"]["contextId"];
//   $elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl","profilBannerUrl", "name", "tags", "description","costum", "links", "profilRealBannerUrl"];
// var_dump(Slug::getElementBySlug("templateCostum" ,  $elParams));
?>
<style>
section {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
}
.header{
    background: #001146;
    background-image : url(<?= $bannerImg; ?>);
    width: 101%;
    height: auto;
    background-size: cover;
    padding-bottom: 58%;
}
.logofn{
    width: 50%;
margin-top: 5%;
position: absolute;
margin-left: 3%;
}
.carto{
    margin-top : -7.9%;
}
.carto-h1 {
    color : white;
}
.carto-d {
    box-shadow: 0px 0px 20px -2px black;
    width: 80%;
    left: 10%;
    background : white;
    padding: 2%;
    font-size : 1.5vw;
}
.blockwithimg {
    margin-top : 2%;
    margin-bottom: 2%;
}
.actu{
    margin-top:3%;

}
.agenda{
    /* background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/fond/fond.png); */
    color : white; 
    margin-top :3%;
    background : <?= $bkagenda; ?>;
}
.agenda-h1{
    margin-left :12%;
    left: 4%;
    margin-top: 2%;
}
.plus-m {
    width : 2%;
}
.img-event{
    height: 250px;
    width: 500px;
}
.carto-p-m{
    font-size: 2.85vw;
    margin-top: 6%;
    line-height: 42px;
}
.carto-n{
    margin-top: 2%;
}
.description{
    padding : 5%; 
    top: 5%;
}
.title-carto{
    font-size : 2.81em;
}
button {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
}
#newsstream .loader{
    display: none;
}
@media (max-width:768px){
    .plus-m {
    width : 5%;
    }
    .carto-description{
        font-size: 2.5vw !important;
    }
    .img-event {
        height: 75px;
        width: 250px;
    }
    .carto-p-m{
        font-size: 2.5rem;
        margin-top: 2%;
        line-height: 19px;
    }
    .p-mobile-description{
        font-size : 1.5rem;
    }
    .description{
        padding : 5%; 
        margin-top: -3%;
    }
    .title-carto{
        font-size : 2.81vw;
    }
    .btn {
        padding: 3px 5px;
        font-size: 7px;
    }
    .card-mobile{
        margin-top : 3%;
    }
    .logofn{
        width: 100%;
        margin-top: 13%;
    }
}
</style>
<div class="header">
<?php 
$params = [  "tpl" => "smarterritoire","slug"=>Yii::app()->session["costum"]["slug"],"canEdit"=>$canEdit,"el"=>$el ];
// if(isset($test))
//     $params["test"]=$costumSlug;
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
?>
    <center>
        <a href="javascript:;" data-hash="#@<?= Yii::app()->session['costum']['slug']; ?>">
            <img id="hexa-haut" style="position: absolute;margin-top: 22vw;width: 14vw;margin-left: 50vw;z-index: 10000;" class="img-responsive img-hexa" src="<?php echo $logo; ?>">
        </a>

        <div class="imgHaut">
            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 564.6 562.4" style="enable-background:new 0 0 564.6 562.4;" xml:space="preserve">
            <style type="text/css">
                @font-face{
                font-family: "futura";
                src: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/font/smarterritoireautre/futur2.ttf") format("ttf");
                }
                .st0haut{opacity:0.4;fill:none;stroke:#FFFFFF;stroke-width:6.4174;stroke-miterlimit:10;}
                .st1haut{opacity:0.4;fill:#FFFFFF;}
                .st2haut{fill:none;}
                .st3haut{fill:#FFFFFF;}
                .st4haut{font-family: 'futura' !important;}
                .st5haut{font-size:20.983px;}
                .st6haut{fill:#00B2BA;}
                .st7haut{fill:#93C021;}
                .st8haut{fill:#1D1D1B;}
            </style>
            <polygon class="st0haut" points="149.8,100.8 274.1,23.9 402,95.1 402.5,229.3 521.1,299 524.4,447.6 400.7,518.9 280,453.7 
                162.3,521.6 35.9,448.7 33.6,305.7 153.3,232.1 "/>
            <polygon class="st1haut" points="275.4,38.9 162.7,105.7 164.2,236.7 278.4,300.9 391.1,234.1 389.6,103.1 "/>
            <polygon class="st1haut" points="397.5,243.6 284.8,310.4 286.3,441.4 400.4,505.6 513.2,438.8 511.7,307.8 "/>
            <polygon class="st1haut" points="159,244.7 46.3,311.5 47.8,442.5 162,506.7 274.7,440 273.2,309 "/>
            <g>
                
                    <!-- <image style="overflow:visible;opacity:0.32;" width="174" height="152" xlink:href="98955EF1AD59A468.png"  transform="matrix(1 0 0 1 314.6704 301.9138)">
                </image> -->
                <g>
                    
                       <!--  <image style="overflow:visible;" width="527" height="458" xlink:href="98955EF1AD59A469.png"  transform="matrix(0.3129 0 0 0.3129 315.9659 303.5446)">
                    </image> -->
                </g>
            </g>
            <rect x="174.2" y="156.3" class="st2haut" width="228.1" height="47.4"/>
            <text transform="matrix(1 0 0 1 174.1616 176.0896)" class="st3haut st4haut st5haut"><?php echo Yii::app()->session["costum"]["contextSlug"] ?></text>
            <g>
                <a href="#smarterre">
                    <!-- <image style="overflow:visible;opacity:0.32;" width="161" height="163" xlink:href="98955EF1AD59A46F.png"  transform="matrix(1 0 0 1 80.5312 297.7746)">
                    </image> -->
                </a>
                <g>
                    <g>
                        <g>
                            <path class="st6haut" d="M181.3,443.8c-0.5,0.2-1,0.3-1.5,0.5c-9.7,2.1-18.5,2.8-26.4,2.5c0,0,0,0,0,0
                                c-53.3-2.1-65.4-52.3-65.4-52.3c42.2-4.5,65.4,3.7,78.1,14.7C182.4,423.3,181.5,441.8,181.3,443.8z"/>
                        </g>
                        <g>
                            <path class="st7haut" d="M227.7,395.5c0,0-11.3,34.1-41.8,47.2c0.1-1.8,1.2-20.7-14.1-33.9C181.3,400.1,198.2,393.6,227.7,395.5z"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="225.7" cy="359.9" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="89.5" cy="359.9" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="102.2" cy="336.2" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="129.4" cy="313.7" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="127.5" cy="340" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="161.1" cy="325.4" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="158.4" cy="306.8" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="191.1" cy="315.6" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="218.7" cy="340" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="204.3" cy="347.5" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="187.3" cy="332.3" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="177.2" cy="360.8" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="155.1" cy="359.9" r="1.9"/>
                        </g>
                        <g>
                            <circle class="st8haut" cx="121.9" cy="361.8" r="1.9"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="89.7,360 89.4,359.8 102,336.1 102.4,336.3              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="122.1,361.8 121.7,361.7 127.3,339.9 127.7,340              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="127.7,340 127.3,340 129.2,313.7 129.6,313.7                "/>
                        </g>
                        <g>
                            
                                <rect x="140.6" y="342.4" transform="matrix(0.172 -0.9851 0.9851 0.172 -206.597 439.4619)" class="st8haut" width="35" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="114.6" y="325.3" transform="matrix(0.1493 -0.9888 0.9888 0.1493 -236.5772 401.1839)" class="st8haut" width="0.4" height="25.6"/>
                        </g>
                        <g>
                            
                                <rect x="105.5" y="344.6" transform="matrix(5.902462e-02 -0.9983 0.9983 5.902462e-02 -260.7166 445.0365)" class="st8haut" width="0.4" height="32.4"/>
                        </g>
                        <g>
                            
                                <rect x="111.8" y="332.8" transform="matrix(0.7929 -0.6094 0.6094 0.7929 -189.4475 140.5434)" class="st8haut" width="0.4" height="32.3"/>
                        </g>
                        <g>
                            
                                <rect x="126" y="332.5" transform="matrix(0.9173 -0.3982 0.3982 0.9173 -120.5302 84.9735)" class="st8haut" width="36.7" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="173" y="300.2" transform="matrix(0.6798 -0.7334 0.7334 0.6798 -178.7316 229.3089)" class="st8haut" width="0.4" height="38.2"/>
                        </g>
                        <g>
                            
                                <rect x="159.5" y="306.3" transform="matrix(0.9896 -0.1441 0.1441 0.9896 -43.8641 26.3271)" class="st8haut" width="0.4" height="19.2"/>
                        </g>
                        <g>
                            
                                <rect x="129" y="309.8" transform="matrix(0.9692 -0.2462 0.2462 0.9692 -71.9064 44.974)" class="st8haut" width="29.9" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="98.2" y="324.7" transform="matrix(0.772 -0.6356 0.6356 0.772 -180.1279 147.6792)" class="st8haut" width="35.3" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="141.1" y="332.9" transform="matrix(0.5845 -0.8114 0.8114 0.5845 -225.2124 260.0605)" class="st8haut" width="0.4" height="34"/>
                        </g>
                        <g>
                            
                                <rect x="167.1" y="346.4" transform="matrix(0.3326 -0.9431 0.9431 0.3326 -205.2222 403.1595)" class="st8haut" width="30.2" height="0.4"/>
                        </g>
                        <g>
                            <rect x="174.9" y="294.4" transform="matrix(0.28 -0.96 0.96 0.28 -172.4396 392.083)" class="st8haut" width="0.4" height="33.2"/>
                        </g>
                        <g>
                            
                                <rect x="204.7" y="309.4" transform="matrix(0.6585 -0.7525 0.7525 0.6585 -176.6705 266.1194)" class="st8haut" width="0.4" height="36.8"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="225.5,359.9 218.5,339.9 219,339.8 226,359.8                "/>
                        </g>
                        <g>
                            
                                <rect x="203.4" y="343.4" transform="matrix(0.8848 -0.4659 0.4659 0.8848 -135.7533 138.1276)" class="st8haut" width="16.3" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="214.8" y="341.3" transform="matrix(0.5 -0.8661 0.8661 0.5 -198.7712 363.0674)" class="st8haut" width="0.4" height="24.8"/>
                        </g>
                        <g>
                            
                                <rect x="180.6" y="323.8" transform="matrix(0.2223 -0.975 0.975 0.2223 -168.7598 436.4172)" class="st8haut" width="17.1" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="195.6" y="328.5" transform="matrix(0.6643 -0.7475 0.7475 0.6643 -188.3468 260.4563)" class="st8haut" width="0.4" height="22.8"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="187.2,332.5 161.1,325.6 161.2,325.2 187.3,332.1                "/>
                        </g>
                        <g>
                            
                                <rect x="175.6" y="353.9" transform="matrix(0.8975 -0.441 0.441 0.8975 -136.6156 120.4022)" class="st8haut" width="30.2" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="177.2" y="360.1" transform="matrix(0.9998 -1.952753e-02 1.952753e-02 0.9998 -6.998 4.003)" class="st8haut" width="48.6" height="0.4"/>
                        </g>
                        <g>
                            
                                <rect x="165.9" y="349.3" transform="matrix(4.319710e-02 -0.9991 0.9991 4.319710e-02 -201.0227 510.7661)" class="st8haut" width="0.4" height="22.1"/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="121.9,362 121.9,361.6 155.1,359.6 155.1,360.1              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="177,360.9 160.9,325.5 161.3,325.3 177.4,360.7              "/>
                        </g>
                        <g>
                            <polygon class="st8haut" points="161.1,325.6 129.4,313.9 129.5,313.5 161.2,325.2                "/>
                        </g>
                        <g>
                            <path class="st8haut" d="M87.3,381.8l0.8-0.7c0.8,1.2,1.9,1.8,3.4,1.8c1.7,0,3-1.1,3-2.7c0-1.7-1.6-2.2-3.2-2.8
                                c-1.7-0.6-3.5-1.2-3.5-3.4c0-1.8,1.6-3.2,3.7-3.2c1.7,0,2.9,0.7,3.7,1.7l-0.8,0.6c-0.7-0.9-1.6-1.5-2.9-1.5
                                c-1.5,0-2.7,1-2.7,2.3c0,1.6,1.5,2.1,3.1,2.6c1.8,0.6,3.6,1.3,3.6,3.6c0,2-1.7,3.6-4.1,3.6C89.6,383.9,88.2,383.1,87.3,381.8z"
                                />
                            <path class="st8haut" d="M103.2,371.1h1l5.1,7.1l5.3-7.1h0.9v12.6h-1v-11l-5.3,7.1l-5.1-7v10.9h-1V371.1z"/>
                            <path class="st8haut" d="M127.9,371.1h1.1l4.9,12.6h-1.1l-1.4-3.5h-6l-1.3,3.5h-1.1L127.9,371.1z M125.9,379.3h5.2l-2.6-6.8
                                L125.9,379.3z"/>
                            <path class="st8haut" d="M141.3,371.1h3.9c2.4,0,4.1,1.8,4.1,3.9c0,1.7-1.1,3.2-2.8,3.7l2.8,5h-1.1l-2.6-4.7h-3.4v4.7h-1V371.1z
                                 M142.3,372v6.1h2.9c1.9,0,3.2-1.5,3.2-3.1c0-1.7-1.3-3-3.2-3H142.3z"/>
                            <path class="st8haut" d="M159.7,373.4h-3.4v-2.3h9.1v2.3h-3.4v10.3h-2.4V373.4z"/>
                            <path class="st8haut" d="M172.1,371.1h7.5v2.3h-5.1v2.8h5.1v2.3h-5.1v2.9h5.1v2.3h-7.5V371.1z"/>
                            <path class="st8haut" d="M186.3,371.1h4c3,0,4.8,1.8,4.8,4.4c0,1.5-0.7,2.9-2.1,3.7l2.5,4.5h-2.7l-2-3.8h-2.1v3.8h-2.4V371.1z
                                 M188.7,373.4v4.4h1.4c2,0,2.6-1,2.6-2.3s-0.8-2.1-2.6-2.1H188.7z"/>
                            <path class="st8haut" d="M202,371.1h4c3,0,4.8,1.8,4.8,4.4c0,1.5-0.7,2.9-2.1,3.7l2.5,4.5h-2.7l-2-3.8h-2.1v3.8H202V371.1z
                                 M204.4,373.4v4.4h1.4c2,0,2.6-1,2.6-2.3s-0.8-2.1-2.6-2.1H204.4z"/>
                            <path class="st8haut" d="M217.6,371.1h7.5v2.3H220v2.8h5.1v2.3H220v2.9h5.1v2.3h-7.5V371.1z"/>
                        </g>
                    </g>
                </g>
            </g>
            </svg>
        </div>
    </center>
</div>

<!-- Carousel -->
 <?php
 $params = array(
     "roles" => "filières",
     "canEdit" => $canEdit,
 );
 echo $this->renderPartial("costum.views.tpls.communityCaroussel", $params ); ?> 
<div class="agenda-carousel">

<!-- <div class="col-xs-12 col-sm-12 col-md-12 no-padding carousel-border" style="margin-top : -7.5%;">
        <div id="docCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner itemctr">
            </div>
            <div id="arrow-caroussel">
                Début svg --> 
               <!-- <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.1 54.9" style="width:35%;">
    <defs><style>.cls-3{fill:none;}.cls-2{fill:#fff;}</style></defs>
    <title>fleche</title>
    <a href="#docCarousel" data-slide="next"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/><polygon class="cls-2" points="87.57 43.75 119.64 43.75 119.64 54.9 147.08 27.45 119.64 0 119.64 11.14 87.57 11.14 87.57 43.75"/></a>
    <a href="#docCarousel" data-slide="prev"><rect class="cls-3" x="131.87" y="340.95" width="78.36" height="54.9" transform="translate(210.23 395.84) rotate(180)"/><polygon class="cls-2" points="64.53 11.14 32.46 11.14 32.46 0 5.01 27.45 32.46 54.9 32.46 43.75 64.53 43.75 64.53 11.14"/></a>
</svg>
            <div class="background-carousel">
            
            </div>
            <div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6"> -->

               <!-- Fin svg --> 
    <!--             </div>
            </div>
                <a class="left carousel-control" href="#docCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#docCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
        </div>
    </div>  -->
</div>

    <?php if($canEdit){ ?> 
<!-- ESPACE ADMIN --> 

<hr>
<div class="container">
<a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent" data-id="<?= Yii::app()->session["costum"]["contextId"]; ?>" data-collection="<?= Yii::app()->session["costum"]["contextType"]; ?>"><i class="fa fa-plus"></i> Ajouter une section</a>
</div>
<?php } ?>

<!-- Carto NEWS -->

<div class="carto-n row">
    <div class="c-description container">
    <?= $this->renderPartial("costum.views.tpls.blockwithimg", array("poiList" => $poiList, "canEdit" => $canEdit)); ?>
    </div>
</div>


<!-- Join -->

<?php 
    // Bug entre Authorisation & mon cms
        // echo $this->renderPartial("costum.views.tpls.blockwithimg", array(
        // "poiList" => $poiList, "canEdit" => $canEdit));
        echo $this->renderPartial("costum.views.custom.templateCostum.tpls.btn", array("poiList" => $poiList));
    ?>


<?php if(isset(Yii::app()->session["costum"]["tpls"]["news"])) { ?>
<!-- Actualité -->
<div class="actu row">
    <div class="col-xs-12 col-sm-12">
    <div class="col-xs-6 col-sm-6" style="padding-left: 16%;">
        <h1><?= $titlenews; ?></h1>
    </div> 
    <div class="col-xs-12">
        <?php 
            echo $this->renderPartial("costum.views.tpls.news", array("canEdit" => $canEdit));
        ?>
    </div>
    </div>
</div>
<?php } ?>

<!-- Agenda --> 
<?php if(isset(Yii::app()->session["costum"]["tpls"]["blockevent"])) { ?>
<div class="agenda row">
    <div class="agenda-h1 col-xs-6 col-sm-6">
    <h1><?php if(!empty(Yii::app()->session["costum"]["tpls"]["blockevent"]["title"])) echo Yii::app()->session["costum"]["tpls"]["blockevent"]["title"]; else "Agenda"; ?> <br></h1>
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait-blanc.svg" class="img-responsive" style="padding-right: 69%;">
    <p>Évènement à venir</p>
    </div>
    <!-- <div class="event col-xs-12 col-sm-12">
        <div id="event-affiche"></div>

    </div> -->

    <!-- TEST EVENT -->
    <div class="col-xs-12">
    <?php echo $this->renderPartial("costum.views.tpls.blockevent",  array("canEdit" => $canEdit)); ?>
    </div>
    <div class="event-lien col-xs-12 col-sm-12 text-center">
            <p>
            <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app " style="text-decoration : none; font-size:2rem; color:white;">
           <center> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus-blanc.svg" class="img-responsive plus-m" style="margin: 1%;"></center>          Voir plus<br>
            d'évènement.</a></p>
        </div>
</div>
<?php } ?>



<script>
jQuery(document).ready(function(){
    mylog.log("render","/modules/costum/views/custom/templateCostum/home.php");


    // chargeElement();
    // afficheEvent();
    setTitle(costum.title);
});
</script>