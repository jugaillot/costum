<?php
	HtmlHelper::registerCssAndScriptsFiles( array('/css/calendar.css'), Yii::app()->theme->baseUrl. '/assets');

	HtmlHelper::registerCssAndScriptsFiles(array('/css/laRaffinerie.css'), $this->module->assetsUrl);

?>

<article class="">

	<p>Depuis 4 ans, l'association Les Rencontres Alternatives porte le projet de la création d'un tiers-lieu associatif, sur le site de l'ancienne usine sucrière de Savanna à Saint-Paul, qui accueillera différents porteurs de projets partageant les valeurs promues par La Raffinerie.</p>

	<p>La friche éco-culturelle "La Raffinerie" offrira donc les informations, les formations, les activités et les outils pour permettre aux utilisateurs d'être acteurs de l'amélioration de leur quotidien tout en répondant, au mieux, aux urgences environnementales et soco-économiques actuelles.</p>

	<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/laRaffinerie3/a-propos.jpg'> 

	<p>Economie circulaire, économie sociale et solidaire, développement durable, transition écologique, biodiversité et culture seront au coeur des activités organisées autour de différents espaces qui constitueront La Raffinerie :</p>

	<ul>
		<li>Espace recyclerie (vélo, D3E, plastique, bois, papier);</li>
		<li>Fablab en éco conception;</li>
		<li>Espace jardin (potager, serre aquaponique, rucher pédagogique, jardins paratagés, poulailler...);</li>
		<li>Espace café / restaurant / micro-brasserie;</li>
		<li>Epicerie en vrac et Amap;</li>
		<li>Espace de coworking dédié aux porteurs de projets innovants et en lien avec l'Economie Sociale et Solidaire et l'Economie Circulaire;</li>
		<li>Activités culturelles (cinéma en plein air, plateau dédié aux arts vivants, salle d'exposition, mur d'expression artistique...)</li>
		<li>Activités sportives (mur d'escalade, terrain de boules, rampe de skate)</li>
		<li>Espace boutique et librairie spécialisée.</li>
	</ul>

	<p>Sur 1700m2 de bâtiment et 6600m2 de terrain, la Raffinerie hébergera des activités qui fonctionneront en interaction les unes avec les autres.</p>

</article> 