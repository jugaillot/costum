<?php
	HtmlHelper::registerCssAndScriptsFiles( array('/css/calendar.css'), Yii::app()->theme->baseUrl. '/assets');

	HtmlHelper::registerCssAndScriptsFiles(array('/css/laRaffinerie.css'), $this->module->assetsUrl);

	$rafElt = PHDB::findOne(	Organization::COLLECTION, 
							array( "slug" => Yii::app()->session["costum"]["slug"]) , 
							array("name", "slug", "description", "address", "geo", "geoPosition") ) ;

?>

<style type="text/css">
	.mapBackground{
		/*background-image: url(/ph/assets/449afa38/images/city/cityDefaultHead_BW.jpg);*/
		background-color: #f6f6f6;
		background-repeat: no-repeat;
		background-position: 0px 0px;
		background-size: 100% auto;
		height: 400px;
	}
</style>

<article class="">
	<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/laRaffinerie3/planaccess.jpg'>
	<div class="col-xs-12 mapBackground no-padding" id="mapRaffinerie"></div>
</article>

<script type="text/javascript">
var mapRaffinerie = {};
var rafElt = <?php echo json_encode($rafElt) ;?> ;

jQuery(document).ready(function() {
	var paramsRaffinerie = {
		container : "mapRaffinerie",
		activePopUp : true,
		tile : "mapbox2"
	};
	mapRaffinerie = mapObj.init(paramsRaffinerie);
	allMaps.maps[paramsRaffinerie.container]=mapRaffinerie;
	var elts = {}
	elts[costum.contextId]= rafElt;

	mapRaffinerie.addElts(elts);
});