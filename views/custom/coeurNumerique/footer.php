<footer class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-12  col-footer col-footer-step dfooter" style="background : #f6f6f6;">
            <div class="col-xs-6 col-sm-6" style="font-size: 12px;">
            Logiciel libre <a href="https://communecter.org" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-co.png" class="logo-footer-co"></a>
            Propulsé par <a href="http://www.open-atlas.org/" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-openatlas.png" class="logo-footer-openaltas"></a></div>
            <div class="col-xs-6 col-sm-6" style="text-align: right; font-size: 12px;">
            Financé par
            <a href="https://smarterr.re" target="_blank"><img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-smarterre.png' class="logo-footer"></a>
                
               <a href="https://www.regionreunion.com/" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-region.png" class="logo-footer"></a>
                <a href="https://www.fondation-free.fr/" target="_blank"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-fondationfree.svg" class="logo-footer"> </a>
        </div>
            </div>
    </div>
</footer>
