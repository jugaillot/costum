<?php 
$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
/*$cssAnsScriptFilesModule = array(
    '/js/classes/CO3_Element.js',
    '/js/classes/CO3_Poi.js',
    '/js/classes/CO3_Article.js',
    '/js/classes/CO3_Event.js',
    '/js/classes/CO3_Obj.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());*/

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"siteDuPactePourLaTransition"),array("updated"=>-1), 3, 0);
?>
<style type="text/css">
	.start-img{
		filter: brightness(0.4);
		-webkit-filter: brightness(0.4);
	}
	.intituteHomePunch{
		color: white;
    text-align: right;
    position: absolute;
    font-weight: 900;
    font-size: 40px;
}
	.puceAlignHome .puceInc{
		padding: 10px 20px;
		background-color: #5b2649;
		color: white;
		text-align: center;
	}
	.puceAlignHome .puceTextExplain{
		padding-right: 0px;
	}
	.btn-redirect-home {
        font-size: 22px;
	    border-radius: 2px;
	    color: white !important;
	    padding: 8px 10px;
	    background-color: #5b2649 !important;
	}
	@media (max-width: 742px){
		.intituteHomePunch{
			text-align: center;
			font-size: 30px;
		}
	}
	@media (min-width: 1000px){
		.intituteHomePunch{
			font-size: 50px;
		}
	}
	@media (min-width: 1250px){
		.intituteHomePunch{
			font-size: 60px;
		}
	}
</style>
<div class="col-xs-12 no-padding">
	<div id="start" class="section-home section-home-video">
		<div class="col-xs-12 content-video-home no-padding">
			<div class="col-xs-12 no-padding container-video">
				<!--<a href="javascript:;" id="launchVideo">
				<i class="fa fa-play-circle"></i>
				</a>-->
				<img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/home_pacte.jpg'>
				<span class="intituteHomePunch col-xs-12 col-sm-10 col-md-10">
					Adoptons 32 mesures concrètes pour<br/> des communes plus écologiques et<br/> plus justes.
					<!--50+ organisations et des milliers de citoyen·nes<br/>s’engagent pour des communes plus écologiques et plus justes<br/><br/>Et vous ?<br/><br/>
					<a href="https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn btn-redirect-home col-lg-4 col-md-6 col-sm-6 col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-xs-12 text-center"
			style="font-size: 22px !important;">Restez informé⋅e</a>-->
				</span>
			</div>
		</div>
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<!--<div class="col-md-12 col-sm-12 col-xs-12 padding-20 section-home" style="padding-left:100px; margin-top:0px;background-color: #f6f6f6;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top: -19px;margin-bottom: -18px;background-color: #fff;font-size: 14px;z-index: 5;">
        <h3 class="col-xs-12 text-center">
                <small style="text-align: left">
  color: #2b2b2b;
  text-shadow: 1px 1px 0px rgba(0,0,0,0.1);">
				<span class="main-title" style="color:#5b2649;   font-size: 45px;">Construire ensemble<br/>les communes de demain</span>
				<br/>
				<hr class="col-xs-4 col-xs-offset-4" style="border-top: 3px solid #fda521;margin-top: 35px;margin-bottom: 40px;">
				<br/>
				<div  class="col-sm-5 col-xs-12 text-center" style="font-size: 1.4em;padding-right:30px;">
					<i class="fa fa-2x fa-users" style="color:#5b2649;"></i><br/><br/>
					Les habitant.es <br/> définissent les priorités<br/> pour leurs communes
				</div>
				<div  class="col-sm-2 col-xs-4 col-xs-offset-4 col-sm-offset-0 margin-top-10 margin-bottom-10">
					<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/logo-min.png'>
				</div>
				<div  class="col-sm-5 col-xs-12 text-center" style="font-size: 1.4em;padding-right:30px;">
					<i class="fa fa-2x fa-university" style="color:#5b2649;"></i><br/><br/>
					Les candidat.es <br/>s'engagent à les mettre en oeuvre<br/> une fois élu.es
				</div>
				</bloquote>
				</h3>
           </div>
    </div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>-->
	<div id="search" class="section-home col-xs-12 bg-purple padding-20" style="margin-top:0px;color:white; padding-bottom: 40px;">
		<!-- <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 header-section">
			<h3 class="text-center">S'engager dans votre commune en trouvant</h3>
		</div> -->
		<div class="col-xs-12">
			<h3 class="text-center"><i class="fa fa-search"></i> Que se passe-t-il dans <span class="text-orange">ma commune</span> ?</h3>
             <div class="col-xs-12 text-center content-input-scope-pacte"></div>
        </div>
       
        
		<!-- <div class="col-xs-12">
			<h3 class="text-center"><span class="text-orange">Mais</span> aussi...</h3>
			<a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn btn-redirect-home col-md-4 col-sm-6 col-md-offset-2 col-xs-12">
					
				Les<br/>
              	<span class="text-orange number-btn">32</span><br>
				Mesures
			</a>
			<a href="javascript:;" data-hash="#toolkit" class="col-md-4 col-sm-6 col-xs-12 lbh-menu-app btn btn-redirect-home">
				Et ...<br>
				<span class="text-orange">Une multitude</span><br>
				des ressources
			</a>
	
		</div> -->
	</div>
	<div  id="mapPacte" class="section-home col-xs-12 no-padding no-margin" style="height: 450px"></div>
   
	<!--<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>-->
	<div class="col-xs-12">
		<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-2 col-lg-3 col-lg-offset-3 margin-top-10">
			<a href="https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn btn-redirect-home col-xs-12 pull-right text-center"
			style="font-size: 22px !important;">Rester informé⋅e</a>
		</div>
		<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-lg-3 margin-top-10">
		<a href="https://www.helloasso.com/associations/collectif-pour-une-transition-citoyenne/formulaires/3" target="_blank" class="btn btn-redirect-home col-xs-12 pull-left text-center"
			style="font-size: 22px !important;">Faire un don</a>
		</div>
	</div>
	<div id="partenaire" class="col-xs-12 support-section section-home no-margin">
	 	<!--<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 no-padding" style="font-size: 20px;">
        	<div class="puceAlignHome col-xs-12">
        		<span class="puceInc col-xs-1">1</span><span class="puceTextExplain col-xs-11">Le Pacte pour la Transition propose 32 mesures concrètes pour une transition locale</span>
        	</div>
        	<div class="puceAlignHome col-xs-12 margin-top-10">
        		<span class="puceInc col-xs-1">2</span><span class="puceTextExplain col-xs-11"> Les habitant·es définissent les mesures prioritaires pour leurs communes</span>
        	</div>
        	<div class="puceAlignHome col-xs-12 margin-top-10">
        		<span class="puceInc col-xs-1">3</span><span class="puceTextExplain col-xs-11"> Les candidat·es aux Municipales de 2020 s’engagent à les mettre en œuvre une fois élu·es</span>
        	</div>
        </div>
        <div class="col-xs-12">
        	<center><h4><a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn btn-redirect-home col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3 col-xs-12">Voir les mesures</a></h4></center>
        </div>
        <div class="col-xs-12 header-section" style="margin-bottom: 0px;">
        	<h3 class="title-section col-xs-12 text-center" style="margin-top: 35px;">50+ organisations</h3>
        </div>-->
        <div class="col-xs-12 no-padding">
          <img class="img-responsive hidden-xs" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte.png'>
          <img class="img-responsive visible-xs" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte_xs.png'>

          
        </div>
        <!--<div class="col-xs-12 header-section" style="margin-bottom: 0px;">
        	<h3 class="col-xs-12 text-center" style="margin-top: 0px;">et des milliers de citoyen·nes
			</h3>
        </div>-->
	</div>
	<div id="actus" class="section-home col-xs-12 no-margin">
		<div class="col-xs-12 header-section">
			<!--<h3 class="title-section col-sm-8 col-xs-12">
				<i class="fa fa-rss"></i> L'actualité du Pacte
			</h3>-->
			<a href="javascript:;" data-hash="#actualites" class="lbh-menu-app btn btn-redirect-home btn-small-orange">Toute l'actu<span class="hidden-sm">alité</span></a>
			<!--<hr/>-->
		</div>
		<div class="col-xs-12" id="actus-pacte">
		</div>
	</div>
	<!--<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<div id="search" class="section-home col-xs-12 no-padding" style="margin-top: 0px;">
		<img class="img-responsive" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/bande_site.png'>
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>-->
	<!--<div id="useit" class="section-home col-md-10 col-md-offset-1">
		<div class="col-xs-12 header-section">
			<h3 id="porterPacte" class="title-section col-sm-8">Vous voulez participer hors de votre commune ?</h3>
			<hr/>
		</div>
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 padding-20 text-explain" >
			<!-- <a href="https://framaforms.org/devenir-benevole-pour-le-pacte-pour-la-tran
			sition-1551263682" target="_blank" class="btn-main-menu col-xs-12 col-sm-6 btn-redirect-home btn-useit"  >
				<div class="text-center">
				  <div class="col-md-12 no-padding text-center">
				      <h4 class="no-margin uppercase">
				        <i class="fa fa-group"></i><br/>
				        <?php //echo Yii::t("home","Devenir<br/>bénévole") ?>
				      </h4>
				  </div>
				</div>
			</a> -->

			<!--<a href=" https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 pull-right btn-redirect-home btn-useit"  >

			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center ">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-rss"></i><br/>
			            <?php echo Yii::t("home","Vous abonner<br/> à la Newsletter") ?>
			          </h4>
			      </div>
			  </div>
			</a>
			<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-5  margin-top-20 btn-redirect-home btn-useit">
			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-envelope"></i><br/>
			            <?php echo Yii::t("home","Nous<br/> écrire") ?>
			          </h4>
			      </div>
			  </div>
			</a>-->
			<!--<a href="https://www.helloasso.com/associations/collectif-pour-une-transitio
			n-citoyenne/formulaires/2" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 margin-top-20 btn-redirect-home btn-useit pull-right">
			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-heart"></i><br/>
			            <?php echo Yii::t("home","Faire un don") ?>
			          </h4>
			      </div>
			  </div>
			</a>-->

      <!--</div>-->
	   <!--  <div class="text-center col-xs-12 col-md-8 col-md-offset-2 text-center text-orange margin-top-20"><b><span style="font-size:30px;">...Et en parler tout autour de vous !!</span></b>
	    </div> -->
    <!--</div>-->
   
</div>
<script type="text/javascript">
	var imgH=0;
	var articles_une = "";
	var mapPacteHome = {};
	var dataSearchPacte = {} ;
	var resultMapGroup={};
	var mapLoadPacte=false;
	function specImage($this){
		imgH=$($this).height();
		$("#launchVideo").css({"line-height": imgH+"px"});
		$(".container-video").css({"max-height": imgH+"px"});
		$("#launchVideo").click(function(){
			str='<div style="padding:0 0 0 0;position:relative;height:'+imgH+'px;">'+
					'<iframe src="https://player.vimeo.com/video/106193787?title=0&byline=0&portrait=0&autoplay=1" '+' style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" '+  'webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'+
				'</div>';
			$(".container-video").addClass("col-md-10 col-md-offset-1").html(str);	
		});
	}
	function initTextInBand(time){
		punchH=$(".intituteHomePunch").height();
		imgH=$(".container-video").height();
		posTopPunch=(imgH-punchH)/2;
		$(".intituteHomePunch").css({"top":posTopPunch+"px"});
		time+=500;
		if(time < 100000)
			setTimeout(function(){initTextInBand(time)}, time);
	}
	function initWelcomePacte(){
		initTextInBand(0);
		articles_une = new CO3_Article(null,<?php echo json_encode($articles_une); ?>);
		pacte.initScopeObj();
		$("#actus-pacte").html(articles_une.SetColNum(3).SetDisplaySocial(false).RenderHtml());
		coInterface.bindLBHLinks();	
	}
	function initPacteMapView(){
		if(typeof mapPacteHome != "undefined" && typeof mapPacteHome.clearMap != "undefined") mapPacteHome.clearMap();
		 // allMaps.clear();
		mapLoadPacte=true;
	  pacte.initScopeObj();
	  var paramsMapPacte = {
	    zoom : 5,
	    container : "mapPacte",
	    activePopUp : true,
	    tile : "mapbox",
	    center : ["47.482649", "2.431357"]
	    // mapOpt:{
	    //   latLon : ["47.482649", "2.431357"]
	    // }
	  };
	  var mapPacteHome = mapObj.init(paramsMapPacte);
	  //timeoutLoadingPacte(0);
	  dataSearchPacte=searchInterface.constructObjectAndUrl();
	  dataSearchPacte.searchType = ["organizations"];
	  dataSearchPacte.indexStep=0;
	  pacte.mapDefault();
	  //dataSearchPacte.searchType = ["NGO","LocalBusiness","Group","GovernmentOrganization"];
	  dataSearchPacte.private = true;
	  //dataSearchPacte.sourceKey = costum.slug;
	  $.ajax({
	    type: "POST",
	    url: baseUrl+"/costum/pacte/mapsearch",
	    data: dataSearchPacte,
	    headers: { "Accept-Encoding" : "None" },
	    dataType: "json",
	    error: function (data){
	      mylog.log(">>> error autocomplete search"); 
	      mylog.dir(data);   
	      $("#dropdown_search").html(data.responseText);  
	      //signal que le chargement est terminé
	      loadingData = false;     
	    },
	    success: function(data){ 
	      mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
	      if(!data){ 
	        toastr.error(data.content); 
	      } 
	      else{ 
	      	resultMapGroup=data.result;
	        mapPacteHome.addElts(data.result, true);
	      }
	    }
	  });
}
	function lazyWelcome(time){
	  if(typeof CO3_Article != "undefined" && typeof pacte != "undefined"){
	  	if(!mapLoadPacte)
	  		initPacteMapView();
	    initWelcomePacte();
	  }else
	    setTimeout(function(){
	      lazyWelcome(time+200)
	    }, time);
	}
	jQuery(document).ready(function() {
		setTitle("Pacte pour la Transition");
		lazyWelcome(0);
	});


</script>