
<style type="text/css">
    footer{margin-top:0px;}
    #sub-doc-page{margin-top: 0px !important;}
    .content-input-scope-pacte #input-sec-search .shadow-input-header .input-global-search{
        border: 2px solid #5b2649;
        color: #5b2649;
        font-size: 20px;

    }
    .header-section hr{
      position: relative !important;
      bottom: 0px !important;
      margin-left: auto !important;
    }
    .resultTitle{
      text-align: center;
    line-height: 40px;
      margin-bottom: 30px;
      font-size: 18px;
    }
    .searchEntity{
      padding: 10px !important;
      font-size: 16px;
      margin-top: 10px !important;
    }
    .searchEntity .entityName{
      text-transform: inherit;
    }
    .searchEntity .entityName .subTiltle{
      font-size: 20px;
    }
</style>
<div id="sub-doc-page">
    <div id="start" class="section-home section-home-video">
        <div class="col-xs-12 content-video-home no-padding">
          <div class="col-xs-12 no-padding container-video text-center" style="max-height: 450px;overflow-y: hidden;">
            <img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/sengaer_bandeau.jpg' style="margin:auto;">
          </div>
        </div>
    </div>
    <div class="col-xs-12 section-separtor no-padding">
      <div class="col-xs-4 bg-orange"></div>
      <div class="col-xs-4 bg-blue"></div>
      <div class="col-xs-4 bg-orange"></div>
    </div>
    <div id="actus" class="section-home col-xs-12 col-md-10 col-md-offset-1 padding-20">
      <div class="col-xs-12 header-section text-center">
        <h3 class="title-section col-xs-12">
          Les listes signataires du Pacte pour la Transition
        </h3>
        <hr/>
      </div>
       <div class="col-xs-12 text-center" style="font-size: 20px;padding-right:30px;padding-top: 30px;">
        <span>Découvrez les engagements des listes candidates dans votre commune !</span>
      </div>
       <div class="col-xs-12 margin-top-20">
              <div class="col-xs-12 text-center content-input-scope-pacte"></div>
        </div>
    </div>
     <div class="col-xs-12 text-center">
        <a href="https://www.pacte-transition.org/#communiquer?preview=poi.5de7cb0d69086484548b4731" target="_blank" class="btn-redirect-home" style="display: inline-block;padding: 10px 25px;border-radius: 4px;">
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin">
                      Comment signer
                    </h4>
                </div>
            </div>
          </a>
        </div>
    <div id="results-candidatures" class="col-xs-12 col-sm-10 col-sm-offset-1 margin-bottom-50"></div>
   
</div>
<script type="text/javascript">
  function initSignature(){
    pacte.initScopeObj();
    coInterface.bindLBHLinks();  
  }
  function lazySignature(time){
    if(typeof pacte != "undefined")
      initSignature();
    else
      setTimeout(function(){
        lazySignature(time+200)
      }, time);
  }
  jQuery(document).ready(function() {
    setTitle("Signatures");
    lazySignature(0);
    
  });
</script>