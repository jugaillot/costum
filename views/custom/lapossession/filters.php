<?php
//$cssAnsScriptFilesModule = array(
//	'/css/ctenat/filters.css',
//);
//HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>
<style type="text/css">
	.projectsProgress{
		display: none;
	}
	.app-projects .projectsProgress{
		display: block;
	}
</style>
<div class="projectsProgress col-xs-12 col-md-10 col-md-offset-1 margin-bottom-20">
	<div class="col-xs-10 col-xs-offset-1">
		<img class="img-responsive col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/images/ctenat/light-bulb.png"/>
		<span class="col-sm-6 col-sm-offset-3 margin-top-10" style="font-size: 20px;text-align: center;text-transform: uppercase;">Contenu à venir rapidement</span>
	</div> 
</div>
<!-- <div id="filterContainer" class="col-xs-12 col-md-10 col-md-offset-1">
</div> -->

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
	 		domainAction : {
	 			view : "selectList",
	 			type : "category",
	 			name : "Domaine d'action",
	 			action : "category",
	 			list : costum.lists.domainAction
	 		}
	 	}
	 };
	//  if(pageApp=="search"){
	//  	paramsFilter.filters.status={
	//  		view : "selectList",
 // 			type : "filters",
 // 			field : "source.status.lapossession",
 // 			name : "Statuts",
 // 			action : "filters",
 // 			list : [ 
 //                "Territoire lauréat", 
 //                "CTE Signé"
 //            ]
	//  	};
	// }

	// if(costum.app == "reseau" ){
 //            paramsFilter.defaults = {
	// 		    fields : { "id" : userId }
	// 	    };
 //    }

	function lazyFilters(time){
	  if(typeof filterObj != "undefined" )
	    filterGroup = filterObj.init(paramsFilter);
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
</script>