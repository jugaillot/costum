<?php 
$cssScriptTheme = array(
		'/plugins/jsonview/jquery.jsonview.js',
		'/plugins/jsonview/jquery.jsonview.css',
		// '/plugins/jQuery/jquery-2.1.1.min.js',
		// '/plugins/jQuery/jquery-1.11.1.min.js',
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js'             
);

$cssAnsScriptFilesModule = array( 
  //'/leaflet/leaflet.css',
  //'/leaflet/leaflet.js',
  //'/css/map.css',
  //'/markercluster/MarkerCluster.css',
  //'/markercluster/MarkerCluster.Default.css',
  //'/markercluster/leaflet.markercluster.js',
  //'/js/map.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );
?>


	<div class="col-lg-12 col-md-12 no-padding" id="bandeau">

		<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauHome.jpg">
		
		<div id="pictos">
			<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauPictos.svg">
		</div>

		<div id="bandeauOvale">
			<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauOvale.svg">
		</div> 

		<img class="img-responsive" id="pictoBandeau" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/PictoBandeau.svg">
			<p class="" id="TextBandeau">
				ACTEURS ET POINTS D'INTÉRÊT
			</p>
	</div>

	<div class="hidden-xs col-md-2 col-lg-2 d-inline-block" id="searchBar">
		<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Je recherche (mot clé, une association, un point d'interet...)">
			<a data-type="filters" href="javascript:;">
				<span id="second-search-bar-addon-possession" class="text-white input-group-addon pull-left main-search-bar-addon-possession">
					<i class="fa fa-search"></i>
				</span>
			</a>
	</div> 

	<div id="dropdown" class="hidden-xs dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding">
	</div>

	<!--Ajout de la map -->
	<!-- <div class="mapContain"> -->
		<div style="margin-top: -1vw;" class="col-xs-12 mapBackground no-padding" id="mapPossession">
		</div>
	<!-- </div> -->

	<div class="container">
		<div class="col-xs-12 col-lg-12">
			<img class="img-responsive no-padding" id="bandeGrise" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Bande-Grise.svg">
			<img class="img-responsive no-padding col-xs-2" id="HexagoneVert" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/HexagoneVert.svg">

			<div class="centered" id="TextPicto">
				<span style="margin-left: 19%;">FILTRE</span> 
				<br> 
				<span style="font-weight: bold">THÉMATIQUE</span>
			</div>
		</div>
	</div>
	
<div id="bonhomme">
	<?php echo $this->renderPartial("costum.views.custom.lapossession.bonhomme"); ?>
</div>

<div class="ContainSection col-xs-12 no-padding">
	<div class="col-xs-6 no-padding" id="section1">
			<img class="img-responsive" id="citoyen" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Photo-Citoyen.jpg">
			<div>
				<img class="img-responsive no-padding col-xs-1 col-lg-1" id="iconesCitoy" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Hexacitoyen.svg">
				<a href="javascript:;" data-hash="#citoyens" class="lbh-menu-app " style="text-decoration : none;">
					<h5 class="titleCitoy">
						CITOYEN
					</h5>
				</a>
				<div class="col-xs-12 no-padding text-center" id="textCitoy">
					<p style="margin-top: 6%;font-size: 1.5vw;">
						JE TROUVE DES ASSOCIATIONS<br>
						JE CONSULTE LES POINTS D'INTÉRÊTS<br>
						JE GÈRE MON RÉSEAU<br>
						JE SUIS L'ACTUALITÉ DES ASSOCIATIONS
					</p>
				</div>
			</div>
	</div>

	<div class="col-xs-6 no-padding" id="section2">
			<img class="img-responsive" id="asso" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Photo-Asso.jpg">
		<div style="margin-top: 1%;">
			<a href="javascript:;" data-hash="#search" class="lbh-menu-app " style="text-decoration : none;">
				<img class="img-responsive no-padding col-lg-1 col-xs-1" id="iconeAsso" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/HexaAssociation.svg">
					<h5 class="titleAsso">
						ASSOCIATION
					</h5>
			</a>
			<div class="col-xs-12 no-padding text-center" id="textAsso">
				<p style="margin-top: 6%;font-size: 1.5vw;">
					JE CRÉE OU TROUVE MON ASSOCIATION<br>
					JE FÉDÈRE MON RÉSEAU<br>
					JE GÈRE MES PROJETS<br>
					JE CRÉE MES ACTUS & ÉVÈNEMENTS
				</p>
			</div>
		</div>
	</div>
</div>

	
	<div style="margin-top: 2%;margin-left: 9%;" class="col-xs-10 actualité">
		<p id="TitreActua">ACTUALITÉS</p>
		<img class="img-responsive no-padding col-lg-1 col-xs-1" id="HexaActu" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/HexaActu.svg">
		<img class="img-responsive no-padding col-xs-10" id="BandeauVert" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauVert.svg">
	</div>

	<div id="newsstream">
        <div class="" style="background-color: white;">
        </div>
    </div>

    <div class="text-center">
	    <a href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none; font-size:2rem;">
	    	<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive plus-m" style="margin: 1%;width: 3%;"><br>
	    	Voir plus d'actualités
	    </a>
    </div>

<script type="text/javascript">
	
var mapPossessionHome = {};
var paramsMapPossession  = {};

jQuery(document).ready(function(){
	setTitle("La Possession");
	$("#navbar").removeClass("navbar-collapse pull-right navbar-right margin-right-15").addClass("navbar-collapse pull-right navbar-right");
	initPossessionMapView();

	urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

	// mylog.log("C'EST ICIIIIIIIIIII",urlNews);

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html"); 
	
	var mapPossessionHome = {};
	var paramsMapPossession  = {};


	setTitle("La Possession");
	initPossessionMapView();


});

function initPossessionMapView(){
	possession.initScopeObj();
	paramsMapPossession = {
		zoom : 5,
		container : "mapPossession",
		activePopUp : true,
		tile : "mapbox",
		menuRight : true,
		mapOpt:{
			latLon : ["-21.115141", "55.536384"],
		}
	};

	mapPossessionHome = mapObj.init(paramsMapPossession);
	dataSearchPossession=searchInterface.constructObjectAndUrl();
	dataSearchPossession.searchType = ["organizations"];
	dataSearchPossession.indexStep=0;
	possession.mapDefault();
	// dataSearchPossession.searchType = ["NGO","LocalBusiness","Group","GovernmentOrganization"];
	dataSearchPossession.private = true;
	dataSearchPossession.sourceKey = costum.slug;
	var donnee = mapPossessionHome.data;
	$.ajax({
		type: "POST",
		url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
		data: dataSearchPossession,
		dataType: "json",
		error: function (data){
			mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
		},
		success: function(data){ 
			mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
			if(!data){ 
				toastr.error(data.content); 
			} 
			else{ 
				// $('#mapContent').html('');
				mapPossessionHome.addElts(data.results, true);
				setTimeout(function(){
					mapPossessionHome.map.panTo([-21.115141,55.536384]);
					mapPossessionHome.map.setZoom(10);
				},2000);
			}
		}
	});	
	
}

var possession={
	initScopeObj : function(){
		$(".content-input-scope-possession").html(scopeObj);
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["RE"]
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.popup.default = function(data){
			mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = mapCustom.custom.getThumbProfil(data) ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup +=  "Accéder à la page";
					popup += '</div></a>';
				popup += '</div>';
			popup += '</div>';
			return popup;
		};
	}
}

</script>