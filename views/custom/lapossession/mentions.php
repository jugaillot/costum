<div style="margin-top: 2%;" id="container-docs" class="col-md-offset-1 col-md-9 col-sm-12 col-xs-12 no-padding text-center"><div class="panel-heading border-light center text-dark partition-white radius-10" style="background-color: #089238">
	<span class="panel-title"> <i style="color: white" class="fa fa-gavel  faa-pulse animated fa-3x  "></i> <span style="font-size: 48px;color: white">Mentions Légales</span></span>
</div>

<div class="space20"></div>
<div style="margin-top:20px">
	
  	<div class="explainDesc"> 
    	En vous connectant sur ce site, vous acceptez sans réserves les présentes modalités. Aussi, conformément de l’Article n°6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance dans l’économie numérique, les responsables du présent site internet <a href="https://www.communecter.org/costum/co/index/id/lapossession"><b>www.communecter.org/costum/co/index/id/lapossession</b></a> sont :
  	</div>

  	<h1 class="homestead explainTitle radius-10" style="color:white;background-color: #089238"> Editeur du Site </h1>
  	<div class="explainDesc">
	    <ul>
	      <li>Association loi 1901 Open Atlas</li>
	      <li>Numéro de SIRET :  513 381 830 00027</li>
	      <li>Responsable editorial : Tibor Katelbach</li>
	      <li>56 rue Andy 97422 La Saline</li>
	      <li>Téléphone  :+262 6 93 91 85 32</li>
	      <li>Email : contact@communecter.org</li>
	      <li>Site Web : <a href="http://www.communecter.org">www.communecter.org</a></li>
	    </ul>
  	</div>

  	<h1 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Objet </h1>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
	     	L’objet de ce site est de communiquer sur La Possession, ses solutions, de publier sur son site et d’envoyer des informations aux internautes qui le souhaitent.<br/>
			Directeur de la publication : Open atlas<br/>
			Conception et réalisation : Pierre Goubeaux
	    </ul>
  	</div>

  	<h1 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Hébergement </h1>
  	<div class="explainDesc">
    	<ul>
	      <li>Hébergeur : OVH</li>
	      <li>France, Roubaix</li>
	      <li>Site Web :  <a href="http://www.ovh.com">www.ovh.com</a></li>
    	</ul>
  	</div>

	<h1 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Propriété intellectuelle </h1>
	  		<div class="explainDesc">
		  		<ul style="font-size: 18px;">
					Le site Internet <b>www.communecter.org/costum/co/index/id/lapossession</b>, sa structure générale, sa charte graphique et les textes y figurant sont protégés dans les conditions et limites prévues dans le Code de la propriété intellectuelle français.<br/>
					Le logo,les textes et la baseline ainsi que toutes œuvres intégrées dans le site sont propriétés de <b>OPEN ATLAS</b>.<br/>
					D’une manière générale, seules les reproductions pour un usage personnel, privé et exclusif de tout intérêt commercial sont autorisées (article L 122-5 du Code de la propriété intellectuelle).<br>

					Toute autre reproduction, représentation, utilisation ou modification, par quelque procédé que ce soit, de tout ou partie du site, de tout ou partie des différentes œuvres qui le composent, sans avoir obtenu l’autorisation préalable et expresse de <b>OPEN ATLAS</b> est strictement interdite et peut être pénalement réprimée notamment au
					titre de contrefaçon. <br/> Toute reproduction autorisée devra respecter les trois conditions suivantes : la gratuité de la diffusion, le respect de l’intégrité des documents reproduits (pas de modification ni altération d’aucune sorte) et la mention « Copyright © 2019 <b>OPEN ATLAS®</b> » claire et lisible.
		  		</ul>
		  	</div>

  	<h1 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Contenu </h1>
		<div class="explainDesc">
	  		<ul style="font-size: 18px;">
				Ce site est une présentation générale de La Possession. <br/> 
				Les informations textuelles, photographiques ou autres qui y sont présentées ont un caractère purement informatif et ne sauraient constituer un document contractuel susceptible de fonder une quelconque action en justice. <br/> OPEN ATLAS se réserve le droit de modifier les informations figurant dans ce site, à tout moment, sans préavis, compte tenu de l’interactivité du site, sans que cela puisse engager sa responsabilité.
			</ul>
  		</div>

	<h1 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Développement </h1>
  	<div class="explainDesc">
	    <ul>
	      <li>Collectif d'indépendant O.R.D.</li>
	      <li>Adresse : 56 rue Andy 97422 La Saline</li>
	    </ul>
 	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
	setTitle("Mentions Légales","");
});
</script>
</div>