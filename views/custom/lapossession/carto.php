<?php 
$cssAnsScriptFilesModule = array( 
	'/leaflet/leaflet.css',
	'/leaflet/leaflet.js',
	'/css/map.css',
	'/markercluster/MarkerCluster.css',
	'/markercluster/MarkerCluster.Default.css',
	'/markercluster/leaflet.markercluster.js',
	'/js/map.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );
?>

<!--Ajout de la map -->
	<div style="margin-top: -1.3%" class="col-xs-12 mapBackground no-padding" id="mapPossession">
	</div>

<script type="text/javascript">
	
var mapPossessionHome = {};
var paramsMapPossession  = {};

jQuery(document).ready(function(){
	setTitle("La Possession");
	initPossessionMapView();
});

function initPossessionMapView(){
	possession.initScopeObj();
	paramsMapPossession = {
		zoom : 5,
		container : "mapPossession",
		activePopUp : true,
		tile : "mapbox",
		menuRight : true,
		mapOpt:{
			latLon : ["-21.115141", "55.536384"],
		}
	};

	mapPossessionHome = mapObj.init(paramsMapPossession);
	dataSearchPossession=searchInterface.constructObjectAndUrl();
	dataSearchPossession.searchType = ["organizations"];
	dataSearchPossession.indexStep=0;
	possession.mapDefault();
	// dataSearchPossession.searchType = ["NGO","LocalBusiness","Group","GovernmentOrganization"];
	dataSearchPossession.private = true;
	dataSearchPossession.sourceKey = costum.slug;
	$.ajax({
		type: "POST",
		url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
		data: dataSearchPossession,
		dataType: "json",
		error: function (data){
			mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
		},
		success: function(data){ 
			mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
			if(!data){ 
				toastr.error(data.content); 
			} 
			else{ 
				// $('#mapContent').html('');
				mapPossessionHome.addElts(data.results, true);
				setTimeout(function(){
					mapPossessionHome.map.panTo([-21.115141,55.536384]);
					mapPossessionHome.map.setZoom(10);
				},2000);
			}
		}
	});	
	
}

var possession={
	initScopeObj : function(){
		$(".content-input-scope-possession").html(scopeObj);
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["RE"]
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.popup.default = function(data){
			mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = mapCustom.custom.getThumbProfil(data) ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup +=  "Rejoindre le collectif"
					popup += '</div></a>';
				popup += '</div>';
			popup += '</div>';
			return popup;
		};
	}
}
</script>