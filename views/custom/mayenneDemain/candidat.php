<style type="text/css">
    	.text-explain{
    		font-size: 22px;
    	}
    </style>
<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-8">L'histoire de Jean-Claude Lavandier</h3>
        	<hr>
        </div>

    <div class="col-xs-12">
        
        	<span class="col-xs-12 text-center">        
             <iframe width="560" height="315" src="https://www.youtube.com/embed/UClP_EE8Rjc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
             <hr style="width:40%; margin:20px auto; border: 4px solid #71b62c;">
    		</span>
    </div> 
     
     <div class="col-xs-12">
        
        	<span class="col-xs-12 text-left text-explain">        

Je suis né le 22 novembre 1952 à Marigné-Peuton, près de Château-Gontier. Je suis le 3è d’une fratrie de 5 enfants. J’ai vécu toute mon enfance à Mée, près de Craon où mon père était bourrelier cordonnier et auxiliaire des PTT. Ma mère, en plus de gérer les 5 enfants, travaillait toute la journée à domicile sur une machine à coudre pour une usine de matériel de camping.
Marié, 2 enfants (des garçons) et 4 petits-enfants (3 filles et 1 garçon)
<br/><br/>
<h3>Ma scolarité</h3>
Ecole primaire à Mée, collège publique à Craon et lycée d’état à Château-Gontier Baccalauréat D
<br/><br/>
Premier emploi d’été à 15 ans à la laiterie de Craon (maintenant Lactalis), puis pendant plusieurs années comme standardiste à la poste de Craon
<br/><br/>
<h3>Faculté de Médecine de Rennes</h3>
A la rentrée 1971-1972, première année de mise en place du numérus clausus. Mon passage à la faculté a été marqué par une grève de 3 mois en 1973, avec occupation de la fac pour dénoncer les effets pervers du numérus clausus et les effets à venir sur la démographie médicale attendus dans les années 2010. Très bonne école pour développer la conscience politique qui m’accompagne depuis. Pour ne rien vous cacher, c’est également le passage par un mouvement politique d’extrême gauche pendant deux ans environ comme beaucoup d’étudiants à cette époque, Mai 1968 n’est pas bien loin. A ce propos, pour moi mai 68 c’est le début de ma modeste carrière de musicien qui va durer 10 ans. Cette grève c’est également la rencontre avec la femme qui est mon épouse depuis 45 ans, sans interruption.
<br/><br/>
Durant ces 5 années à Rennes je travaille comme infirmier en réanimation durant mes vacances pour financer mes études. Expérience importante qui va marquer profondément mes relations avec les soignants dans mon exercice médical. Trop de médecins n’ont jamais eu cette immersion dans le monde des soignants.
<br/><br/>
<h3>1977</h3>
Début de l’internat à l’hôpital de Saint-Brieuc et naissance de notre premier fils.
<br/><br/>
<h3>1980</h3>
Naissance de notre deuxième fils.
<br/><br/>
Durant cet internat, j’effectue 2 stages, soit un an, comme urgentiste au SAMU 22, ce qui va beaucoup marquer mon parcours professionnel
<br/><br/>
<h3>Octobre 1981</h3>
Première installation comme médecin généraliste au Genest Saint Isle, près de Laval, dans un cabinet de groupe atypique qui a un projet de Centre de santé, l’ancêtre des maisons de santé pluriprofessionnelles que nous voyons fleurir aujourd’hui. Mais en 1981, nous avons toute la profession contre nous, de même que les responsables de l’assurance maladie, les mêmes qui aujourd’hui se vantent d’avoir mis en place un mode d’exercice innovant. Vous l’aurez compris, notre projet n’a pas pu se concrétiser. Nous avions trop d’avance.
<br/><br/>
Dès mon installation, je m’engage syndicalement et adhère au Syndicat de la Médecine Générale, syndicat de gauche qui s’oppose à la toute puissante CSMF, qui ne conçoit qu’un mode d’exercice celui de la médecine libérale avec le paiement à l’acte. De 85 à 89, je suis membre du bureau national du SMG dont je suis le trésorier national
<br/><br/>
Du point de vue exercice de la médecine, je m’intéresse aux médecines parallèles, je passe un diplôme d’acupuncteur, d’auriculothérapeute, et de médecine manuelle.
<br/><br/>
<h3>1989</h3>
Changement de cabinet et installation avec mon épouse à Montenay. Le Syndicat de la Médecine Générale adhère à une nouvelle structure MG France qui est toujours aujourd’hui un syndicat majeur en médecine générale. Je crée la section mayennaise de ce syndicat, est élu président et siège à ce titre au CA de la CPAM et représente les médecins généralistes de la Mayenne à l’Union Régionale des Médecins Libéraux.
<br/><br/>
Pendant ce passage à Montenay, je renoue avec les urgences en étant recruté comme médecin sapeur-pompier par le Centre de Secours d’Ernée. Je vivrai cette expérience inoubliable pendant 10 ans. Je participe, avec quelques collègues, à la mise en place d’un véritable service médical des sapeur- pompiers qui existe aujourd’hui. J’en profite pour passer mes diplômes de médecine d’urgence et de médecine de catastrophe.
<br/><br/>
<h3>1996</h3>
Sollicité par les médecins urgentistes du Centre hospitalier de Mayenne qui ont l’habitude de travailler avec moi lors les interventions des pompiers, je suis engagé comme médecin urgentiste en janvier 1996. Je vais exercer pendant 21 ans au CHNM. Parallèlement à mon activité d’urgentiste, je m’occupe du DIM, département d’information médicale, service médico-technique qui gère les données médicales des dossiers patients qui génèrent une partie des recettes de l’hôpital. Je vais être élu pendant près de 10 ans Président de la CME, en charge de l’organisation médicale des soins, à la tête de 60 à 70 médecins.
<br/><br/>
Durant ma dernière année d’exercice, je serai élu Président du collège médical du GHT de la Mayenne, avec pour mission l’élaboration du projet médical du territoire, associant les médecins hospitaliers et les médecins généralistes qui interviennent dans les hôpitaux locaux. Je m’acquitte de cette mission avec beaucoup de difficultés, mais j’y parviens à la grande surprise des directeurs d’hôpitaux et des présidents des conseils de surveillance et du président du conseil départemental qui pour cela me témoigne une certaine estime.
<br/><br/>
<h3>2008</h3>
Michel Angot me sollicite pour intégrer l’équipe municipale, je ne donne pas suite compte tenu de mes responsabilités à l’hôpital.
<br/><br/>
<h3>2014</h3>
Cette fois j’accepte la proposition compte tenu de la date de la retraite qui approche. Je suis conseiller délégué en charge de l’animation et du jumelage. Libéré de mes obligations professionnelles, Michel me confie une mission plus ambitieuse, la coordination de la politique du centre ville.
<br/><br/>
A l’automne 2017, la majorité des élus qui envisagent de briguer un nouveau mandat me sollicite pour mener la liste pour les municipales de 2020. Toute ma vie professionnelle a été au service des autres, mon engagement à la tête de cette liste n’est que la continuité de ma vie professionnelle, sous une autre forme. Comme je l’ai dit, si j’ai toujours eu une sensibilité de gauche, je suis un humaniste au service des autres.
<br/><br/>
Comme vous pouvez le constater, je n’ai pas de casseroles, pas d’ennui avec la justice, si ce n’est un procès avec le conseil de l’ordre des médecins dans les années 80 pour refus de paiement de ma cotisation considérant cette structure comme trop réactionnaire et s’opposant à tout progrès d’organisation de la médecine en France. Ce qui ne va pas empêcher un Président du Conseil de me demander de travailler avec lui pour mettre en place une plateforme informatique pour permettre aux personnels de santé de communiquer entre eux pour une meilleure prise en charge des patients.
<br/><br/>
Voilà vous connaissez tous les secrets de ma vie.
    		</span>
    </div> 
        
    </div>
</div>

<script>
jQuery(document).ready(function() {
    setTitle("Le Candidat"); 
});
</script>