<?php 
$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"mayenneDemain"),array("updated"=>-1), 3, 0);
?>
<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #71b62c;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #8a2f88;
    background-color: white;
    color: #8a2f88;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #8a2f88;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #71b62c;
  }
  .text-explain{
    color: #555;
    font-size: 25px;
  }
  .blue-bg {
  background-color: white;
  color: #8a2f88;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #71b62c;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #8a2f88;
    border: inset 3px #8a2f88;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #8a2f88;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #8a2f88;
    font-size: 40px;
    color: white;

}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  padding:0;
  margin-bottom: 0;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #cfc2ae;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline-ctc .timeline div {
  padding: 0;
  height: 40px;
}
.timeline-ctc .timeline hr {
  border-top: 3px solid #cfc2ae;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline-ctc .timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline-ctc .timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline-ctc .timeline .corner {
  border: 3px solid #cfc2ae;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline-ctc .timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline-ctc .timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline-ctc .timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline-ctc .timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
	
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>Mayenne Demain<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/banner.jpg'> 
  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#8a2e88; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                <span class="main-title">Construisez la commune de demain</span><br>
                <small>
                  <b>Les 15 et 22 mars 2020, Jean-Claude Lavandier et son équipe se présenteront aux élections municipales et communautaires. Aidez-nous à
                identifier et mettre en œuvre des mesures concrètes pour encourager la transition
                écologique, sociale et démocratique de Mayenne.<br/><span class="bullet-point"></span><br/>
                Vous, citoyennes et citoyens, êtes les mieux placés pour définir ces priorités et construire la ville de demain.<br>
                </small>
              </h3>
              <div class="col-xs-12">
                <hr style="width:40%; margin:20px auto; border: 4px solid #71b62c;">
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/UClP_EE8Rjc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
              </div>
            </div>
          </div>

        </div>

      </div>



     <div class="timeline-ctc col-xs-12 no-padding" style="font-weight: 300;height: 100%;text-align: inherit;">
             
              <div class="container-fluid blue-bg no-padding">
                <div class="container col-xs-12 no-margin no-padding">
                  <h2 class="pb-3 pt-2">
                    <i class="fa fa-calendar"></i> L'agenda
                  </h2>
                  <!--first section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center bottom">
                      <div class="circle">1</div>
                    </div>
                    <div class="col-6">
                      <h5>Octobre à Décembre 2019</h5>
                      <p>Rédaction collaborative du programme par les membres de la liste</p>
                    </div>
                  </div>
                  <!--path between 1-2-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--second section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Décembre 2019 et janvier 2020</h5>
                      <p>Proposez vos idées sur le site pour prise en compte dans le programme</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">2</div>
                    </div>
                  </div>
                  <!--path between 2-3-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <!--third section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle">3</div>
                    </div>
                    <div class="col-6">
                      <h5>Février 2020</h5>
                      <p>Publication du programme</p>
                    </div>
                  </div>
                   <!--path between 3-4-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--4th section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Février à Mars 2020</h5>
                      <p>Vous pouvez continuer à donner votre avis sur le site</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">4</div>
                    </div>
                  </div>
                  <!--path between 4-5-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <!--5th section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle active">5</div>
                    </div>
                    <div class="col-6">
                      <h5>15 & 22 Mars 2020</h5>
                      <p>Elections municipales et communautaires</p>
                    </div>
                  </div>
                   <!--path between 3-4-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--6th section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>À partir d'avril 2020</h5>
                      <p>Si la liste est élue, suivi de la réalisation du programme avec les citoyen⋅ne⋅s</p>
                    </div>
                    <div class="col-2 text-center top">
                      <div class="circle">6</div>
                    </div>
                  </div>
                </div>
              </div>
      </div>

   <div class="col-xs-12 no-padding support-section text-center">
        <h2 id="porterMayennedemain"><i class="fa fa-connectdevelop"></i> Je veux participer</h2>
        <div class="col-xs-12 padding-20 text-center text-explain" >
            
         Bonne nouvelle ! Alors voilà des pistes pour le faire :
          <br/><br/>
          <strong>Vous souhaitez proposer une idée pour le programme ?</strong>
          <br/>
          
          <div class="text-center col-xs-12">
           <a href="#dda" class="lbh btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-5 margin-top-20 margin-bottom-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Je contribue") ?>
                      </h4>
                  </div>
              </div>
          </a>
          </div>
          <br/><br/>
          
          Jean-Claude Lavandier et son équipe participent à des réunions chez l'habitant pour discuter avec vous.<br/>
         <strong>Vous souhaitez organiser une réunion chez vous ?</strong>
           <br/>
           
           <div class="text-center col-xs-12">
           <a href="mailto:contact@mayenne-demain.fr" target="_blank" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-5 margin-top-20 margin-bottom-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Nous écrire") ?>
                      </h4>
                  </div>
              </div>
          </a>
          </div>
           <br/><br/>
           
           <strong>Vous souhaitez soutenir financièrement la campagne de la liste Mayenne Demain ?</strong>
           <br/>
           <div class="text-center col-xs-12">
                <a href="#don" class="lbh btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-5 margin-top-20 margin-bottom-20"  >
                <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Faire un don") ?>
                      </h4>
                  </div>
                </div>
                </a>
            </div>
          
          
          <br/><br/>
            Vous êtes également les bievenu⋅e⋅s au local de campagne <br/>
            <strong>20 rue Aristide Briand à Mayenne</strong>
            <br/><br/>
         
            Ainsi qu'à notre réunion publique :<br/>
            <strong>le lundi 09 mars 2020 à 20h au Théâtre Municipal.</strong>
            <br/><br/>
          <div class="text-center col-xs-12"><br/> <br/>...Et n'hésitez pas à en parler tout autour de vous !!</div>
        </div>
      </div>
<div style="margin-top: 2%;margin-bottom: 2%;margin-left: 9%;" class="col-xs-10">
          <h1 class="text-white text-center">L'actualité de #MayenneDemain</h1>
        </div>
      <div class="col-xs-12 no-padding support-section text-center">
        

        <div class="col-xs-12" id="actus-mayennedemain" style="background-color: white;"></div>

          <div class="text-center">
            <a href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none; font-size:2rem;">
              <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/mayenneDemain/plus.svg" class="img-responsive plus-m" style="margin: 1%;width: 3%;"><br>
              Voir plus d'actualités
            </a>
          </div>
      </div>

<script type="text/javascript">

	jQuery(document).ready(function() {
		setTitle("Mayenne Demain");
		urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";
    ajaxPost("#actus-mayennedemain",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html"); 
		
	});


</script>