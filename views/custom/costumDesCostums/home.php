<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

    $cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
    );
    
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
    
    $poiList = array();
    if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
        $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
    
        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                               "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                               "type"=>"cms") );
                               
    } 

    //  var_dump($poiList);
    // var_dump(Yii::app()->session["costum"]["contextId"]);
    ?>
    <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace&display=swap" rel="stylesheet"> 
    <link href="//db.onlinewebfonts.com/c/cb18f0f01e1060a50f2a273343bb844e?family=Homestead" rel="stylesheet" type="text/css"/>
<style>
@font-face {
    font-family: "Homestead"; src: url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.eot"); src: url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.woff") format("woff"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.svg#Homestead") format("svg"); 
    font-family : "Helvetica"; src: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/font/costumDesCostums/Helvetica.ttf") format("ttf");
} 
  .header{
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/photo.jpg);
    background-size: cover;
    padding-bottom: 49%;
}
.intro{
    background-color: #ededed;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: -3%;
    padding-bottom: 3%;
}
.intro-description{
    background: white;
    text-align: center;
    padding-top: 3%;
    padding-bottom: 3%;
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    border: solid 1px silver;
}
.intro-description > h1,h2,h3,h4,h5,h6{
    font-family: Homestead !important;
}
.projet-title{
    text-align : center;
    border-top: dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    border-bottom:dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    color: <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.projet{
    margin-top:3%;
    margin-left: 10%;  
    margin-right: 10%;
}
.projet-affiche{
    margin-top: 3%;
    background-color:#ededed;
    box-shadow: 0px 0px 20px -2px #878786;
    padding-bottom: 3%;
}
.explication{
    margin-top:3%;
    margin-left: 10%;  
    margin-right: 10%;
}
.explication-title{
    text-align : center;
    border-top: dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["bg-green"]; ?>;
    border-bottom:dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["bg-green"]; ?>;
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["bg-green"]; ?>
}

.explication-img {
    text-align: center;
}
.tarif{
    margin-top:3%;
    background-color:#f8f9f9;
    margin-bottom : 2%;
}
.tarif-title {
    text-align : center;
    border-top: dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    border-bottom:dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    margin-top: 5%;
    margin-bottom: 5%;
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.tarif-border{
    background-color: #ededed;
    padding: 2%;
    box-shadow: 0px 0px 20px -2px #878786;
}
.tarif-c{
    border-right : dashed 1.5px black;
    text-align : center;
}
.tarif-cs{
    text-align : center;
}
.bouton{
    margin-top: 2%;
    margin-left: 10%;
    margin-right: 10%;
}
.bouton-img {
    text-align: center;
}
.tarif-c > p {
    margin-top: 5%;
    color : #878786;
    font-size: 2.25rem;
}
.tarif-cs > p {
    margin-top: 5%;
    color : #878786;
    font-size: 2.25rem;
}
.tarif-c > h2{
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.tarif-cs > h2{
    color: <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.bouton-img > span {
    background: white;
    border-radius: 20px 20px 20px 20px;
    padding: 2.3%;
    padding-left: 5%;
    padding-right: 5%;
    border: black 1px solid;
}
.tarif-description > p {
    font-style: italic;
    text-align: center;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: 3%;
    margin-bottom: 3%;
    color: #878786;
}
.btn-plus-tarif{
    margin-top: -3%;
    padding: 2%;
}
.img-projet{
    width: 100%;
    height: 50%;
}
.modal-header{
    background-color: #94c138;
    color: white;
}
.center{
    margin-left : 10%;
    margin-right: 10%;
}
.img-expli{
    width: 60%;
}
.m-plus{
    width : 5%;
}
.costum-m{
    height:300px;
    overflow-y:auto; 
    background-color: white;
    border: solid 3px #ededed;
}
.costum-m > a {
    text-decoration : none;
}
@media (max-width:768px){
    .tarif-title > h1{
        font-size : 3rem;
    }
    .tarif-c > p {
        font-size: 2rem;
    }
    .tarif-cs > p {
        font-size: 2rem;
    }
    .tarif-c > h1{
        font-size: 3rem;
    }
    .tarif-cs > h1{
        font-size: 3rem;
    }
    .tarif-c{
        border-bottom : dashed 1.5px black;
        margin-top : 2%;
    }
    .bouton-img > span{
        font-size: 1.5rem;
        padding: 5%;
    }
    .img-projet {
        width: 100%;
        height: 75%;
    }
    .projet-mobile{
        font-size: 1rem;
        line-height: 9px;
    }
    .bouton-img{
        margin-top : 10%;
        font-size: 1rem;
    }
    .tarif-title > h1{
        font-size : 3rem;
    }
    .center{
        margin-left: 0%;
        margin-right:0%;
    }
    .explication{
        margin-left: 0%;
        margin-right:0%;
    }
    .explication-title > h1{
        font-size : 3rem;
    }
    .explication-img > h1 {
        font-size : 2.5rem;
    }
    .img-expli{
        width: 80%;
    }
    .m-plus{
        width : 15%;
    }
    .poi-m{
        margin-left: -4%;
    }
    .costum-m{
        height:200px;
        overflow-y:auto; 
        background-color: white;
        border: solid 3px #ededed;
    }
    .projet{
        margin-left: 0%;
        margin-right: 0%;
    }
    .projet-title > h1{
        font-size : 3rem;
    }
    .intro-description{
        font-size: 3rem;
    }
}
</style>
<div class="header">
<center class="col-xs-12 col-sm-12">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/header.png" class="img-responsive" style="margin-top: 3%; width:80%"  usemap="#headerimg">

        <map name="headerimg">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
        <!-- #$-:Please do not edit lines starting with "#$" -->
        <!-- #$VERSION:2.3 -->
        <!-- #$AUTHOR:Tony Emma -->
            <area shape="circle" coords="450,97,92" alt="Costum pour le pacte de la transition" href="https://pacte-transition.org" target="_blank" />
            <area shape="circle" coords="501,346,68" alt="Costum du site Open Altas" target="_blank" href="http://www.open-atlas.org/" />
            <area shape="circle" coords="736,131,75" alt="Site du costum de la Raffinerie" href="https://www.communecter.org/costum/co/index/id/laRaffinerie" target="_blank" />
            <area shape="circle" coords="770,442,90" alt="Costum de la filière Numérique" href="https://www.communecter.org/costum/co/index/id/coeurNumerique" target="_blank" />
        </map>

</center>
</div>

<div class="intro row">
    <div style="margin-top: 3%; margin-bottom: 0%; margin-left: 3%; margin-right: 3%;">
        <div class="intro-description col-xs-12 col-sm-12">
            <h1>Créer votre propre réseau sociale personnalisée !</h1>
            <h2>Votre design, vos données et innover </h2>
            <h4> Tout en contribuant aux communs</h4>
        </div>
    </div>
</div>

<div class="projet row">
    <div class="projet-title col-xs-12 col-sm-12">
    <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Nos exemples de costum</h1>
    </div>

    <div class="projet-affiche col-xs-12 col-sm-12">
        <div class="col-lg-12 text-center" id='content-results-persons' style="margin-top:2vw;">
				<!-- içi que s'affiche les persons --> 
			</div>
        <!-- <div class="col-xs-12 col-sm-12 no-padding"> -->
        <?php 
        // // $costums = array_merge(PHDB::findAndSortAndLimitAndIndex("costum",array(),array("_id" => -1),1),PHDB::findAndLimitAndIndex("costum",array(),2));
        // $costums = PHDB::findAndSortAndLimitAndIndex("costum",array(),array("_id" => -1),3);

        // $html="";
        // foreach($costums as $key=> $v){
        //     if(@$v["slug"]){
        //     $el = Slug::getElementBySlug($v["slug"], ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl", "name", "tags", "description"]);
        //     $el=$el["el"];
        //     $img=$this->module->getParentAssetsUrl()."/images/wave.jpg";
        //     if (@$v["metaImg"]) 
        //         $img = Yii::app()->getModule( $this->module->id )->getAssetsUrl().$v["metaImg"];
        //     else if(!empty($el["profilMediumImageUrl"]))
        //         $img = Yii::app()->createUrl($el["profilImageUrl"]);
        //     $html.="<div class='col-md-4 costum-m'>".
        //             "<img src='".$img."' class='img-responsive img-projet'/>".
        //             "<a href='".Yii::app()->createUrl("/costum/co/index/id/".$v["slug"])."' target='_blank'>".
        //                 "<h3>".$el["name"]."</h3>".
        //                 "</a>".
        //                 "<span class='projet-mobile'>".@$el["shortDescription"]."</span>".
        //                 "<div class='projet-mobile letter-red'>";
        //                 if(!empty($el["tags"])){
        //                     foreach($el["tags"] as $v){
        //                         $html.="#".$v;
        //                     }
        //                 }
        //                 $html.="</div></div>";
        //         }
        // }
        // echo $html;
    ?>
        <!-- </div> -->
    </div>

    <div class="col-xs-12 col-sm-12" style="text-align:center; margin-top: 2%;">
    <center>
    <a href="javascript:;" data-hash="#searchcostum" class="lbh-menu-app">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive m-plus">
    </a>
    </center>
    </div>
</div>

<div class="explication row">
    <div class="explication-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Explication</h1>
    </div>
    <div class="col-xs-12 margin-top-20 poi-m">
                    <?php 
                    $params = array(
                        "poiList"=>$poiList,
                        "listSteps" => array("1","2","3","4"),
                        "el" => $el,
                        "color1" => Yii::app()->session["costum"]["css"]["color"]["bg-green"]
                    );
                    echo $this->renderPartial("costum.views.tpls.wizard",$params); ?>
    </div> 
    <div class="explication-img col-xs-12 col-sm-12 no-padding">
            <center> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/illu.svg" class="img-responsive img-expli"></center>
            <h1 style="color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>; font-family: 'Covered By Your Grace', cursive !important; margin-top: 4%;">
            « Contribuer aux communs avec un contexte personnalisé. »
            </h1>
    </div>
</div>

<!-- Expérimentée --> 


<div class="experimente row">
    <div class="center">
        <div class="tarif-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Expérimenter</h1>
        </div>
    </div>
    
    <!-- Les tarifs -->
    <div style="margin-left: 2%; margin-right: 2%;">
        <div class="tarif-border col-xs-12 col-sm-12">
        <div class="tarif-c bouton-img col-md-4">
            <h1><i class="fa fa-lightbulb-o" aria-hidden="true"></i>
 Votre idée</h1>
            <p>Descriver votre<br> costum de rêve</p>
            </div>
            <div class="tarif-c bouton-img col-md-4">
            <h1><i class="fa fa-mouse-pointer"></i> En un clic</h1>
            <p>Testez-les templates<br> existant à partir de communecter</p>
            
            </div>
            <div class="tarif-cs bouton-img col-md-4">
            <h1><i class="fa fa-user-circle-o" aria-hidden="true"></i>
 En autonomie</h1>
            <p>Débrouillard,<br> faites le vous-mêmes</p>
           
            </div>
        </div>
        <div class="col-xs-12" style="margin-top: -1%;">
            <div class="bouton-img col-md-4">
            <span><a onclick="dyFObj.openForm('project');" style="text-decoration : none" href="javascript:;">Vous avez une idée de costum</a></span>
            </div>    
            <div class="bouton-img col-md-4">
            <span>Votre costum en un clic</span>
            </div> 
            <div class="bouton-img col-md-4">
            <span><a href="https://doc.co.tools/books/2---utiliser-loutil/page/costum" target="_blank" style="text-decoration : none;"> Documentation</a></span>
            </div>   
        </div>
        </div>


    <div class="tarif-description col-xs-12 col-sm-12">
   <p style="font-style: italic; text-align: center; margin-left: 10%; margin-right: 10%; margin-top: 3%; margin-bottom: 3%; color: #878786;">
   Sed ligula erat, tincidunt vel ligula semper, pretium aliquam ipsum. Maecenas congue eros metus, id vehicula sapien posuere a. Fusce ac felis magna. Mauris a ultricies enim, a aliquet risus. Nunc suscipit maximus elementum. Nulla sed nisi nec justo scelerisque porttitor et eu dolor. Integer bibendum volutpat elit, ac volutpat est imperdiet vitae. Phasellus dignissim dapibus neque, sit amet molestie lectus aliquam eget. 
   </p>
    </div>
</div>


<div class="tarif row">
    <div class="center">
        <div class="tarif-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Tarifs</h1>
        </div>
    </div>
    
    <!-- Les tarifs -->
    <div style="margin-left: 2%; margin-right: 2%;">
        <div class="tarif-border col-xs-12 col-sm-12">
                <div class="tarif-c col-md-3">
                <h1>2000 €</h1>
                <p> Association - <br> de 50 <br> adhérents </p>
                </div>
                <div class="tarif-c col-md-3">
                <h1>2800 €</h1>
                <p> Association + <br> de 50 <br> adhérents </p>
                </div>
                <div class="tarif-c col-md-3">
                <h1>3200 €</h1>
                <p> Coopératives <br> et entreprises <br> responsable </p>
                </div>
                <div class="tarif-cs col-md-3">
                <h1>Devis</h1>
                <p> Collectivé <br> et autres <br>entreprise </p>
                </div>
        </div>
        </div>
        <div class="btn-plus-tarif col-xs-12 col-sm-12">
                <div class="col-md-3">
                    <center>
                        <a href="#" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center> 
                </div>
                <div class="col-md-3">
                    <center>
                        <a href="#" onclick="changeModal('first')" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center>
                </div>
                <div class="col-md-3">
                    <center>
                        <a href="#" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center>
                </div>
                <div class="col-md-3">
                    <center>
                        <a href="#" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center>
                </div>
        </div>

    <div class="tarif-description col-xs-12 col-sm-12">
   <p style="font-style: italic; text-align: center; margin-left: 10%; margin-right: 10%; margin-top: 3%; margin-bottom: 3%; color: #878786;">
       Nous sommes soucieux de proposer un tarif juste et équitable.
       Nous le fixons en fonction de la taille de votre organisation et vos besoins de personnalisations. <br />
       Nous privilégions le système d’abonnement mensuel. Si ce n’est pas possible merci de nous contacter. <br />
       Le prix fixe correspond à la mise en place du site, et le coût mensuel à la participation au commun.

   </p>
    </div>
</div>

<!-- <div class="bouton row">
        <div class="col-xs-12 col-sm-12" style="margin-bottom: 2%;">
  
        </div>
</div> -->


<!-- Modal -->
<div class="modal fade" id="descriptionTarif" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.5);">
  <div class="modal-dialog modal-lg" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Les tarifs</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -4%">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p> La participation mensuelle permet d’améliorer le commun et donne droit à l’assistance mail et chat. </p>
       <p>
Le prix fixe comprend : <br />
<ul>
    <li>Personnalisation d’une de nos maquettes : ajout du logo, couleurs et textes</li>
    <li>Choix des fonctionnalités (<a href="https://doc.co.tools/books/2---utiliser-loutil/page/liste" target="_blank">voir la liste</a>)</li>
    <li>500 Mo d’espace de stockage</li>
    <li><b>Formation de 3 heures à la prise en main de l’outil</b></li>
</ul>
</p>
<p>
    Il est bien sûr possible d’aller plus loin (devis sur simple demande) : <br />
    <ul>
        <li>Importer des données</li>
        <li>Organiser un événement convivial de référencement collectif (cartopartie)</li>
        <li>Créer une maquette personnalisée</li>
        <li>Intégrer votre maquette réalisée chez un autre prestataire</li>
        <li>Créer une nouvelle fonctionnalité</li>
        <li>Organiser un concours</li>
    </ul>
</p>
      </div>
    </div>
  </div>
</div>
<script>
 jQuery(document).ready(function() {
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    contextData = {
        id : "<?php echo Yii::app()->session["costum"]["contextId"] ?>",
        type : "<?php echo Yii::app()->session["costum"]["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
    
    // autoCompleteSearchGS("", 0,3);
    globalSearchCostum();
});

// GLOBALSEARCHCOSTUM
function globalSearchCostum(){
    // autoCompleteSearchGS(search,1,3);
    mylog.log("---------------- moduleId", moduleId);

    var data = {"searchType" : "costum", "searchBy" : "ALL",
    "indexMin" : 1, "indexMax" : 3, "limit" : 3 };
    
    $.ajax({
		type: "POST",
		url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
		data: data,
		dataType: "json",
		error: function (e){
			mylog.log("error"); mylog.dir(e);          
		},
		success: function(a){
            mylog.log("success autoglobalsearch --------------------", a);
            
            var i = data.indexMin;
            // Gestion affichage des costums
            var str = "<div class='costum-search'>";

            $.each(a.results, function(k, v) {
                if(i <= data.indexMax){
                    mylog.log("globalcostum res",v);
                    i++; // Permettra de count le nombre de projet
                    var shortDescription = (notEmpty(v.shortDescription)) ? v.shortDescription : "";
                    var images = (notEmpty(v.profilMediumImageUrl)) ? baseUrl+v.profilMediumImageUrl : "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.costum.defaultImg;

                    // Initilisation de certains choses
                    str += "<div class='col-md-4 costum-m'>";
                    str += "<a href='"+baseUrl+"/costum/co/index/slug/"+v.slug+"' target='_blank'>";
                    str += "<img src='"+images+"' class='img-responsive img-projet'>";
                    str += "<h4>"+v.name+"</h4>";
                    str += "<p>"+shortDescription+"</p>";

                    str += "<div class='letter-red'>";
                    if(notEmpty(v.tags)){
                       v.tags.forEach(value =>
                        str += "#"+value );
                    }
                    str += "</div>";

                    str += "</a></div>";
                }
                else 
                    return false;
            });
            
            str += "</div>";
            mylog.log(str);
            $("#content-results-persons").html(str);
        }
    });
}
</script>