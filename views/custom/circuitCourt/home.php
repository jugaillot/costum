<div class="no-padding col-xs-12">


    <style type="text/css">
      #customHeader{
        margin-top: 0px;
      }
      #costumBanner{
       /* max-height: 375px; */
      }
      #costumBanner h1{
        position: absolute;
        color: white;
        background-color: rgba(0,0,0,0.4);
        font-size: 29px;
        bottom: 0px;
        padding: 20px;
        text-align:center;
      }
      #costumBanner h1 span{
        color: #eeeeee;
        font-style: italic;
      }
      #costumBanner img{
        min-width: 100%;
      }
      .btn-main-menu{
        background: #3595a8;
        border-radius: 20px;
        padding: 20px !important;
        color: white;
        cursor: pointer;
        border:3px solid transparent;
        /*min-height:100px;*/
      }
      .btn-main-menu:hover{
        border:2px solid #3595a8;
        background-color: white;
        color: #1b7baf;
      }
      .ourvalues img{
        height:70px;
      }
      .main-title{
        color: #3595a8;
      }

      .ourvalues h3{
        font-size: 25px;
      }
      .box-register label.letter-black{
        margin-bottom:3px;
        font-size: 13px;
      }

       .participate {
    background-color: white;
      }
     
       .participate i{
      
    float: left;
    margin-right: 20px;
    color: #ef5b2b ;

      }

      .participate-content{
        font-size: 17px;
      }
     

      @media screen and (min-width: 450px) and (max-width: 1024px) {
        .logoDescription{
          width: 60%;
          margin:auto;
        }
      }

      @media (max-width: 1024px){
        #customHeader{
          margin-top: -1px;
        }
      }
      @media (max-width: 768px){

      }

    .mapBackground{
        /*background-image: url(/ph/assets/449afa38/images/city/cityDefaultHead_BW.jpg);*/
        background-color: #f6f6f6;
        background-repeat: no-repeat;
        background-position: 0px 0px;
        background-size: 100% auto;
        height: 500px;
    }
    </style>

    <div class="col-xs-12 no-padding" id="customHeader" style="background-color: white; display : none">
      <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">

    <?php

  //  $baseUrl = "127.0.0.1/ph";
  //  typeItem  organizations;
  //  contextData.id="5cab04021efec9fa1d253f32";
    
    ?> 
    
      <h1 class="col-xs-6 col-sm-6 col-sm-offset-3 col-xs-offset-3">La Compagnie des Tiers-Lieux</h1>
        <img style="height:300px;" class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/laCompagnieDesTierslieux/banner.jpg'> 
      </div>
    </div> 

    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
        
          
        <div class="no-padding col-xs-12 " style="background-color: #f6f6f6; min-height:400px;">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left no-padding shadow2" style="background-color: #fff;font-size: 14px;z-index: 5;">
                <div class="col-xs-12 no-padding participate" style="padding-top:0px !important; padding-left:100px; margin-top:0px;">
                        <h2 class="col-xs-12 text-center" style="/*font-family:'Pacifico', Helvetica, sans-serif;*/color: #00a5a5;   font-size: 30px">
                        <small style="color: #333;text-align:justify !important;">
                            Ce site présente une cartographie des différents acteurs des circuits courts de la Réunion <br>
                            en rapport avec l'alimentation avec des AMAP, des épiceries en vrac et des jardins partagés. <br> <br>
                            Ce référencement n'est pas exhaustif et sera mise à jour au fur et à mesure, si vous connaissez des initiatives dans ces domaines, n'hésitez pas à nous les partager!!<br/>

                           <!-- <a href="#search" class="lbh lbh-menu-app"> Aggrandir la carte </a><br/> -->
                        </small>   
                        </h2><br/>
                    <br/>
                    <div class="col-xs-12 padding-10 text-center">
                    	<button class="btn btn-default tagCC bg-red text-white " data-tag="all">Tous</button>
                    	<button class="btn btn-default tagCC bg-dark text-white" data-tag="Epicerie">Epicerie</button>
                    	<button class="btn btn-default tagCC bg-dark text-white" data-tag="Amap">Amap</button>
                    	<button class="btn btn-default tagCC bg-dark text-white" data-tag="Jardin partagé">Jardin partagé</button>
                      <button class="btn btn-default tagCC bg-dark text-white" data-tag="Producteur">Producteur</button>
                      <button class="btn btn-default tagCC bg-dark text-white" data-tag="Tierslieux">Tiers lieux</button>
                    </div>
                    <div class="col-xs-12 mapBackground no-padding" id="mapCircuit"></div>
                </div>
            </div>

        
        </div>
    
    </div>

</div>

<script type="text/javascript">
var mapCircuitHome = {};
var dataSearchCC = {} ;
jQuery(document).ready(function() {
    setTitle("Circuit Court");
    dataSearchCC=searchInterface.constructObjectAndUrl();
    dataSearchCC.searchType = ["projects", "organizations"];
    dataSearchCC.sourceKey = "circuitCourt";

    var paramsMapCC = {
		container : "mapCircuit",
		activePopUp : true,
		tile : "mapbox2",
		forced : {
			latLon : [-21.135745255030592,55.52918591952641],
			zoom : 10
		}
		//menuRight : true
    };
    mapCircuitHome = mapObj.init(paramsMapCC);
    allMaps.maps[paramsMapCC.container]=mapCircuitHome;
    $.ajax({
        type: "POST",
        url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
        data: dataSearchCC,
        dataType: "json",
        error: function (data){
			mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
        },
        success: function(data){ 
            mylog.log(">>> success autocomplete search data in cc!!!! ", data); //mylog.dir(data);
            if(!data){ 
              toastr.error(data.content); 
            } else { 
              mapCircuitHome.addElts(data.results);
              allMaps.maps[paramsMapCC.container]=mapCircuitHome;
              //j {lat: -21.135745255030592, lng: 55.52918591952641}
              setTimeout(function(){
                //mapCircuitHome.map.setView([-21.135745255030592,55.52918591952641], 10);
              },1000);
            }
        }
    });
	//mapCircuitHome.map.setZoom(mapCircuitHome.mapOpt.zoom);
    $(".tagCC").click(function () {
		mylog.log(".tagCC", $(this).data("tag"));
		$(".tagCC").removeClass("bg-red").addClass("bg-dark");
		mapCircuitHome.filtres.tagsActived = [];
		if($(this).data("tag") != "all")
			mapCircuitHome.filtres.tagsActived.push($(this).data("tag"));
		$(this).removeClass("bg-dark").addClass("bg-red");
		mapCircuitHome.clearMap();
		mapCircuitHome.addElts(mapCircuitHome.data);
		
	});
});
 

</script>


