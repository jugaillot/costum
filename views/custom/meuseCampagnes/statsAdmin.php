 /* Exercice pour créer un tableau de bord de statistics de la donnée sur le costum de MeuseCampagnes
Lien du site : www.communecter.org/costum/co/index/id/meuseCampagnes
L'idée est de fournir des résultats trimestriels au financeurs pour justifier l'usage de la plateform
Il faut liste pour deux dates précises choisient par l'admin, le nombre de :
	- Organisations créées par les utilisateurs [collection : organizations]
	- Projets créés par les utilisateurs [collection : projects]
	- Projets créés par les utilisateurs [collection : events]
	- Annonces créés par les utilisateurs [collection : classifieds]
	- News créés par les utilisateurs [collection : news]
		- Le nombre de réactions qu'elles ont générés [collection : stocker dans news]
		- Le nombre de commentaires qu'elles ont générés   [collection : comments]
L'idée est donc de créer une vue dans l'espace administrateur de MeuseCampagne qui redirige sur cette vue si :

I - Préparer l'environement de travail
	[x] S'ajouter l'alias roles : {superAmdin : true} sur son utilisateur pour avoir accès à l'espace admin
	[x] Créer l'entrée dans le json qui permet d'afficher dans le menu admin "Statisques trimestriels"
	[x] Créer la redirection sur le bouton pour arriver dans cette vue (cf : /assets/meuseCampagnes/js/admin.js )
	[x] Obtenir le "Salut les gars" pour pouvoir commencer le chantier

II - Créer la vue statique avec les différents critères décrits ci desssus 
	[x] Input choissisez vos dates et bouton "générer"
	[x] Les résultats qui en découleront
	[x] Mettre un peu style en respectant la charte graphique du costum tout en restant très soft

III - Partie dynamique php - créer un controlleur pour rendre tout ça dynamique
	[x] Ajouter un fichier /costum/controllers/actions/meuseCampagnes/admin/StatisticsAction.php
	[x] copier, coller et adapter la structure d'une autre action de ce dossier (ex : SaveEventAction.php) pour qu'il soit opérationnel
	[x] Ajouter l'alias du chemin dans MeusecampagnesController.php

IV - Partie dynamique ajax - créer la partie js / ajax dans ce fichier qui va appeler le controller
	[x] Créer l'event jquery sur le button "générer"
	[ ] Ajouter dans cette event l'ajax qui permet d'aller jusqu'au fichier du controller
	[x] Rendre la saisie de date opérationnelle en utilisant le plugin de calendrier
		#$(".classDesDeuxInputs").datetimepicker({ 
		#        autoclose: true,
		#        lang: "fr",
		#        format: "d/m/Y",
		#        timepicker:false
		#    });
	[ ] Récupérer les dates dans l'ajax et l'envoyer au controller

V - Partie métier - Dans un premier sans prendre en compte les dates !!!
	[ ] Compte les organizations/projets/events/classifieds/news/comments créées sur le costum "meuseCampagnes"
		[ ] Chaque élément ajouté dans un costum contient une clé dans sa donnée "source : {key : meuseCampagnes}"
		[ ] Faire les requetes pour chaque collection en suivant cette exemple
			# PHDB::count({$nomDeLaCollection}, $conditions);
			[]  {$nomDeLaCollection} == ex : Document::COLLECTION pour cela ça peut être l'occasion de découvrir les models dans le module citizenToolkit afin de voir comment appeler chaque collection
			[] $condition == récupérer la donnée liée à la source. La condition est un tableau php ex : 
				# array( "type" => Organization::COLLECTION );
		[ ] Ajouter les résultats à un tableau $res qui sera renvoyé à l'appel ajax 

VI - Rendre dynamique la vue static 
	[ ] Passer dans une fonction js renderStats la structure html et l'ajouter au container 
	[ ] Avec les résultats afficher les bons numéros 

VII - Chapeau bas les gars et pour les réactions sur les news, je vous laisse me dire comment on peut récupérer tout ça ;) 


*/ 

