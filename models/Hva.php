<?php

class Hva {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	public static function prepData($params){
        if(!empty(Yii::app()->session["costum"] ) ) {
            if( !empty($params["source"]) && !empty($params["source"]["toBeValidated"]) ) {

            	if(!empty($params["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]]))
            		unset($params["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]]);
                $validated = true;
                if( !empty(Yii::app()->session["costum"]["typeObj"]) && 
                    ( (!empty(Yii::app()->session["costum"]["typeObj"][$params["collection"]]) &&
                        !empty(Yii::app()->session["costum"]["typeObj"][$params["collection"]]["validatedParent"]) ) ||
                        (!empty(Yii::app()->session["costum"]["typeObj"][$params["key"]]) &&
                        !empty(Yii::app()->session["costum"]["typeObj"][$params["key"]]["validatedParent"]) ) ) ) {

                    if($params["collection"] == Event::COLLECTION && !empty($params["organizer"])) {
                        $parent = "organizer";
                    }else{
                        $parent = "parent";
                    }
                    $validated = false;
                    foreach ($params[$parent] as $key => $value) {
                        if(!empty($value["type"])){
                            $eltParent = PHDB::findOneById($value["type"],$key, array('name','source'));
                            // var_dump( $eltParent);
                            if( !empty($eltParent) && 
                                !empty($eltParent["source"]) && 
                                !empty($eltParent["source"]["toBeValidated"]) && 
                                !empty($eltParent["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]]) &&
                                $eltParent["source"]["toBeValidated"][Yii::app()->session["costum"]["slug"]] )
                                $validated = true;
                        }else
                            $validated = true;
                        
                    }
                }
                //var_dump( $validated);
                if($validated === true)
                    $params["source"]["toBeValidated"] = array(Yii::app()->session["costum"]["slug"] => true);
            }
        }
        //Rest::json($params); exit;
        return $params;
    }

    
    public static function elementAfterSave($data){
        if(!empty(Yii::app()->session["costum"] ) ) {
            if($data["collection"] == Event::COLLECTION){
                if(!empty($data["params"]) && !empty($data["params"]["creator"]) ){
                    //var_dump($data["params"]["creator"]); exit;

                    Link::connect($data["id"], $data["collection"], $data["params"]["creator"], Person::COLLECTION, Yii::app()->session["userId"], "attendees", true, false, false, false, "");
                    Link::connect($data["params"]["creator"], Person::COLLECTION, $data["id"], $data["collection"], Yii::app()->session["userId"], "events", true, false, false, false, "");


                    // connect($originId, $originType, $targetId, $targetType, $userId, $connectType,$isAdmin=false,$pendingAdmin=false,$isPending=false, $isInviting=false, $role="", $settings=false)
                }
            }
        }
        //Rest::json($params); exit;
        return $data;
    }
}
?>