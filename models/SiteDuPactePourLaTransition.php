<?php

class SiteDuPactePourLaTransition {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";

	public static $dataBinding_allPerson  = [
        "name"      => ["valueOf" => "name"],
        "email"      => ["valueOf" => "email"],
        "tags"      => ["valueOf" => "tags"],
        "Code Postal"     => array("parentKey"=>"scope","valueOf"=>array("postalCode"=> array("valueOf" => "postalCode"))),
    ];

    public static $dataBinding_allOrganization  = array(
        "id"     => array("valueOf" => "id"),
        "name"      => array("valueOf" => "name"),
        // "postalcode"     => array("parentKey"=>"address", 
        //                      "valueOf" => array("postalCode" => array("valueOf" => "postalCode"))),
        // "ville"     => array("parentKey"=>"address", 
        //                      "valueOf" => array("addressLocality" => array("valueOf" => "addressLocality"))),
        "Candidat"     => "",
        "Code Postal"     => array("parentKey"=>"scope","valueOf"=>array("postalCode"=> array("valueOf" => "postalCode"))),
        "Description du candidat"     => "",
        "Mesures"     => "",
        // "address"   => array("parentKey"=>"address", 
        //                      "valueOf" => array(
        //                             "@type"             => "PostalAddress", 
        //                             "streetAddress"     => array("valueOf" => "streetAddress"),
        //                             "postalCode"        => array("valueOf" => "postalCode"),
        //                             "addressLocality"   => array("valueOf" => "addressLocality"),
        //                             "codeInsee"         => array("valueOf" => "codeInsee"),
        //                             "addressRegion"     => array("valueOf" => "addressRegion"),
        //                             "addressCountry"    => array("valueOf" => "addressCountry")
        //                             )),
        // "postalcode"   => array("parentKey"=>"geo", 
        //                      "valueOf" => array(
        //                             "@type"             => "GeoCoordinates", 
        //                             "postalcode"          => array("valueOf" => "latitude"),
        //                             "longitude"         => array("valueOf" => "longitude")
        //                             )),
        // "startDate"     => array("valueOf" => "startDate"),
        // "endDate"       => array("valueOf" => "endDate"),
        // "geo"   => array("parentKey"=>"geo", 
        //                      "valueOf" => array(
        //                             "@type"             => "GeoCoordinates", 
        //                             "latitude"          => array("valueOf" => "latitude"),
        //                             "longitude"         => array("valueOf" => "longitude")
        //                             )),

    );

    // public static $dataHead_allOrganization  = array(
    //     "type"     => "Type",
    //     "name"      => "Nom",
    //     "siren"     => "Siren",
    //     "why"     => "Pourquoi souhaitez-vous réaliser un CTE ? Quelles sont vos attentes vis-à-vis d’un CTE ?",
    //     "nbHabitant"     => "Nombre d'habitants concernés?",
    //     "dispositif"     => "Quels sont les dispositifs actuellement mis en place sur tout ou partie du territoire (contractuels et/ou en lien avec la transition écologique) ?",
    //     "planPCAET"     => "Avez élaboré un Plan climat-air-énergie territorial (PCAET) sur votre territoire ?",
    //     "singulartiy"     => "Qu'est ce qui constitue la singularité de votre territoire ? Quel est le contexte économique et quels sont ses enjeux en termes de transition écologique ?",
    //     "caracteristique"     => "Quelles sont les caractéristiques de votre territoire sur lesquelles vous agissez/vous souhaiteriez agir dans le cadre du CTE ? (exemples : problèmes majeurs/risques auxquels vous êtes confrontés, thèmes sur lesquels vous vous êtes engagés à agir)",
    //     "actions"     => "Quelles sont les actions envisagées dans votre projet de CTE ? Décrivez au moins 3 pistes d’actions.",
    //     "economy"     => "Quels acteurs socio-économiques prévoyez-vous de mobiliser pour le portage d’actions du CTE ?",
    //     "inCharge"     => "Qui serait en charge du projet au sein de la collectivité (chargé de mission, équipe dédiée, élu...) ?",
    //     "autreElu"     => "Autres élus engagés politiquement dans la réussite du projet (député, sénateur, maires) ?",
    //     "contact"     => "Nom du référent local, Adresse postale, numéro de téléphone et adresse mail ?",
    //     // "url.communecter"     => "Url ctenat",
    //     // "url.pdf"     => "PDF",
    //     "url.website"     => "Site internet",
    //     "address.streetAddress"     => "Rue",
    //     "address.postalCode"     => "Code Postal",
    //     "address.addressLocality"     => "Ville",
    //     "address.codeInsee"     => "Insee",
    //     "address.addressCountry"     => "Code Pays",
    //     "geo.latitude"     => "Latitude",
    //     "geo.longitude"     => "Longitude",

    // );

    public static function saveContrat($params){

        //var_dump("HelloThere"); exit;
        PHDB::remove(Poi::COLLECTION, array("source.key"=>"siteDuPactePourLaTransition", "type"=>"contract"));
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $measuresPacte=PHDB::find(Poi::COLLECTION, array( "source.key" => "siteDuPactePourLaTransition",
                                        "type" => "measure"), array("name"));
        $resultImport = array();
       // var_dump($res["elementsObj"]); exit;
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"])){
                    $value["type"] = "contract";
                    $value["creator"] = Yii::app()->session["userId"];
                    $value["created"] = time();
                    if(strlen(@$value["postalCode"])==4){
                        $value["postalCode"]="0".$value["postalCode"];
                    }
                    $measures = null ;
                    if(!empty($value["mesuresPacte"])){
                        $measures = explode(",", $value["mesuresPacte"]);
                        unset($value["mesuresPacte"]);
                    }
                    $insee = null ;
                    if(!empty($value["insee"])){
                        $insee = $value["insee"];
                        $city=City::getCityByInsee($insee);
                        if(empty($city)){
                            if(strlen($insee)==4){
                                $tryNewInsee="0".$insee;
                                $city=City::getCityByInsee($tryNewInsee);
                                if(empty($city)){
                                    $getCities=City::getByPostalCode(@$value["postalCode"]);
                                    if(!empty($getCities)){
                                        foreach($getCities as $k=> $v){
                                            $city=$v;
                                        }
                                        $value["insee"]=$city["insee"];
                                        $insee=$city["insee"];
                                    }
                                }
                            }
                        }
                        $prepScope=array();
                        if(isset($city["level1"])){
                            $prepScope["level1Name"] = $city["level1Name"];
                            $prepScope["level1"] = $city["level1"];
                        }

                        if(isset($city["level2"])){
                            $prepScope["level2Name"] = $city["level2Name"];
                            $prepScope["level2"] = $city["level2"];
                        }

                        if(isset($city["level3"])){
                            $prepScope["level3Name"] = $city["level3Name"];
                            $prepScope["level3"] = $city["level3"];
                        }

                        if(isset($city["level4"])){
                            $prepScope["level4Name"] = $city["level4Name"];
                            $prepScope["level4"] = $city["level4"];
                        }
                        else if(isset($city["country"]))
                            $prepScope["country"] = $city["country"];
                    
                        $prepScope["postalCode"] = $city["postalCodes"][0]["postalCode"];
                        $prepScope["cityName"] = $city["postalCodes"][0]["name"];
                        $prepScope["city"] = (string)$city["_id"];
                        $prepScope["type"] ="cities";
                        if(count($city["postalCodes"])>1)
                            $key=(string)$city["_id"]."cities".$city["postalCodes"][0]["postalCode"];
                        else
                            $key=(string)$city["_id"]."cities";
                        $prepScope["key"]=$key;
                        $value["scope"]=array($key=>$prepScope);
                    }
                    //$searchRegExp = Search::accentToRegex($value["name"]);
                    //$alreadyContractExist=PHDB::find(Poi::COLLECTION, array("name" => new MongoRegex("/.*{$searchRegExp}.*/i"), "insee"=> $insee,"source.key"=>"siteDuPactePourLaTransition", "type"=>"contract"));
                    //if(empty($alreadyContractExist)){
                      //  foreach($alreadyContractExist as $k => $v){
                        //    PHDB::remove(Poi::COLLECTION, array("_id"=>$v["_id"]));
                        //}
                    //}
                    $postalCode = null ;
                    if(!empty($value["postalCode"])){
                        $postalCode = $value["postalCode"];
                         $where = array( "source.key" => "siteDuPactePourLaTransition",
                                            "email" => "pacte-".$postalCode."@listes.transition-citoyenne.org"  );
                           
                            $orga = PHDB::findOne( Organization::COLLECTION, $where, array("name") );
                            if(!empty($orga))
                                $value["parent"]=array((string)$orga["_id"]=>array("type"=>Organization::COLLECTION));  
                        if(isset($value["url"]) && !empty($value["url"])){
                            PHDB::update(Organization::COLLECTION, 
                                array("_id"=>$orga["_id"]), 
                                array('$set'=>array("url"=>$value["url"]))
                            );
                            unset($value["url"]);
                        } 
                    }
                    $mesuresLocale=array();
                    for ($i=0; $i < 5; $i++) { 
                        # code...
                        $incMes=($i+1);
                        if(isset($value["mesureLocale".$incMes]) && !empty($value["mesureLocale".$incMes])){
                            $mesuresLocale[$i]=array("name"=>"Mesure locale ".$incMes, "description"=>$value["mesureLocale".$incMes]);
                            unset($value["mesureLocale".$incMes]);
                        }
                    }
                    if(!empty($mesuresLocale))
                        $value["localMeasures"]=$mesuresLocale;
                    $update = false;
                    $id = false;
                     if( !empty($measures) ){
                        //$listOwnerRegex = array();
                        $links=array();
                        $links["measures"]=array();
                        foreach ($measures as $keylO => $vallO) {
                            $mesLevel=explode(".", $vallO);
                            $keyMes=self::array_find($mesLevel[0], $measuresPacte);
                            if(!empty($keyMes) && !empty($mesLevel[1]) ){
                                $links["measures"][$keyMes]=array("type"=>Poi::COLLECTION, "level"=>$mesLevel[1]);
                            }
                        }
                        if(!empty($links["measures"]))
                            $value["links"]=$links;
                    }
                    
                    $resElt = array(
                        "update" => false,
                        "id" => false,
                        "name" => @$value["name"]
                    );
                   // $contractExist=PHDB::findOne(Poi::COLLECTION, array("insee"=>$insee, "name"=>@$value["name"]));
                    //if(empty($contractExist)){
                        //var_dump($value);
                    PHDB::insert(Poi::COLLECTION, $value );
                    //if( !empty($measures) ){
                        //$listOwnerRegex = array();
                     //   foreach ($measures as $keylO => $vallO) {
                     //       $mesLevel=explode(".", $vallO);
                     //       $listOwnerRegex = new MongoRegex("/.*{$mesLevel[0]}.*/i");   
                     //       $where = array( "source.key" => "siteDuPactePourLaTransition",
                       //                 "type" => "measure",
                         //               "name" => $listOwnerRegex );
                           // $measure = PHDB::findOne( Poi::COLLECTION, $where, array("name") );
                    //        if(!empty($measure) && !empty($mesLevel[1]) ){
                      //         // foreach ($measure as $keyM => $valM) {
                        //            Link::connect((String) $value["_id"], Poi::COLLECTION, (string)$measure["_id"], Poi::COLLECTION, Yii::app()->session["userId"], "measures",false,false,false,false, "",array("name"=>"level", "value"=>$mesLevel[1]));
                          //          Link::connect((string)$measure["_id"], Poi::COLLECTION, (String) $value["_id"], Poi::COLLECTION, Yii::app()->session["userId"], "contracts",false,false,false, false, "", array("name"=>"level", "value"=>$mesLevel[1]));
                                //}
                    //        }
                    //    }
                    //}*/
                    $resultImport[] = $resElt;
                  //  }
                }   
            } 
        }

        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }
    public static function array_find($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            if (false !== stripos($value["name"], $needle)) {
                return $key;
            }
        }
        return false;
    }
}
?>