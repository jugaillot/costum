<?php
class TemplateCostum{
    const collection = "costum";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function getEvent($source){
        $params = array(
            "result"    =>  false);
        
        
            date_default_timezone_set('UTC');

            $date_array = getdate ();
            $numdays = $date_array["wday"];
     
         //    $startDate = strtotime(date("Y-m-d", time() - ($numdays * 24*60*60)));
            $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
            $startDate = strtotime(date("Y-m-d H:i"));
     
         //    var_dump(date(DateTime::ISO8601, $endDte));
         
            
            $where = array(
                 "source" => array(
                    "insertOrign" =>    "costum",
                    "keys"         =>    array(
                                        $source["slug"]),
                    "key"          =>  $source["slug"]),            
                 "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
                 "endDate"     =>  array('$lt'    => new MongoDate($endDate))
                 );
         
     

        $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION,$where,3);
        //    var_dump($allEvent);
    
           if(@$allEvent){
               //WHAT ENCORE UN EL FAMOSO TABLEAU
               $res = array();
               
               $params = array(
                   "result" =>  true
               );
    
               $res = self::createResultEvent($allEvent);
               return array_merge($params,$res);
           }
    
           return $params;
    }


   private static function createResultEvent($params){

    $res["element"] = array();
    foreach($params as $key => $value){

     $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
     $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
     $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

        array_push($res["element"], array(
            "id"               => (String) $value["_id"],
            "name"             =>  $value["name"],
            "startDate"        => date(DateTime::ISO8601, $value["startDate"]->sec),
            "type"             =>  Yii::t("event",$value["type"]),
            "imgMedium"        =>  $imgMedium,
            "img"              =>  $img,
            "resume"           =>  $resume,
            "slug"             =>  $value["slug"]
        ));
    }
 //    var_dump($res);
    return $res;
}

}