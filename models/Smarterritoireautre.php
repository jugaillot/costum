<?php 
class Smarterritoireautre{
  const COLLECTION = "costum";
  const CONTROLLER = "costum";
    const MODULE = "costum";

   public static function getEvent($id,$type,$source){

       $parametres = array(
           "result" => false
       );
       date_default_timezone_set('UTC');

       $date_array = getdate();
       $numdays = $date_array["wday"];

       $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
       $startDate = strtotime(date("Y-m-d H:i"));
    
       
       $where = array(
                 "source" => array(
                    "insertOrign" =>    "costum",
                    "keys"         =>    array(
                                        $source),
                    "key"          =>  $source),            
                 "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
                 "endDate"     =>  array('$lt'    => new MongoDate($endDate))
                 );


       $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where,3);

       if(isset($allEvent)){

           $results = array();
           
           $parametres = array(
               "result" =>  true
           );

           $results = self::createResultEvent($allEvent);
           return array_merge($parametres,$results);
       }
       return $parametres;
   }


   private static function createResultEvent($params){

       $res["element"] = array();
       foreach($params as $key => $value){
        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"], 
               "type"             =>  Yii::t("event",$value["type"]),
               "startDate"        =>  date(DateTime::ISO8601, $value["startDate"]->sec),
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "slug"             =>  $value["slug"]
           ));
       }
       return $res;
   }

    public static function getCommunity($id,$type){
        $community = Element::getCommunityByTypeAndId($type, $id, "all", null , "filières",null);
        
       if (isset($community)) {
          $results = self::getDataCommunity($community);
       }
       return $results;
    }


     private static function getDataCommunity($data){
        $params = array(
             "result" => false
         );

        $elements = [];

        foreach ($data as $k => $v) {
          $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilImageUrl","profilRealBannerUrl","links","slug")); 
        }

        // var_dump($elements);

        if (isset($elements)) {
            $params = array(
                "result" => true
            );
            
          $results  = self::createResultCommunity($elements);
          return array_merge($params, $results);
        }
        return $params;
     }

   private static function createResultCommunity($params){

       $res["elt"] = array();

       foreach($params as $key => $value){
        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $imgBanner = (@$value["profilRealBannerUrl"] ? $value["profilRealBannerUrl"] : "none");
        $countActus = PHDB::count(News::COLLECTION,array("source.key" => "smarterritoireautre"));

           array_push($res["elt"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"],
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "imgBanner"        =>  $imgBanner,
               "slug"             =>  $value["slug"],  
               "countEvent"       =>  count(@$value["links"]["events"]),
               "countActeurs"     =>  count(@$value["links"]["members"]),
               "countProjet"      =>  count(@$value["links"]["projects"]),
               "countActus"       =>  @$countActus
           ));
        }
    return $res;
  }
}
