<?php
/**
 * 
 */
class MednumRhoneAlpe{
  const COLLECTION = "costum";
  const CONTROLLER = "costum";
  const MODULE = "costum";

  public static function getEventCommunity($id,$type){

    $community = Element::getCommunityByTypeAndId($type, $id, "organizations", null , null ,null);
        
       if (isset($community)) {
          $results = self::getDataEventCommunity($community);
       }

       return $results;
   }

   private static function getDataEventCommunity($data){
        $params = array(
             "result" => false
         );

        $elements = [];
        $tabres = [];
        $final = [];

        date_default_timezone_set('UTC');

       $date_array = getdate();
       $numdays = $date_array["wday"];

       $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
       $startDate = strtotime(date("Y-m-d H:i"));

       $where = array(
                 "source" => array(
                    "insertOrign" =>    "costum",
                    "keys"         =>    array(
                                        Yii::app()->session["costum"]["contextSlug"]),
                    "key"          =>  Yii::app()->session["costum"]["contextSlug"]),            
                 "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
                 "endDate"     =>  array('$lt'    => new MongoDate($endDate))
                 );

       $tabres = PHDB::find(Event::COLLECTION,$where);

        foreach ($data as $k => $v) {
          $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilImageUrl","profilRealBannerUrl","links","slug"));

           $tabres += PHDB::find(Event::COLLECTION,array(
            "links.organizer.".$elements[$k]["_id"] => array('$exists'=>1),
            "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
            "endDate"     =>  array('$lt'    => new MongoDate($endDate))
            ));
        }

        if (isset($tabres)) {
            $params = array(
                "result" => true
            );
            
          $results  = self::createResultEvent($tabres);

          return array_merge($params,$results);
        }
        return $results;
     }


   private static function createResultEvent($params){
       $res["element"] = array();
       foreach($params as $key => $value){
        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

           array_push($res["element"], array(
               "id"               => (String) @$value["_id"],
               "name"             =>  @$value["name"],
               "shortDescription" =>  @$resume,
               "type"             =>  @Yii::t("event",$value["type"]),
               "startDate"        =>  date(DateTime::ISO8601, @$value["startDate"]->sec),
               "imgMedium"        =>  @$imgMedium,
               "img"              =>  @$img,
               "slug"             =>  @$value["slug"]
           ));
       }
       return $res;
   }
}










// $parametres = array(
       //     "result" => false
       // );
       // date_default_timezone_set('UTC');

       // $date_array = getdate();
       // $numdays = $date_array["wday"];

       // $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
       // $startDate = strtotime(date("Y-m-d H:i"));
    
       
       // $where = array(
       //      "organizer" =>    array(
       //                              $id),
       //      "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
       //      "endDate"     =>  array('$lt'    => new MongoDate($endDate))
       //      );


       // // var_dump($where);


       // $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where,3);

       // var_dump($allEvent);

       // if(isset($allEvent)){

       //     $results = array();
           
       //     $parametres = array(
       //         "result" =>  true
       //     );

       //     $results = self::createResultEvent($allEvent);
       //     return array_merge($parametres,$results);
       // }
       // return $parametres;
?>


